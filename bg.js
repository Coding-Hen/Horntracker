var _link_JSON = {};

chrome.runtime.onConnect.addListener(function(port) {
    port.onMessage.addListener(function(msg) {

        if (msg.request == "options")
        {
            //Fix for people who had the dreaded default option.
            localStorage.removeItem('ht_00000000');

            var xhr_url = 'http://www.horntracker.com/backend/getoptions.php?fbid=' + msg.user;
            var xhr = new XMLHttpRequest();

            xhr.open("GET", xhr_url, true);


            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4)
                {
                    var options_msg = "";
                    if (is_JSON_string(xhr.responseText))
                    {
                        var opts = JSON.parse(xhr.responseText);


                        if (localStorage.getItem('ht_' + msg.user) == null)
                            localStorage.setItem('ht_' + msg.user, xhr.responseText);
                        else
                        {
                            //We need to get local notification position data which is not stored on the server.
                            var local_opts = JSON.parse(localStorage.getItem('ht_' +  msg.user));
                            opts['ht_box_position_fb']  = local_opts['ht_box_position_fb'];
                            opts['ht_box_position_mhg'] = local_opts['ht_box_position_mhg'];

                            opts['ht_default_name'] = local_opts['ht_default_name'];

                            localStorage.setItem('ht_' + msg.user, JSON.stringify(opts));
                        }


                        port.postMessage({options: opts, msg: options_msg});
                    }
                    else
                    {
                        options_msg = 'Could not retrieve existing settings, the options you have set here</br> are the last saved settings on this machine and may not represent your settings saved on the server.</br></br>';
                        port.postMessage({options: JSON.parse(localStorage.getItem('ht_' + msg.user)), msg: options_msg});
                    }
                }
            }

            xhr.send();
        }

        else if (msg.request == "push")
        {
            localStorage.setItem(msg.mm_user, msg.mm_data);
            localStorage.setItem('ht_mostmiceme', msg.ht_user);
        }


        else if (msg.request == "badge_update")
        {
            if (is_set(msg.text))
                chrome.browserAction.setBadgeText({text:msg.text});
            if (is_set(msg.bg_color))
                chrome.browserAction.setBadgeBackgroundColor({color:msg.bg_color});
        }

        else if (msg.request == "input_my_setup")
        {
            chrome.tabs.sendMessage(msg.tabId, msg.user, function(v) {
                //console.log(v);
            });
        }

        else if (msg.request == "update_options")
        {
            update_ht_options(msg.fbid, msg.default_name);
        }

        else if (msg.request == "update_notification_position")
        {
            update_ht_notification(msg.fbid, msg.position, msg.pos_var);
        }

        else if (msg.request == "getrequest")
        {
            var xhr_url = msg.url;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", xhr_url, true);

            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4)
                    port.postMessage({html: xhr.responseText});
            }

            xhr.send();
        }

        else if (msg.request == "postrequest")
        {
            var xhr_url = msg.url;
            var xhr = new XMLHttpRequest();

            xhr.open("POST", xhr_url, true);
            xhr.setRequestHeader("Content-type", "application/json");

            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4)
                    port.postMessage({html: xhr.responseText, original_msg: msg});
            }

            update_ht_options(msg.fbid, msg.default_name);

            //New style.
            msg.ht_post_params.paramJSON = localStorage.getItem('ht_' + msg.fbid);
            msg.ht_post_params.fbid = msg.fbid;
            msg.ht_post_params.last_seen = msg.lastHT;
            if (is_set(msg.hunters_online) && is_set(msg.hunters_online_date))
            {
                msg.ht_post_params.hunters_online = msg.hunters_online;
                msg.ht_post_params.hunters_online_date = msg.hunters_online_date;
            }

            xhr.send(JSON.stringify(msg.ht_post_params));
        }

        else if (msg.request == "postrequest_hg")
        {
            var xhr_url = msg.url;
            var xhr = new XMLHttpRequest();
            xhr.open("POST", xhr_url, true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4)
                    port.postMessage({html: xhr.responseText, original_msg: msg});
            }

            msg.params.hg_is_ajax = 1;
            msg.params.sn = 'Hitgrab';


            xhr.send(serialize(msg.params).replace(" ", "+"));

        }

        else if (msg.request == "postrequest_fb")
        {
            var xhr_url = msg.url;
            var xhr = new XMLHttpRequest();
            xhr.open("POST", xhr_url, true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4)
                    port.postMessage({html: xhr.responseText, original_msg: msg});
            }

            msg.params.hg_is_ajax = 1;
            msg.params.sn = 'Facebook';

            xhr.send(serialize(msg.params).replace(" ", "+"));
        }
    });
});

function update_ht_options(fbid, default_name)
{
    var storageJSON = { };

    if (localStorage.getItem('ht_' + fbid) == null)
    {
        storageJSON = default_options();
        storageJSON['ht_default_name'] = default_name;

        localStorage.setItem('ht_' + fbid, JSON.stringify(storageJSON));
    }
    else
    {
        //Always update the name, options may have an incorrect name stored.
        storageJSON = JSON.parse(localStorage.getItem('ht_' + fbid));
        storageJSON['ht_default_name'] = default_name;

        localStorage.setItem('ht_' + fbid, JSON.stringify(storageJSON));
    }
}

function update_ht_notification(fbid, position, position_var)
{
    var storageJSON = { };

    if (localStorage.getItem('ht_' + fbid) == null)
    {
        storageJSON = default_options();
        
        //Update notification position.
        storageJSON['ht_box_position_fb'] = position;
        storageJSON['ht_box_position_mhg'] = position;

        localStorage.setItem('ht_' + fbid, JSON.stringify(storageJSON));
    }
    else
    {
        storageJSON = JSON.parse(localStorage.getItem('ht_' + fbid));
        storageJSON[position_var] = position;

        localStorage.setItem('ht_' + fbid, JSON.stringify(storageJSON));
    }
}

function default_options()
{
    //By default opted out of everything.
    storageJSON = { };
    storageJSON['ht_is_opted_in_hunt'] = false;
    storageJSON['ht_is_opted_in_convertible'] = false;
    storageJSON['ht_is_opted_in_crown'] = false;
    storageJSON['ht_is_opted_in_mice'] = false;

    storageJSON['ht_is_opted_in_profile'] = false;
    storageJSON['ht_is_opted_in_cheese'] = false;
    storageJSON['ht_is_opted_in_potions'] = false;
    storageJSON['ht_is_opted_in_charms'] = false;
    storageJSON['ht_is_opted_in_special'] = false;
    storageJSON['ht_is_opted_in_journal'] = false;
    storageJSON['ht_is_opted_in_tacky'] = false;

    storageJSON['ht_is_sharing_profile'] = false;
    storageJSON['ht_is_sharing_cheese'] = false;
    storageJSON['ht_is_sharing_potions'] = false;
    storageJSON['ht_is_sharing_charms'] = false;
    storageJSON['ht_is_sharing_special'] = false;
    storageJSON['ht_is_sharing_journal'] = false;
    storageJSON['ht_is_sharing_tacky'] = false;
    storageJSON['ht_is_opted_in_public_hunts'] = false;

    storageJSON['ht_cheese_order'] = 'ht_cheese_az';
    storageJSON['ht_potion_order'] = 'ht_potion_az';
    storageJSON['ht_charm_order'] = 'ht_charm_az';
    storageJSON['ht_special_order'] = 'ht_special_az';

    storageJSON['ht_default_name'] = 'Unknown';
    storageJSON['ht_alt_name'] = '';

    //Notification position.
    storageJSON['ht_box_position_fb'] = '50px-300px';
    storageJSON['ht_box_position_mhg'] = '50px-300px';

    //By default notifications are on, and set for 10 seconds.
    storageJSON['ht_notification'] = true;
    storageJSON['ht_notification_display_time'] = 10;

    //By default attraction rate instead of attraction bonus is off as is stale rate instead of freshness.
    storageJSON['ht_real_ar'] = false;
    storageJSON['ht_real_stale'] = false;

    //By default platinum crown display is false.
    storageJSON['ht_is_platinum_crown'] = false;

    //By default mouse badges is false.
    storageJSON['ht_is_mouse_badge'] = false;

    //By default mouse catch goals are off, and set for 100 catches when on.
    storageJSON['ht_is_opted_in_goals'] = false;
    storageJSON['ht_is_opted_in_goals_amount'] = 100;

    //Default for badge text is none.
    storageJSON['ht_badge_text'] = 'ht_badge_none';

    //By default MostMice report checking is off.
    storageJSON['ht_is_opted_in_mostmice'] = false;

    //By default crafting submissions are off.
    storageJSON['ht_is_opted_in_crafting'] = false;

    //By default revealing hidden specials is off.
    storageJSON['ht_is_reveal_hidden_special'] = false;

    //Default message style.
    storageJSON['ht_message_style'] = 'You successfully submitted %s hunt(s)!';

    //By default, modify TEM display is off
    storageJSON['ht_is_opted_in_modify_TEM'] = false;
    //Default TEM display message
    storageJSON['ht_TEM_display_style'] = 'AR: %ar%</br>CR: %cr%</br>ECR: %ecr%';

    return storageJSON;
}

function is_set(variable)
{
    if (typeof variable === "undefined")
        return false;
    else
        return true;
}

function is_JSON_string(str) 
{
    try 
    {
        JSON.parse(str);
    } 
    catch (e) 
    {
        return false;
    }

    return true;
}

function serialize(obj)
{
  var str = [];
  
  for (var item in obj)
  {
    if (typeof obj[item] === "object")
    {
        for (var subitem in obj[item])
        {
            if (typeof obj[item][subitem] !== "function")
                str.push(encodeURIComponent(item) + "[" + encodeURIComponent(subitem) + "]=" + encodeURIComponent(obj[item][subitem]));
        }
    }
    else if (typeof obj[item] !== "function")
    {
        str.push(encodeURIComponent(item) + "=" + encodeURIComponent(obj[item]));
    }
  }
  
  return str.join("&");
}