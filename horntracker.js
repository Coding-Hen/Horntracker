//Written by Nick Alston, inspired by Rohan Mehta.
var _version = chrome.runtime.getManifest().version;
var _options = new Array();
var _is_active = true;
var _inactive_message = '';
var _last_ht_seen = Math.round((new Date()).getTime() / 1000);
var _setupHTMarkerCalled = false;

var _hunters_online = '';
var _hunters_online_date = '';
var uObj;

var _cur_user_id;
var _cur_user_options;
var _cur_user_full_name;

var _user_profile = {};

var _request_type = 'postrequest_hg';

setupIntercept(chrome.runtime.getURL("xhrintercept.js"));

getUserObject(initWithUser);

function initWithUser(resp) {
    uObj = resp;

    //Between these two, should find the user id on the page via some embedded JSON object.
    _cur_user_id = uObj.sn_user_id;
    _cur_user_full_name = uObj.firstname + " " + uObj.lastname;

    setupHTMarker(uObj.sn_user_id);

    if (_is_active) {
        optionsRequest(_cur_user_id, true);

        //Track users online, and use a datestamp. Must be done on page load only, or the number will be stale.
        set_hunters_online();
    }
    else
        appendStatus(_inactive_message);

    //This will update the name in the options to allow for easy selection in the options screen.
    updateOptions();
}

function optionsRequest(fbid, isInitialCheck) {
    var options_port = chrome.runtime.connect({name: "options_request"});
    options_port.postMessage({request: "options", user: fbid});
    options_port.onMessage.addListener(function (msg) {

        //Update _cur_user_options.
        if (is_set(msg.options))
            _cur_user_options = msg.options;

        //Call the initial page checks after options have been updated if requested.
        if (isInitialCheck)
            check_pages();



        //We should setup the Chrome badge here now that we know our options are completed.
        setupBadge();

        if (msg.msg != '')
            appendStatus(msg.msg);

        //Update xhrintercept with the new options.
        var evt = document.createEvent("MutationEvents");
        evt.initMutationEvent("xhri_options_change", true, true, null, _cur_user_id, JSON.stringify(_cur_user_options), 'ht_stuff', 1);
        document.dispatchEvent(evt);
    });
}

function setupBadge() {
    //Setup badge display if options for it is not 'ht_badge_none'.
    if (is_set(_cur_user_options)) {
        if (_cur_user_options.ht_badge_text !== 'ht_badge_none') {
            if (localStorage.getItem('ht_badge_text_last_value') !== null &&
                localStorage.getItem('ht_badge_text_last_color') !== null) {
                updateBadge(localStorage.getItem('ht_badge_text_last_value'), localStorage.getItem('ht_badge_text_last_color'));
            }
            else
                updateBadge('?', '#66CDAA');
        }
    }
}

function updateOptions() {
    var update_options_port = chrome.runtime.connect({name: "update_options_request"});
    update_options_port.postMessage({request: "update_options", fbid: _cur_user_id, default_name: _cur_user_full_name});
}

function check_pages() {
    if (is_set(_cur_user_options)) {
        //TEM update for individual mouse
        if (document.URL.match(/adversaries.php\?mouse=/g) !== null)
            ht_submit_TEM_request_individual();

        //CrownTracker/MostMice/User Profile enabled.
        if (document.URL.match(/profile.php\?snuid=(\w+)/g) !== null) {
            var profile_user_id = document.URL.match(/profile.php\?snuid=(\w+)/g)[0].replace('profile.php?snuid=', '');

            if (_cur_user_options.ht_is_opted_in_crown ||
                _cur_user_options.ht_is_opted_in_mostmice ||
                _cur_user_options.ht_is_opted_in_goals) {
                if (_cur_user_options.ht_is_opted_in_mostmice ||
                    _cur_user_id == profile_user_id) {
                    var POST_params = {};
                    var profile_port = chrome.runtime.connect({name: "get_crowns_request"});
                    var ht_request = 'https://www.mousehuntgame.com/managers/ajax/users/profiletabs.php?action=badges&snuid=' + profile_user_id;

                    profile_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
                    profile_port.onMessage.addListener(function (msg) {
                        var enter_JSON = {};
                        if (is_JSON_string(msg.html))
                            enter_JSON = JSON.parse(msg.html);
                        else {
                            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                            return;
                        }

                        var mice_caught = {};
                        var mice_thumb = {};

                        for (var mouse in enter_JSON.mouse_data) {
                            mice_caught[enter_JSON.mouse_data[mouse].name] = Number(enter_JSON.mouse_data[mouse].num_catches);
                            mice_thumb[enter_JSON.mouse_data[mouse].name] = enter_JSON.mouse_data[mouse].thumb;
                        }

                        if (_cur_user_id == profile_user_id) {
                            //For a personal profile use remaining_mice, will only used logged-in user's non-crowned mice here.
                            for (var mouse in enter_JSON.remaining_mice) {
                                var m_name_full = enter_JSON.remaining_mice[mouse].name;
                                var m_quant = Number(m_name_full.match(/\((\d)+\)/g)[0].replace('(', '').replace(')', ''));

                                var m_name = m_name_full.substr(0, m_name_full.indexOf('(')).trim();
                                mice_caught[m_name] = m_quant;
                            }
                        }

                        if (_cur_user_options.ht_is_opted_in_mostmice) {
                            if (document.URL.match(/mousehuntgame/g) !== null) {
                                var un = $('span.hunterInfoView-userName').text();
                                var mm_title_gif = $('img.hunterInfoView-userTitleIcon').attr('src').split('?')[0].split('/').pop();
                                var hunter_id = $('div#profileCampLeft > span.label').text();

                                var profile_info = {
                                    un: un,
                                    ts: Date(),
                                    mice: mice_caught,
                                    title: mm_title_gif,
                                    mice_thumbs: mice_thumb,
                                    hunter_id: hunter_id
                                };

                                var submit_port = chrome.runtime.connect({name: "mm_push"});
                                submit_port.postMessage({
                                    request: "push",
                                    mm_user: 'mostmice_' + profile_user_id,
                                    mm_data: JSON.stringify(profile_info),
                                    ht_user: _cur_user_id
                                });

                                ht_submit_mostmice(JSON.stringify(profile_info), profile_user_id, 'http://www.horntracker.com/backend/submit/mostmicesubmit.php');
                            }
                        }

                        if (_cur_user_options.ht_is_opted_in_crown &&
                            _cur_user_id == profile_user_id) {
                            ht_submit_from_storage(JSON.stringify(mice_caught), null, _cur_user_id, _cur_user_full_name, 'ht_is_opted_in_crown', 'http://www.horntracker.com/backend/submit/crownsubmit.php');
                        }


                        if (_cur_user_options.ht_is_opted_in_goals &&
                            _cur_user_id == profile_user_id) {
                            ht_crown_calc(JSON.stringify(mice_caught), _cur_user_id, _cur_user_full_name, 'ht_is_opted_in_goals', 'http://www.horntracker.com/backend/submit/crowncalc.php');
                        }
                    });
                }
            }

            if (_cur_user_options.ht_is_opted_in_profile) {
                //Add a "Journal Summaries" tab
                if (_cur_user_id == profile_user_id) {
                    if (_cur_user_options.ht_is_opted_in_journal)
                        populateJournals(profile_user_id);
                }
                else
                    populateJournals(profile_user_id);
            }
        }

        //TODO: Add in options that would use this.
        //General inventory tracking.
        if (document.URL.match(/inventory.php/g) !== null) {
            var POST_params = {
                "classifications": ["bait", "trinket", "potion", "stat", "quest", "convertible"],
                "action": "get_items_by_classification",
                "uh": uObj.unique_hash
            };

            var inventory_port = chrome.runtime.connect({name: "post_inventory_request"});
            var ht_request = 'http://www.mousehuntgame.com/managers/ajax/users/userInventory.php';

            inventory_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
            inventory_port.onMessage.addListener(function (msg) {
                var enter_JSON = {};
                if (is_JSON_string(msg.html))
                    enter_JSON = JSON.parse(msg.html);
                else {
                    appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                    return;
                }

                var special_inv = {};
                var potion_inv = {};
                var cheese_inv = {};
                var charm_inv = {};

                var special_thumb = {};
                var potion_thumb = {};
                var cheese_thumb = {};
                var charm_thumb = {};

                var special_all = {};

                for (var item in enter_JSON.items) if (enter_JSON.items.hasOwnProperty(item) && item != "length") {
                    var classification = enter_JSON.items[item].classification;
                    var item_name = enter_JSON.items[item].name;
                    var item_quant = enter_JSON.items[item].quantity;
                    var thumbnail = enter_JSON.items[item].thumbnail;
                    switch (classification) {
                        case "stat":
                        case "quest":
                        case "convertible":
                            //Specials
                            special_inv[item_name] = item_quant;
                            special_thumb[item_name] = thumbnail;
                            special_all[item_name] = enter_JSON.items[item];
                            break;
                        case "potion":
                            //Potions
                            potion_inv[item_name] = item_quant;
                            potion_thumb[item_name] = thumbnail;
                            break;
                        case "bait":
                            //Cheese
                            cheese_inv[item_name] = item_quant;
                            cheese_thumb[item_name] = thumbnail;
                            break;
                        case "trinket":
                            //Charms
                            charm_inv[item_name] = item_quant;
                            charm_thumb[item_name] = thumbnail;
                            break;
                    }
                }

                if (_cur_user_options.ht_is_opted_in_cheese && !is_empty(cheese_inv))
                    ht_submit_from_storage(JSON.stringify(cheese_inv), JSON.stringify(cheese_thumb), _cur_user_id, _cur_user_full_name, 'ht_is_opted_in_cheese', 'http://www.horntracker.com/backend/submit/cheesesubmit.php');
                if (_cur_user_options.ht_is_opted_in_potions && !is_empty(potion_inv))
                    ht_submit_from_storage(JSON.stringify(potion_inv), JSON.stringify(potion_thumb), _cur_user_id, _cur_user_full_name, 'ht_is_opted_in_potions', 'http://www.horntracker.com/backend/submit/potionsubmit.php');
                if (_cur_user_options.ht_is_opted_in_charms && !is_empty(charm_inv))
                    ht_submit_from_storage(JSON.stringify(charm_inv), JSON.stringify(charm_thumb), _cur_user_id, _cur_user_full_name, 'ht_is_opted_in_charms', 'http://www.horntracker.com/backend/submit/charmsubmit.php');
                if (_cur_user_options.ht_is_opted_in_special && !is_empty(special_inv))
                    ht_submit_from_storage(JSON.stringify(special_inv), JSON.stringify(special_thumb), _cur_user_id, _cur_user_full_name, 'ht_is_opted_in_special', 'http://www.horntracker.com/backend/submit/specialsubmit.php');

                if (document.URL.match(/inventory.php\?tab=4/g) !== null) {
                    if (_cur_user_options.ht_is_reveal_hidden_special) {
                        var evt = document.createEvent("MutationEvents");
                        evt.initMutationEvent("xhri_hidden_item", true, true, null, JSON.stringify(special_all), '', 'ht_stuff', 1);
                        document.dispatchEvent(evt);
                    }
                }
            });
        }

        //View hidden specials.
        if (_cur_user_options.ht_is_reveal_hidden_special) {
            //Specials tracking enabled for the NEW UI.
            if (document.URL.match(/inventory.php\?tab=special/g) !== null)
                ht_get_specials(null);

            //Specials tracking enabled for the OLD UI.
            if (document.URL.match(/inventory.php\?tab=4/g) !== null) {
                //View hidden specials.
                if (_cur_user_options.ht_is_reveal_hidden_special) {
                    var evt = document.createEvent("MutationEvents");
                    evt.initMutationEvent("xhri_hidden_item_legacy", true, true, null, '', '', 'ht_stuff', 1);
                    document.dispatchEvent(evt);
                }
            }
        }

        //AR change enabled.
        if (_cur_user_options.ht_real_ar) {
            var loc_name = uObj.location;
            var chs_name = uObj.bait_name;
            var chm_name = uObj.trinket_name;
            var bse_name = uObj.base_name;
            var trp_name = uObj.weapon_name;

            ht_submit_ar_request(loc_name, chs_name, chm_name, bse_name, trp_name);
        }

        //Stale change enabled.
        if (_cur_user_options.ht_real_stale) {
            var loc_name = uObj.location;
            var chs_name = uObj.bait_name;
            var chm_name = uObj.trinket_name;
            var bse_name = uObj.base_name;
            var trp_name = uObj.weapon_name;

            ht_submit_stale_request(loc_name, chs_name, chm_name, bse_name, trp_name);
        }

        //Charm not trap power enabled.
        //Deprecated. 
        /*
         if (_cur_user_options.ht_charm_not_trap)
         {
         var chm_name = uObj.trinket_name;
         var chm_count = uObj.trinket_quantity;

         ht_charm_not_trap_power(chm_name, chm_count);
         }
         */
    }

    //Journal summaries
    if ($('.log_summary').length > 0) {
        var journal_fb_id = _cur_user_id;
        if (document.URL.match(/profile.php\?snuid=(\d+)/g) !== null) {
            //If we're on someone else's profile page we need to get their user id for summary submissions.
            journal_fb_id = document.URL.match(/profile.php\?snuid=(\d+)/g)[0].replace('profile.php?snuid=', '');
        }

        if (_cur_user_id.toString() == journal_fb_id) {
            //NOTE: This assumes only 1 summary per page.
            var time_text = $('.log_summary div.journaltext tbody tr th')[0].innerText;

            var catches = parseInt($('td', $('.log_summary div.journaltext tbody tr')[3])[1].innerText);
            var fta = parseInt($('td', $('.log_summary div.journaltext tbody tr')[3])[3].innerText);
            var misses = parseInt($('td', $('.log_summary div.journaltext tbody tr')[4])[1].innerText);
            var stales = parseInt($('td', $('.log_summary div.journaltext tbody tr')[4])[3].innerText);

            var gold_gained = parseInt($('td', $('.log_summary div.journaltext tbody tr')[7])[1].innerText.replace(/\,|\./g, ''));
            var points_gained = parseInt($('td', $('.log_summary div.journaltext tbody tr')[7])[3].innerText.replace(/\,|\./g, ''));
            var gold_lost = parseInt($('td', $('.log_summary div.journaltext tbody tr')[8])[1].innerText.replace(/\,|\./g, ''));
            var points_lost = parseInt($('td', $('.log_summary div.journaltext tbody tr')[8])[3].innerText.replace(/\,|\./g, ''));

            var fc = $('.log_summary div.journaltext tbody tr a').attr('onclick').replace('; return false;', '');

            var ht_journal_sum = {
                time: time_text,
                cth: catches,
                fta: fta,
                miss: misses,
                stl: stales,
                g_g: gold_gained,
                g_l: gold_lost,
                p_g: points_gained,
                p_l: points_lost,
                fc: fc,
                j_fb: journal_fb_id
            };

            ht_submit_journal_summary(JSON.stringify(ht_journal_sum), _cur_user_id, _cur_user_full_name);
        }

        //NOTE: Don't submit other people's journals (they can't opt-in or opt-out this way). 
        //		Additionally the time stamps will be fairly incorrect. Maybe this is ok regardless?
    }
}

function ht_get_specials(aEvent) {
    var POST_params = {
        "classifications": ["stat", "quest", "convertible"],
        "action": "get_items_by_classification",
        "uh": uObj.unique_hash
    };

    var inventory_port = chrome.runtime.connect({name: "post_inventory_request"});
    var ht_request = 'http://www.mousehuntgame.com/managers/ajax/users/userInventory.php';

    //Display a loading hidden items image unless it already exists.
    if ($('#spinner').length == 0) {
        var div_loading_text = document.createElement('div');
        div_loading_text.id = 'spinner_text';
        div_loading_text.appendChild(document.createTextNode('HornTracker is retrieving hidden items...'));
        $('.inventoryPage-tagDirectory-tag:not(.all):last-child').after(div_loading_text);

        var div_loading = document.createElement('div');
        div_loading.id = 'spinner';
        $(div_loading_text).append(div_loading);
    }

    inventory_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
    inventory_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        var special_all = {};

        for (var item in enter_JSON.items) if (enter_JSON.items.hasOwnProperty(item) && item != "length") {
            var classification = enter_JSON.items[item].classification;
            var item_name = enter_JSON.items[item].name;
            switch (classification) {
                case "stat":
                case "quest":
                case "convertible":
                    //Specials
                    special_all[item_name] = enter_JSON.items[item];
                    break;
            }
        }

        var evt = document.createEvent("MutationEvents");
        evt.initMutationEvent("xhri_hidden_item", true, true, null, JSON.stringify(special_all), '', 'ht_stuff', 1);
        document.dispatchEvent(evt);
    });
}

function ht_getpage_special(aEvent) {
    /*
     page:Inventory
     page_arguments[tab]:special
     last_read_journal_entry_id:130601
     */
    var POST_params = {
        "page_arguments": {"tab": "special"},
        "page": "Inventory",
        "last_read_journal_entry_id": 1,
        "uh": uObj.unique_hash
    };

    var page_port = chrome.runtime.connect({name: "page_special_request"});
    var ht_request = 'http://www.mousehuntgame.com/managers/ajax/pages/page.php';

    page_port.postMessage(
        {
            request: _request_type,
            url: ht_request,
            params: POST_params,
            specials_from_inventory_request: JSON.parse(aEvent.prevValue)
        });

    page_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        for (var i in enter_JSON.page.tabs) {
            var tab = enter_JSON.page.tabs[i];
            if (tab.initialized) {
                switch (tab.name.toLowerCase()) {
                    case "special":
                        var evt = document.createEvent("MutationEvents");
                        evt.initMutationEvent("xhri_populate_hidden_specials", true, true, null, JSON.stringify(tab), JSON.stringify(msg.original_msg.specials_from_inventory_request), 'ht_stuff', 1);
                        document.dispatchEvent(evt);
                        break;
                    default:
                        appendStatus('Expected special tab information, and got something else: ' + tab.name);
                        return;
                        break;
                }
            }
        }
    });
}

function ht_2017_winter_hunt(aEvent) {
    var whObj = JSON.parse(aEvent.newValue);

    //The entire HTML of the response.
    //var whContent = whObj.messageData.message_model.messages[0].messageData.content;

    var ht_params = {
        winterHunt: JSON.stringify(whObj),
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    
    var ht_request = 'http://www.horntracker.com/backend/submit/gwhsnowman.php';

    var gwh_snowman_port = chrome.runtime.connect({name: "post_snowman_request"});

    gwh_snowman_port.postMessage({request: "postrequest", url: ht_request, ht_post_params: ht_params});
    gwh_snowman_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html)) {
            enter_JSON = JSON.parse(msg.html);
            appendStatus("Snowman return submitted.");
        }
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }
    });
    gwh_snowman_port.onDisconnect.addListener(function (event) {
    });
}

function ht_crown_calc(collection_array, fb_id, fb_name, optin_option_name, requestURL) {
    //If we're opted in to optin_option_name, send crowns in storage.
    if (is_set(_cur_user_options)) {
        //optin_option_name enabled.
        if (_cur_user_options[optin_option_name]) {
            var collection_port = chrome.runtime.connect({name: "post_collection_request"});

            var ht_params = {
                'submission_array': collection_array,
                'goal': '{"id":"' + _cur_user_options.ht_is_opted_in_goals_amount + '"}',
                'version': '{"id":"' + _version + '"}',
                'curpage': '{"id":"' + document.URL + '"}'
            };



            collection_port.postMessage({
                request: "postrequest",
                url: requestURL,
                ht_post_params: ht_params,
                lastHT: _last_ht_seen,
                fbid: fb_id,
                default_name: fb_name,
                hunters_online: _hunters_online,
                hunters_online_date: _hunters_online_date
            });

            collection_port.onMessage.addListener(function (msg) {

                var enter_JSON = {};
                if (is_JSON_string(msg.html))
                    enter_JSON = JSON.parse(msg.html);
                else {
                    appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                    return;
                }

                _user_profile.goals = enter_JSON;
            });
        }
    }
}

function populateJournals(fbid) {
    //TODO: Add progress bar indicating we're waiting for data.

    var journal_port = chrome.runtime.connect({name: "journal_profile_request"});
    var ht_request = 'http://horntracker.com/backend/profile.php?functionCall=journal&fbid_1=' + _cur_user_id + '&fbid_2=' + fbid;
    journal_port.postMessage({request: "getrequest", url: ht_request});
    journal_port.onMessage.addListener(function (msg) {

        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        var journal_tab = $($('#tabbarControls_page li')[3]).clone(false);

        $('a', journal_tab).attr('onclick', 'app.views.TabBarView.page.show(4); return false;');
        $('a', journal_tab).text('Summaries');

        $('div', journal_tab).addClass('ht_control');

        $('#tabbarControls_page ul').append(journal_tab);

        //Create the content page.
        var journal_content = $($('#tabbarContent_page > div')[3]).clone(false);
        journal_content.attr('id', 'tabbarContent_page_4');

        journal_content.removeClass('active');
        journal_content.addClass('inactive');

        $('#tabbarContent_page').append(journal_content);

        journal_tab.click(function () {
            //Populate the profile summary information
            resetTableJournals(enter_JSON.data);
        });
    });
}

function setupHTMarker(fbid) {
    if (_setupHTMarkerCalled)
        return;

    _setupHTMarkerCalled = true;
    if (document.getElementById('ht_version') == null) {
        var marker = document.createElement("div");
        marker.setAttribute('id', 'ht_version');
        marker.setAttribute('version', _version);
        marker.setAttribute('user', fbid);
        document.body.appendChild(marker);
    }
    else {
        _is_active = false;
        _inactive_message = 'Version ' + _version + ' of the extension has been disabled due to an existing instance of HornTracker installed at the same time.</br>Please only run one instance of HornTracker at a time.';
    }
}

function addUserVarToHTMarker(user) {
    if ($('#ht_version').length > 0) {
        //Remove some data.
        delete user.unique_hash;
        delete user.user_id;

        $('#ht_version').attr('uservar', JSON.stringify(user));
        $('#ht_version').attr('uservar_updated', Date.now());
    }
}

function setupIntercept(jsPath) {
    //Necessary for current implementation. Should be changed ASAP.
    var old = document.getElementById('uploadScript');
    if (old != null) {
    }
    else {
        var script = document.createElement('script');

        script.id = 'uploadScript';
        script.type = 'text/javascript';
        script.src = jsPath;

        document.head.appendChild(script);
    }


    document.addEventListener("ht_submit", ht_submit_event, false);
    document.addEventListener("ht_convertible", ht_submit_convertible, false);
    document.addEventListener("ht_trap_change", ht_notice_trap_change, false);
    document.addEventListener("ht_effectiveness", ht_submit_effectiveness, false);

    //The beta UI doesn't need this as we can rely on the event from the TEM not being wiped.
    //Non beta UI method.
    if ($('a.campPage-trap-trapEffectiveness').length == 0)
        document.addEventListener("ht_effectiveness", ht_mod_TEM, false);

    document.addEventListener("ht_group_effectiveness", ht_submit_group_effectiveness, false);
    document.addEventListener("ht_crafting", ht_submit_crafting, false);
    document.addEventListener("ht_inventory", ht_submit_inventory, false);
    //NOTE: Removed for now as there are no settings for this feature.
    //document.addEventListener("ht_treasure", ht_relic_hunter, false);
    document.addEventListener("ht_fb_user", ht_get_fb_user, false);
    document.addEventListener("ht_getstat", ht_getstat, false);
    document.addEventListener("ht_getuser", ht_get_user_no_callback, false);
    document.addEventListener("ht_marketplace", ht_submit_marketplace, false);
    document.addEventListener("ht_getspecials", ht_get_specials, false);
    document.addEventListener("ht_getpage_special", ht_getpage_special, false);
    document.addEventListener("ht_2017_winter_hunt", ht_2017_winter_hunt, false);    

    //Intercept for items tab changing.
    $('#tabbarControls_page ul li a:contains("Items")').on('click', itemtabEntered);

    //Intercept for mice tab changing.
    $('#tabbarControls_page ul li a:contains("Mice")').on('click', micetabEntered);

    //Intercept for King's Crown tab changing.
    $('#tabbarControls_page ul li a:contains("King\'s Crowns")').on('click', crowntabEntered);

    //Intercept for Mice badges.
    $('div.MiceCaughtViewTable').on('DOMNodeInserted', miceCaughtChanged);

    //Intercept for TEM information.
    setupEffectivenessClick();

    //Intercept for tab on inventory pages changing.
    $('li.inventory a').on('click', function (e) {
        //TODO: Should wait for the $('a.mousehuntHud-page-tabHeader') to exist first.
        setTimeout(function () {
            check_pages();
        }, 2000);
    });
}

function itemtabEntered(event) {
    if (_cur_user_options.ht_is_opted_in_profile) {
        $('#tabbarContent_page_3').on('DOMNodeInserted', itemtabChanged);
        var profile_user_id = document.URL.match(/profile.php\?snuid=(\d+)/g)[0].replace('profile.php?snuid=', '');

        if (_cur_user_id == profile_user_id) {
            if (_cur_user_options.ht_is_opted_in_cheese) {
                if (!is_set(_user_profile.cheese))
                    setCheese(profile_user_id);
            }

            if (_cur_user_options.ht_is_opted_in_potions) {
                if (!is_set(_user_profile.potion))
                    setPotion(profile_user_id);
            }

            if (_cur_user_options.ht_is_opted_in_charms) {
                if (!is_set(_user_profile.charm))
                    setCharm(profile_user_id);
            }

            if (_cur_user_options.ht_is_opted_in_special) {
                if (!is_set(_user_profile.special))
                    setSpecial(profile_user_id);
            }
        }
        else {
            if (!is_set(_user_profile.cheese))
                setCheese(profile_user_id);

            if (!is_set(_user_profile.potion))
                setPotion(profile_user_id);

            if (!is_set(_user_profile.charm))
                setCharm(profile_user_id);

            if (!is_set(_user_profile.special))
                setSpecial(profile_user_id);
        }
    }
}

function micetabEntered(event) {
    if (_cur_user_options.ht_is_opted_in_profile) {
        //TODO: Check if this .on already exists from itemtabEntered?
        $('#tabbarContent_page_1').on('DOMNodeInserted', micetabChanged);
        var profile_user_id = document.URL.match(/profile.php\?snuid=(\d+)/g)[0].replace('profile.php?snuid=', '');

        if (_cur_user_id == profile_user_id) {
            if (_cur_user_options.ht_is_opted_in_tacky) {
                if (!is_set(_user_profile.tacky))
                    setTacky(profile_user_id);
                else {
                    //Check to see if our tacky glue li is active.
                    if ($('#MiceCaughtView li[data-tab=tacky].active').length > 0)
                        call_update_tacky();
                }
            }
        }
        else {
            if (!is_set(_user_profile.tacky))
                setTacky(profile_user_id);
            else {
                //Check to see if our tacky glue li is active.
                if ($('#MiceCaughtView li[data-tab=tacky].active').length > 0)
                    call_update_tacky();
            }
        }
    }
}

function crowntabEntered(event) {
    var profile_user_id = document.URL.match(/profile.php\?snuid=(\d+)/g)[0].replace('profile.php?snuid=', '');

    //Only allowing goals for same profiles.
    if (_cur_user_id == profile_user_id) {
        if (_cur_user_options.ht_is_opted_in_goals) {
            //Display a goals loading image unless it already exists.
            if ($('#spinner').length == 0) {
                var div_loading_text = document.createElement('div');
                div_loading_text.id = 'spinner_text';
                div_loading_text.appendChild(document.createTextNode('Loading goals...'));
                $('#tabbarContent_page_2 > div').prepend(div_loading_text);

                var div_loading = document.createElement('div');
                div_loading.id = 'spinner';
                $('#tabbarContent_page_2 > div').prepend(div_loading);
            }

            if (!is_set(_user_profile.goals)) {
                //Wait until complete.
                goalTO = setTimeout(function () {
                    crowntabEntered(null)
                }, 5000);
            }
            else {
                //Check to see if our crowns are available.
                if ($('#tabbarContent_page_2 div.crownheader').length > 0) {
                    call_update_goals();
                    $('#spinner').remove();
                    $('#spinner_text').remove();
                }
                else {
                    goalTO = setTimeout(function () {
                        crowntabEntered(null)
                    }, 1000);
                }

            }
        }
    }
}

function miceCaughtChanged(event) {
    if ($('table.micecaughttable').length == 0)
        return;

    var profile_user_id = document.URL.match(/profile.php\?snuid=(\d+)/g)[0].replace('profile.php?snuid=', '');

    //Use mouse badge here to show this.
    if (_cur_user_options.ht_is_mouse_badge) {
        if (!is_set(_user_profile.badges)) {
            var badge_port = chrome.runtime.connect({name: "mouse_badge_request"});
            var ht_request = 'http://horntracker.com/backend/profile.php?functionCall=mousebadge&fbid_1=' + _cur_user_id + '&fbid_2=' + profile_user_id;
            badge_port.postMessage({request: "getrequest", url: ht_request});
            badge_port.onMessage.addListener(function (msg) {

                var enter_JSON = {};
                if (is_JSON_string(msg.html))
                    enter_JSON = JSON.parse(msg.html);
                else {
                    appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                    return;
                }

                _user_profile.badges = enter_JSON;
                updateBadges();
            });
        }
        else
            updateBadges();
    }
}

function updateBadges() {
    //$('table.micecaughttable tr td.label:contains(mouse)')
    for (var mouse in _user_profile.badges.data) {
        $('table.micecaughttable tr td.label:contains("' + mouse + '")').each(function () {
            var full_mouse_name = $(this).text();
            if (full_mouse_name == mouse)
                $('table.micecaughttable tr td.label:contains("' + full_mouse_name + '")').next().addClass('ht_mousebadge_' + _user_profile.badges.data[mouse].position);

            //NOTE: Not currently used.
            /*
             if (_user_profile.badges.data[mouse].position == 1)
             {
             Fireworks.createParticle(null, null, null, null, null);
             }
             */
        });
    }
}

function setCheese(profile_user_id) {
    var inv_port = chrome.runtime.connect({name: "cheese_profile_request"});
    var ht_request = 'http://horntracker.com/backend/profile.php?functionCall=cheese&fbid_1=' + _cur_user_id + '&fbid_2=' + profile_user_id;
    inv_port.postMessage({request: "getrequest", url: ht_request});
    inv_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        _user_profile.cheese = enter_JSON;
    });
}

function setPotion(profile_user_id) {
    var inv_port = chrome.runtime.connect({name: "potion_profile_request"});
    var ht_request = 'http://horntracker.com/backend/profile.php?functionCall=potion&fbid_1=' + _cur_user_id + '&fbid_2=' + profile_user_id;
    inv_port.postMessage({request: "getrequest", url: ht_request});
    inv_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        _user_profile.potion = enter_JSON;
    });
}

function setCharm(profile_user_id) {
    var inv_port = chrome.runtime.connect({name: "charm_profile_request"});
    var ht_request = 'http://horntracker.com/backend/profile.php?functionCall=charm&fbid_1=' + _cur_user_id + '&fbid_2=' + profile_user_id;
    inv_port.postMessage({request: "getrequest", url: ht_request});
    inv_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        _user_profile.charm = enter_JSON;
    });
}

function setSpecial(profile_user_id) {
    var inv_port = chrome.runtime.connect({name: "special_profile_request"});
    var ht_request = 'http://horntracker.com/backend/profile.php?functionCall=special&fbid_1=' + _cur_user_id + '&fbid_2=' + profile_user_id;
    inv_port.postMessage({request: "getrequest", url: ht_request});
    inv_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        _user_profile.special = enter_JSON;
    });
}

function setTacky(profile_user_id) {
    var inv_port = chrome.runtime.connect({name: "tacky_profile_request"});
    var ht_request = 'http://horntracker.com/backend/profile.php?functionCall=tackyglue&fbid_1=' + _cur_user_id + '&fbid_2=' + profile_user_id;
    inv_port.postMessage({request: "getrequest", url: ht_request});
    inv_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        _user_profile.tacky = enter_JSON;
    });
}

function itemtabChanged(event) {
    //This is where we add our profile tabs now.
    if (_cur_user_options.ht_is_opted_in_cheese) {
        if ($('#tabbarContent_page div.active ul li a:contains("Cheeses")').length > 0) {
            //Do nothing?
        }
        else {
            var cheeseTab = $($('#tabbarContent_page div.active ul li.inactive')[0]).clone(true);

            /*<a href="#" data-category="skins">   Skins<br>
             <img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
             25 of 25 (+2) LE  
             </a>
             */
            $('a', cheeseTab).empty();
            $('a', cheeseTab).data('category', "cheese");
            if (is_set(_user_profile.cheese)) {
                //<img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
                $('a', cheeseTab).append(document.createTextNode('Cheeses'));
                $('a', cheeseTab).append(document.createElement('br'));
                if ((_user_profile.cheese.data.length - _user_profile.cheese.missing) == _user_profile.cheese.total) {
                    var check = document.createElement('img');
                    check.src = 'https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png';
                    $(check).css('float', 'right');
                    $(check).css('margin-right', '-23px');
                    $(check).css('margin-top', '-12px');
                    $('a', cheeseTab).append(check);
                }

                $('a', cheeseTab).append(document.createTextNode((_user_profile.cheese.data.length - _user_profile.cheese.missing) + ' of ' + _user_profile.cheese.total));
            }
            else {
                //TODO: How do we handle when this data comes in?
                $('a', cheeseTab).text("Cheeses [#]");
            }

            cheeseTab.addClass('ht_profile_tab');

            $('a', cheeseTab).on('click', function (event) {
                if ($(this).parent().parent().hasClass('active')) {
                    event.preventDefault();
                    return;
                }

                $('#tabbarContent_page div.active ul li.active').addClass('inactive');
                $('#tabbarContent_page div.active ul li.active').removeClass('active');

                //Clear out everything in the container tab and fill it ourselves.
                $(this).parent().parent().removeClass('inactive');
                $(this).parent().parent().addClass('active');

                $('.itemImageBoxes').empty();

                //Fill .itemImageBoxes
                if (is_set(_user_profile.cheese.itemImageBoxes) && _user_profile.cheese.itemImageBoxes.children().length != 0) {
                    var iib_parent = $('.itemImageBoxes').parent();
                    $('.itemImageBoxes').remove();
                    iib_parent.append(_user_profile.cheese.itemImageBoxes);
                }
                else {
                    for (var inv_item in _user_profile.cheese.data)
                        createDivOnItemImageBoxes(_user_profile.cheese.data[inv_item]);

                    _user_profile.cheese.itemImageBoxes = $('.itemImageBoxes').clone(false);
                }

                event.preventDefault();
            });

            //Modify cheeseTab to use HT stuff.
            $('#tabbarContent_page div.active ul').append(cheeseTab);
        }
    }

    if (_cur_user_options.ht_is_opted_in_potions) {
        if ($('#tabbarContent_page div.active ul li a:contains("Potions")').length > 0) {
            //Do nothing?
        }
        else {
            var potionTab = $($('#tabbarContent_page div.active ul li.inactive')[0]).clone(true);

            /*<a href="#" data-category="skins">   Skins<br>
             <img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
             25 of 25 (+2) LE  
             </a>
             */
            $('a', potionTab).empty();
            $('a', potionTab).data('category', "potion");
            if (is_set(_user_profile.potion)) {
                //<img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
                $('a', potionTab).append(document.createTextNode('Potions'));
                $('a', potionTab).append(document.createElement('br'));

                if ((_user_profile.potion.data.length - _user_profile.potion.missing) == _user_profile.potion.total) {
                    var check = document.createElement('img');
                    check.src = 'https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png';
                    $(check).css('float', 'right');
                    $(check).css('margin-right', '-23px');
                    $(check).css('margin-top', '-12px');
                    $('a', potionTab).append(check);
                }

                $('a', potionTab).append(document.createTextNode((_user_profile.potion.data.length - _user_profile.potion.missing) + ' of ' + _user_profile.potion.total));
            }
            else {
                //TODO: How do we handle when this data comes in?
                $('a', potionTab).text("Potions [#]");
            }

            potionTab.addClass('ht_profile_tab');

            $('a', potionTab).on('click', function (event) {
                if ($(this).parent().parent().hasClass('active')) {
                    event.preventDefault();
                    return;
                }

                $('#tabbarContent_page div.active ul li.active').addClass('inactive');
                $('#tabbarContent_page div.active ul li.active').removeClass('active');

                //Clear out everything in the container tab and fill it ourselves.
                $(this).parent().parent().removeClass('inactive');
                $(this).parent().parent().addClass('active');

                $('.itemImageBoxes').empty();

                //Fill .itemImageBoxes
                if (is_set(_user_profile.potion.itemImageBoxes) && _user_profile.potion.itemImageBoxes.children().length != 0) {
                    var iib_parent = $('.itemImageBoxes').parent();
                    $('.itemImageBoxes').remove();
                    iib_parent.append(_user_profile.potion.itemImageBoxes);
                }
                else {
                    for (var inv_item in _user_profile.potion.data)
                        createDivOnItemImageBoxes(_user_profile.potion.data[inv_item]);

                    _user_profile.potion.itemImageBoxes = $('.itemImageBoxes').clone(false);
                }

                event.preventDefault();
            });

            //Modify potionTab to use HT stuff.
            $('#tabbarContent_page div.active ul').append(potionTab);
        }
    }

    if (_cur_user_options.ht_is_opted_in_charms) {
        if ($('#tabbarContent_page div.active ul li a:contains("Charms")').length > 0) {
            //Do nothing?
        }
        else {
            var charmTab = $($('#tabbarContent_page div.active ul li.inactive')[0]).clone(true);

            /*<a href="#" data-category="skins">   Skins<br>
             <img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
             25 of 25 (+2) LE  
             </a>
             */
            $('a', charmTab).empty();
            $('a', charmTab).data('category', "charm");
            if (is_set(_user_profile.charm)) {
                //<img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
                $('a', charmTab).append(document.createTextNode('Charms'));
                $('a', charmTab).append(document.createElement('br'));

                if ((_user_profile.charm.data.length - _user_profile.charm.missing) == _user_profile.charm.total) {
                    var check = document.createElement('img');
                    check.src = 'https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png';
                    $(check).css('float', 'right');
                    $(check).css('margin-right', '-23px');
                    $(check).css('margin-top', '-12px');
                    $('a', charmTab).append(check);
                }

                $('a', charmTab).append(document.createTextNode((_user_profile.charm.data.length - _user_profile.charm.missing) + ' of ' + _user_profile.charm.total));
            }
            else {
                //TODO: How do we handle when this data comes in?
                $('a', charmTab).text("Charms [#]");
            }

            charmTab.addClass('ht_profile_tab');

            $('a', charmTab).on('click', function (event) {
                if ($(this).parent().parent().hasClass('active')) {
                    event.preventDefault();
                    return;
                }

                $('#tabbarContent_page div.active ul li.active').addClass('inactive');
                $('#tabbarContent_page div.active ul li.active').removeClass('active');

                //Clear out everything in the container tab and fill it ourselves.
                $(this).parent().parent().removeClass('inactive');
                $(this).parent().parent().addClass('active');

                $('.itemImageBoxes').empty();

                //Fill .itemImageBoxes
                if (is_set(_user_profile.charm.itemImageBoxes) && _user_profile.charm.itemImageBoxes.children().length != 0) {
                    var iib_parent = $('.itemImageBoxes').parent();
                    $('.itemImageBoxes').remove();
                    iib_parent.append(_user_profile.charm.itemImageBoxes);
                }
                else {
                    for (var inv_item in _user_profile.charm.data)
                        createDivOnItemImageBoxes(_user_profile.charm.data[inv_item]);

                    _user_profile.charm.itemImageBoxes = $('.itemImageBoxes').clone(false);
                }

                event.preventDefault();
            });

            //Modify charmTab to use HT stuff.
            $('#tabbarContent_page div.active ul').append(charmTab);
        }
    }

    if (_cur_user_options.ht_is_opted_in_special) {
        if ($('#tabbarContent_page div.active ul li a:contains("Specials")').length > 0) {
            //Do nothing?
        }
        else {
            var specialTab = $($('#tabbarContent_page div.active ul li.inactive')[0]).clone(true);

            /*<a href="#" data-category="skins">   Skins<br>
             <img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
             25 of 25 (+2) LE  
             </a>
             */
            $('a', specialTab).empty();
            $('a', specialTab).data('category', "specials");
            if (is_set(_user_profile.special)) {
                //<img src="https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png" style="float: right; margin-right: -23px; margin-top: -12px;">
                $('a', specialTab).append(document.createTextNode('Specials'));
                $('a', specialTab).append(document.createElement('br'));

                if ((_user_profile.special.data.length - _user_profile.special.missing) == _user_profile.special.total) {
                    var check = document.createElement('img');
                    check.src = 'https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png';
                    $(check).css('float', 'right');
                    $(check).css('margin-right', '-23px');
                    $(check).css('margin-top', '-12px');
                    $('a', specialTab).append(check);
                }

                $('a', specialTab).append(document.createTextNode((_user_profile.special.data.length - _user_profile.special.missing) + ' of ' + _user_profile.special.total));
            }
            else {
                //TODO: How do we handle when this data comes in?
                $('a', specialTab).text("Specials [#]");
            }

            specialTab.addClass('ht_profile_tab');

            $('a', specialTab).on('click', function (event) {
                if ($(this).parent().parent().hasClass('active')) {
                    event.preventDefault();
                    return;
                }

                $('#tabbarContent_page div.active ul li.active').addClass('inactive');
                $('#tabbarContent_page div.active ul li.active').removeClass('active');

                //Clear out everything in the container tab and fill it ourselves.
                $(this).parent().parent().removeClass('inactive');
                $(this).parent().parent().addClass('active');

                $('.itemImageBoxes').empty();

                //Fill .itemImageBoxes
                if (is_set(_user_profile.special.itemImageBoxes) && _user_profile.special.itemImageBoxes.children().length != 0) {
                    var iib_parent = $('.itemImageBoxes').parent();
                    $('.itemImageBoxes').remove();
                    iib_parent.append(_user_profile.special.itemImageBoxes);
                }
                else {
                    for (var inv_item in _user_profile.special.data)
                        createDivOnItemImageBoxes(_user_profile.special.data[inv_item]);

                    _user_profile.special.itemImageBoxes = $('.itemImageBoxes').clone(false);
                }

                event.preventDefault();
            });

            //Modify specialTab to use HT stuff.
            $('#tabbarContent_page div.active ul').append(specialTab);
        }
    }
}

function micetabChanged(event) {
    //This is where we add our tacky glue tabs now.
    if (_cur_user_options.ht_is_opted_in_tacky) {
        if ($('#MiceCaughtView li[data-tab=tacky]').length > 0) {
            //Do nothing.
        }
        else {
            var containerGroup = $($('#MiceCaughtView li[data-tab=location]')[0]).parent();
            var tackyTab = $($('#MiceCaughtView li[data-tab=location]')[0]).clone(false);

            $(tackyTab).removeClass('active');
            $(tackyTab).addClass('inactive');

            $(tackyTab).attr('data-tab', 'tacky');
            $('div.bcenter', tackyTab).text('Tacky Glue');
            $('a', tackyTab).addClass('horntracker');
            $('a', tackyTab).attr('onclick', 'app.views.MiceCaughtView.mice_catches.showLocationTab(); return false;');

            //TODO: Copy location information and then 

            $('a', tackyTab).click(function (event) {
                //app.views.MiceCaughtView.mice_catches.showLocationTab();

                $(tackyTab).removeClass('inactive');
                $(tackyTab).addClass('active');

                //Remove the active class from Group and Location
                $(tackyTab).siblings().removeClass('active');
                $(tackyTab).siblings().addClass('inactive');

                call_update_tacky();
            });

            containerGroup.append(tackyTab);

            //Modify siblings to call both show functions?
            $('a', $(tackyTab).siblings()[0]).attr('onclick', 'app.views.MiceCaughtView.mice_catches.showLocationTab(); app.views.MiceCaughtView.mice_catches.showGroupTab(); return false;');
            $('a', $(tackyTab).siblings()[1]).attr('onclick', 'app.views.MiceCaughtView.mice_catches.showGroupTab(); app.views.MiceCaughtView.mice_catches.showLocationTab(); return false;');

            /*
             <li class="active" data-tab="location"><div class="control">
             <a href="#" onclick="app.views.MiceCaughtView.mice_catches.showLocationTab(); return false;" class="bubbletab clear-block">
             <div class="bleft"></div>
             <div class="bcenter">Location</div>
             <div class="bright"></div>
             </a>
             </div>
             </li>
             */
        }
    }
}

function ht_getstat(aEvent) {
    //Determine if we're on the profile page, with mouse tab up, and tacky clicked.
    if ($('#MiceCaughtView li[data-tab=tacky].active').length > 0) {
        //We need to delay about a second.
        timeout_tack = setTimeout(function () {
            call_update_tacky()
        }, 500);
    }
}

function call_update_tacky() {
    //Nothing to do here, move along.
    if ($('div.micecaughttable tr th.statheader').length == 0)
        return;

    //Update the statheader lines
    var tacky_cheese = $($('div.micecaughttable tr th.statheader')[0]).clone(false);
    var last_caught = $($('div.micecaughttable tr th.statheader')[0]).clone(false);

    $('div.micecaughttable tr th.statheader')[2].innerText = 'Base';
    $('div.micecaughttable tr th.statheader')[3].innerText = 'Charm';

    tacky_cheese.text('Cheese');
    last_caught.text('Last Caught');

    $('div.micecaughttable tr th.statheader').parent().append(tacky_cheese);
    $('div.micecaughttable tr th.statheader').parent().append(last_caught);

    //Update individual location categories.
    $('.MiceCaughtViewCategories > div > li a').each(function () {

        $(this).click(function () {
            setTimeout(function () {
                call_update_tacky()
            }, 50);
        });

        var display = this.text;
        //this.innerHTML.split('<br>')
        //display = "Acolyte Realm   <br>   11 of 13"

        var disp_array = this.innerHTML.split('<br>')

        var display_location = disp_array[0].trim();
        var display_count = disp_array[1].trim();

        //display_count = "11 of 13"

        var display_count_of = display_count.substr(display_count.search('of'));

        if (is_set(_user_profile.tacky.data[display_location])) {
            this.innerHTML = display_location + '<br>' + _user_profile.tacky.data[display_location].count + ' ' + display_count_of;

            if (_user_profile.tacky.data[display_location].count == parseInt(display_count_of.substring(3))) {
                //Add checkmark.
                var check = document.createElement('img');
                check.src = 'https://www.mousehuntgame.com//images/icons/checkmark_greenglow.png';
                $(check).css('float', 'right');
                $(check).css('margin-right', '-23px');
                $(check).css('margin-top', '-12px');
                $(this).append(check);
            }
        }
        else
            this.innerHTML = display_location + '<br>0 ' + display_count_of;
    });

    //Now, update each individual mouse within those location categories.
    var cur_text = $('.MiceCaughtViewCategories > div > li.active a')[0].innerText;
    var cur_location = cur_text.substr(0, cur_text.search('\n'));

    if (is_set(_user_profile.tacky.data[cur_location])) {
        //Roll through our profile data and update the mice seen.
        $('div.micecaughttable tr:has(td.label)').each(function () {
            if (is_set(_user_profile.tacky.data[cur_location][$('td', this)[1].innerText])) {
                //TODO: Make these a pic instead of text.
                var cur_mse = _user_profile.tacky.data[cur_location][$('td', this)[1].innerText];

                if (cur_mse.catches > 0) {
                    this.classList.add('caughtMouse');
                    this.classList.remove('notCaughtMouse');
                }
                else {
                    this.classList.remove('caughtMouse');
                    this.classList.add('notCaughtMouse');
                }

                $('td', this)[2].innerText = cur_mse.catches;
                $('td', this)[3].innerText = cur_mse.misses;

                $('td', this)[4].innerText = '';
                var tacky_base_img = document.createElement('img');
                tacky_base_img.classList.add('ht_small');
                tacky_base_img.src = cur_mse.basethumb;
                $(tacky_base_img).attr('title', cur_mse.base);
                $('td', this)[4].appendChild(tacky_base_img);

                $('td', this)[5].innerText = '';
                var tacky_charm_img = document.createElement('img');
                tacky_charm_img.classList.add('ht_small');
                tacky_charm_img.src = cur_mse.charmthumb;
                $(tacky_charm_img).attr('title', cur_mse.charm);
                $('td', this)[5].appendChild(tacky_charm_img);

                var mouse_cheese = $($('td', this)[5]).clone(false);
                mouse_cheese.text('');

                var tacky_cheese_img = document.createElement('img');
                tacky_cheese_img.classList.add('ht_small');
                tacky_cheese_img.src = cur_mse.cheesethumb;
                $(tacky_cheese_img).attr('title', cur_mse.cheese);

                $('td', this).parent().append(mouse_cheese);

                mouse_cheese.append(tacky_cheese_img);

                var mouse_last_caught = $($('td', this)[5]).clone(false);
                if (cur_mse.catches > 0)
                    mouse_last_caught.text(formatTimeStamp(cur_mse.since));
                else
                    mouse_last_caught.text('Never');

                $('td', this).parent().append(mouse_last_caught);
            }
            else {
                this.classList.remove('caughtMouse');
                this.classList.add('notCaughtMouse');

                //0 caught of this mouse.
                $('td', this)[2].innerText = '0';
                $('td', this)[3].innerText = '0';

                $('td', this)[4].innerText = 'N/A';
                $('td', this)[5].innerText = 'N/A';

                var mouse_cheese = $($('td', this)[5]).clone(false);
                mouse_cheese.text('N/A');

                $('td', this).parent().append(mouse_cheese);

                var mouse_last_caught = $($('td', this)[5]).clone(false);
                mouse_last_caught.text('Never');

                $('td', this).parent().append(mouse_last_caught);
            }
        });
    }
    else {
        //No mouse has been seen.
        $('div.micecaughttable tr:has(td.label)').addClass('notCaughtMouse');
        $('div.micecaughttable tr:has(td.label)').removeClass('caughtMouse');

        $('div.micecaughttable tr:has(td.label)').each(function () {
            //0 caught and missed of this mouse.
            $('td', this)[2].innerText = '0';
            $('td', this)[3].innerText = '0';

            $('td', this)[4].innerText = 'N/A';
            $('td', this)[5].innerText = 'N/A';

            var mouse_cheese = $($('td', this)[5]).clone(false);
            mouse_cheese.text('N/A');

            $('td', this).parent().append(mouse_cheese);

            var mouse_last_caught = $($('td', this)[5]).clone(false);
            mouse_last_caught.text('Never');

            $('td', this).parent().append(mouse_last_caught);
        });
    }
}

function call_update_goals() {
    $('div.mousebox').hover(
        function (e) {
            var goal_diag = document.createElement('div');
            goal_diag.classList.add('ht_goal');

            var m_name = $('div.thumb >img', this).attr('title');
            if (!is_set(_user_profile.goals.mouse))
                return;

            if (is_set(_user_profile.goals.mouse[$('div.thumb >img', this).attr('title')])) {
                var m_goal = _user_profile.goals.mouse[$('div.thumb >img', this).attr('title')];

                var m_goal_p = document.createElement('p');

                m_goal_p.appendChild(document.createTextNode('Suggested location - cheese - charm'));
                m_goal_p.appendChild(document.createElement('hr'));
                m_goal_p.appendChild(document.createTextNode(m_goal.best_cr_setup));
                m_goal_p.appendChild(document.createElement('hr'));
                m_goal_p.appendChild(document.createTextNode('Estimated hunts: ' + Math.ceil(m_goal.hunts_until_goal)));

                goal_diag.appendChild(m_goal_p);
            }
            else {
                var m_goal_p = document.createElement('p');
                m_goal_p.appendChild(document.createTextNode('Goal met, or no information.'));
                goal_diag.appendChild(m_goal_p);
            }

            $(goal_diag).css('left', (e.clientX) + 'px');
            $(goal_diag).css('top', (e.clientY) + 'px');

            $(this).append(goal_diag);

            $(this).addClass('ht_goal_hovered');

        }, function (e) {
            $(this).find('div.ht_goal').remove();
            $(this).removeClass('ht_goal_hovered');
        }
    );
}

function createDivOnItemImageBoxes(user_var) {
    /*
     <div class="itemImageBox gotItem">
     <div class="itemImage">
     <img src="https://www.mousehuntgame.com/images/items/weapons/gray/390fef110eac9dc3d9cb626c5e392c3a.jpg?cv=196">
     <div class="quantity">0</div>
     <div class="limitedEdition"></div>
     </div>
     <div class="boxName">2010 Blastoff Trap</div>
     </div>
     */
    var iib = document.createElement('div');
    iib.classList.add('itemImageBox');
    if (user_var.quant > 0)
        iib.classList.add('gotItem');
    else
        iib.classList.add('noItem');

    var ii = document.createElement('div');
    ii.classList.add('itemImage');

    var ii_img = document.createElement('img');
    ii_img.src = user_var.thumb;

    var ii_quant = document.createElement('div');
    ii_quant.classList.add('quantity');
    ii_quant.appendChild(document.createTextNode(user_var.quant));

    //TODO: Add this?
    //<div class="limitedEdition"></div>

    ii.appendChild(ii_img);
    ii.appendChild(ii_quant);

    var box_name = document.createElement('div');
    box_name.classList.add('boxName');
    box_name.appendChild(document.createTextNode(user_var.name));

    iib.appendChild(ii);
    iib.appendChild(box_name);

    $('.itemImageBoxes').append(iib);
}

function updateLastSeen() {
    _last_ht_seen = Math.round((new Date()).getTime() / 1000);
}

function set_hunters_online() {
    try {
        _hunters_online = document.getElementsByClassName('gameinfo')[0].children[0].children[0].innerText.replace("Hunters Online: ", '').replace(',', '').replace('.', '')
        _hunters_online_date = Math.round((new Date()).getTime() / 1000);
    }
    catch (e) {
        return;
    }
}

function ht_notice_trap_change(aEvent) {
    updateLastSeen();

    if (!is_set(_cur_user_options)) {
        return;
    }

    var mhuser = JSON.parse(aEvent.newValue).user;
    uObj = mhuser;

    var loc_name = mhuser.location;
    var chs_name = mhuser.bait_name;
    var chm_name = mhuser.trinket_name;
    var chm_count = mhuser.trinket_quantity;
    var bse_name = mhuser.base_name;
    var trp_name = mhuser.weapon_name;

    if (_cur_user_options.ht_real_ar)
        ht_submit_ar_request(loc_name, chs_name, chm_name, bse_name, trp_name);

    if (_cur_user_options.ht_real_stale)
        ht_submit_stale_request(loc_name, chs_name, chm_name, bse_name, trp_name);

    /*
     if (_cur_user_options.ht_charm_not_trap)
     ht_charm_not_trap_power(chm_name, chm_count);
     */
}

function ht_charm_not_trap_power(charm, quantity) {
    var a_link = document.location.origin + '/inventory.php?tab=1';
    var chm_name = '<a href="' + a_link + '">' + charm + '</a> (' + quantity + ')';

    if ($('.hudstatlist span#hud_trapPower').length == 0)
        return;

    $('.hudstatlist span#hud_trapPower')[0].innerHTML = chm_name;

    var lbl_array = $('.hudstatlist span.hudstatlabel');
    for (var lbl in lbl_array) if (lbl_array.hasOwnProperty(lbl)) {
        if (lbl_array[lbl].innerHTML == 'Trap&nbsp;Power:')
            lbl_array[lbl].innerHTML = 'Charm:';
    }
}

function ht_submit_event(aEvent) {
    var ht_params = {
        'user_before': aEvent.prevValue,
        'hunt_return': aEvent.newValue,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = JSON.parse(aEvent.prevValue).sn_user_id;
    var fb_name = JSON.parse(aEvent.prevValue).firstname + ' ' + JSON.parse(aEvent.prevValue).lastname;

    var cc_found = false;
    var rh_found = false;
    for (var journal in JSON.parse(aEvent.newValue).journal_markup) if (JSON.parse(aEvent.newValue).journal_markup.hasOwnProperty(journal) && journal != "length") {
        if (JSON.parse(aEvent.newValue).journal_markup[journal].render_data.text.match(/Crown Collector Mouse/) !== null)
            cc_found = true;
        if (JSON.parse(aEvent.newValue).journal_markup[journal].render_data.text.match(/Relic Hunter Mouse/) !== null)
            rh_found = true;
    }

    if (cc_found) {
        var POST_params = {};

        var profile_port = chrome.runtime.connect({name: "get_crowns_request"});
        var ht_request = 'https://www.mousehuntgame.com/managers/ajax/users/profiletabs.php?action=badges&snuid=' + fb_id;

        profile_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
        profile_port.onMessage.addListener(function (msg) {
            var enter_JSON = {};
            if (is_JSON_string(msg.html))
                enter_JSON = JSON.parse(msg.html);
            else {
                appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                return;
            }

            var crown_gold = 0;
            var crown_silver = 0;
            var crown_bronze = 0;
            for (var mouse in enter_JSON.mouse_data) {
                if (Number(enter_JSON.mouse_data[mouse].num_catches) >= 500)
                    crown_gold = crown_gold + 1;
                else if (Number(enter_JSON.mouse_data[mouse].num_catches) >= 100)
                    crown_silver = crown_silver + 1;
                else if (Number(enter_JSON.mouse_data[mouse].num_catches) >= 10)
                    crown_bronze = crown_bronze + 1;
            }

            ht_submit_TEM_request();

            //Request an updated options object.
            optionsRequest(fb_id, false);

            ht_params.crowns = {'gold': crown_gold, 'silver': crown_silver, 'bronze': crown_bronze};

            ht_submit_huntrecord_realtime(ht_params, fb_id, fb_name);
        });
    }
    else if (rh_found) {
        var POST_params = {
            "classifications": ["collectible"],
            "action": "get_items_by_classification",
            "uh": uObj.unique_hash
        };

        var inventory_port = chrome.runtime.connect({name: "post_inventory_request"});
        var ht_request = 'http://www.mousehuntgame.com/managers/ajax/users/userInventory.php';

        inventory_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
        inventory_port.onMessage.addListener(function (msg) {
            var enter_JSON = {};
            if (is_JSON_string(msg.html))
                enter_JSON = JSON.parse(msg.html);
            else {
                appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                return;
            }

            var relics = 0;
            for (var item in enter_JSON.items) {
                //NOTE: We add one because the relic check occurs after we've lost it to the Relic Hunter Mouse.
                if (enter_JSON.items[item].type == 'ancient_relic_collectible')
                    relics = Number(enter_JSON.items[item].quantity) + 1;
            }

            ht_submit_TEM_request();

            //Request an updated options object.
            optionsRequest(fb_id, false);

            ht_params.relics = {'total': relics};

            ht_submit_huntrecord_realtime(ht_params, fb_id, fb_name);
        });
    }
    else {
        ht_submit_TEM_request();

        //Request an updated options object.
        optionsRequest(fb_id, false);

        ht_submit_huntrecord_realtime(ht_params, fb_id, fb_name);
    }
}

function ht_submit_huntrecord_realtime(ht_params, fb_id, fb_name) {

    ht_submit_huntrecord({
        request: "postrequest",
        url: 'http://www.horntracker.com/backend/submit/enter.php',
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: fb_id,
        default_name: fb_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
}

function ht_submit_huntrecord(post_msg) {
    var submit_port = chrome.runtime.connect({name: "post_hunt_request"});
    submit_port.postMessage(post_msg);
    submit_port.onMessage.addListener(function (msg) {
        //Default is ON, so if not set, should display.
        var display_on = true;

        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);

            updateLastSeen();

            //Store locally in indexedDB.
            ht_store_hunt(msg.original_msg);

            return;
        }

        //Check for previously submitted, but failed, stored hunts to submit.
        ht_submit_failed_hunts();

        //Check to see what display value it is.
        if (is_set(_cur_user_options)) {
            if (!_cur_user_options.ht_notification)
                display_on = false;



            switch (_cur_user_options.ht_badge_text) {

                case 'ht_badge_none':
                    break;
                case 'ht_badge_rank':
                    localStorage.setItem('ht_badge_text_last_value', badge_truncate(enter_JSON.rank));

                    if (enter_JSON.rank == "1")
                        localStorage.setItem('ht_badge_text_last_color', '#FFD700');
                    else if (enter_JSON.rank == "2")
                        localStorage.setItem('ht_badge_text_last_color', '#C0C0C0');
                    else if (enter_JSON.rank == "3")
                        localStorage.setItem('ht_badge_text_last_color', '#CD7F32');
                    else
                        localStorage.setItem('ht_badge_text_last_color', '#66CDAA');

                    updateBadge(localStorage.getItem('ht_badge_text_last_value'), localStorage.getItem('ht_badge_text_last_color'));

                    break;
                case 'ht_badge_total':
                    localStorage.setItem('ht_badge_text_last_value', badge_truncate(enter_JSON.total));

                    var totalInt = parseInt(enter_JSON.total);
                    localStorage.setItem('ht_badge_text_last_color', '#66CDAA');

                    if (totalInt < 10000)
                        localStorage.setItem('ht_badge_text_last_color', '#66CDAA');
                    else if (totalInt < 20000)
                        localStorage.setItem('ht_badge_text_last_color', '#CD7F32');
                    else if (totalInt < 30000)
                        localStorage.setItem('ht_badge_text_last_color', '#C0C0C0');
                    else
                        localStorage.setItem('ht_badge_text_last_color', '#FFD700');

                    updateBadge(localStorage.getItem('ht_badge_text_last_value'), localStorage.getItem('ht_badge_text_last_color'));
                    break;
                case 'ht_badge_24':
                    localStorage.setItem('ht_badge_text_last_value', badge_truncate(enter_JSON.tfour));

                    var t4Int = parseInt(enter_JSON.tfour);
                    localStorage.setItem('ht_badge_text_last_color', '#66CDAA');

                    if (t4Int < 30)
                        localStorage.setItem('ht_badge_text_last_color', '#66CDAA');
                    else if (t4Int < 60)
                        localStorage.setItem('ht_badge_text_last_color', '#CD7F32');
                    else if (t4Int < 90)
                        localStorage.setItem('ht_badge_text_last_color', '#C0C0C0');
                    else
                        localStorage.setItem('ht_badge_text_last_color', '#FFD700');

                    updateBadge(localStorage.getItem('ht_badge_text_last_value'), localStorage.getItem('ht_badge_text_last_color'));
                    break;
                case 'ht_badge_till_thousand':
                    localStorage.setItem('ht_badge_text_last_value', (1000 - (parseInt(enter_JSON.total) % 1000)).toString());
                    localStorage.setItem('ht_badge_text_last_color', '#66CDAA');

                    updateBadge(localStorage.getItem('ht_badge_text_last_value'), localStorage.getItem('ht_badge_text_last_color'));
                    break;
                case 'ht_badge_till_rank':
                    localStorage.setItem('ht_badge_text_last_value', enter_JSON.nextHunts.toString());
                    localStorage.setItem('ht_badge_text_last_color', '#66CDAA');

                    updateBadge(localStorage.getItem('ht_badge_text_last_value'), localStorage.getItem('ht_badge_text_last_color'));
                    break;
            }
        }

        if (display_on) {
            var statusMessage = '';
            if (is_set(_cur_user_options)) {
                statusMessage = '<span style="color:#1E5934">' + _cur_user_options.ht_message_style;

                statusMessage = statusMessage.replace(/%t/g, enter_JSON.entries);
                statusMessage = statusMessage.replace(/%s/g, enter_JSON.successCount);
                statusMessage = statusMessage.replace(/%f/g, enter_JSON.failedCount);
                statusMessage = statusMessage.replace(/%d/g, enter_JSON.dupeCount);
                statusMessage = statusMessage.replace(/%i/g, enter_JSON.ignoreCount);
                statusMessage = statusMessage.replace(/%r/g, enter_JSON.rank);
                statusMessage = statusMessage.replace(/%l/g, enter_JSON.total);
                statusMessage = statusMessage.replace(/%2/g, enter_JSON.tfour);
                statusMessage = statusMessage.replace(/%ocr/g, enter_JSON.orank);
                statusMessage = statusMessage.replace(/%ocl/g, enter_JSON.ototal);
                statusMessage = statusMessage.replace(/%oc2/g, enter_JSON.otfour);
                statusMessage = statusMessage.replace(/%odr/g, enter_JSON.orank.replace(',', '.'));
                statusMessage = statusMessage.replace(/%odl/g, enter_JSON.ototal.replace(',', '.'));
                statusMessage = statusMessage.replace(/%od2/g, enter_JSON.otfour.replace(',', '.'));
            }
            else {
                statusMessage = '<span style="color:#1E5934">You successfully submitted %s hunt(s)!';
                statusMessage = statusMessage.replace('%s', enter_JSON.successCount);
            }

            statusMessage += '</span></br>';

            for (var i = 0; i < enter_JSON.admin.length; i++)
                statusMessage += enter_JSON.admin[i] + '<br>';

            appendStatus(statusMessage);
        }

        updateLastSeen();
    });
}

function ht_store_hunt(postMessage) {
    var request = window.indexedDB.open("HT_DB", 2);
    request.onupgradeneeded = function (event) {
        upgradeHTDB();
    };

    request.onsuccess = function (event) {
        db = request.result;

        var newItem = [{postMessageValue: postMessage}, postMessage.lastHT];

        //Load data into storedHunts
        var transaction = db.transaction("storedHunts", "readwrite");

        transaction.oncomplete = function (event) {
            //TODO: Necessary?
        };

        transaction.onerror = function (event) {
            //TODO: Some error notification?
        };

        var objectStore = transaction.objectStore("storedHunts");

        var requestAdd = objectStore.add(newItem[0]);

        requestAdd.onsuccess = function (event) {
            //TODO: Some notification for success
        };

        requestAdd.onerror = function (event) {
            //TODO: Some error notification?
        };
    };
}

function ht_submit_failed_hunts() {
    var request = window.indexedDB.open("HT_DB", 2);
    request.onupgradeneeded = function (event) {
        upgradeHTDB();
    };

    request.onsuccess = function (event) {
        db = request.result;

        //Load data into storedHunts
        var objectStore = db.transaction('storedHunts', 'readwrite').objectStore('storedHunts');

        objectStore.records = new Array();
        objectStore.openCursor().onsuccess = function (event) {
            var cursor = event.target.result;

            if (cursor) {
                console.log(cursor.value);

                //Push hunt with this msg value to be submitted later.
                this.source.records.push(cursor.value.postMessageValue);

                //Move to next record.
                cursor.continue();
            }
            else {
                //Clear everything in our storedHunts objectStore.
                objectStore.clear();

                //TODO: Loop through records and submit for each.
                for (var rec in this.source.records) {
                    ht_submit_huntrecord(this.source.records[rec]);
                }
            }
        };
    };
}

function upgradeHTDB() {
    var db = event.target.result;

    db.onerror = function (event) {
        //TODO: Some error notification?
    };

    var objectStore = db.createObjectStore('storedHunts', {keyPath: 'postMessage', autoIncrement: true});

    objectStore.createIndex('postMessageValue', 'postMessageValue', {unique: true});
}

function ht_submit_convertible(aEvent) {
    var convertible_port = chrome.runtime.connect({name: "post_convertible_request"});

    var ht_params = {
        'convertible': aEvent.newValue,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = JSON.parse(aEvent.newValue).user.sn_user_id;
    var fb_name = JSON.parse(aEvent.newValue).user.firstname + ' ' + JSON.parse(aEvent.newValue).user.lastname;
    var ht_request = 'http://www.horntracker.com/backend/submit/convertible.php';

    convertible_port.postMessage({
        request: "postrequest",
        url: ht_request,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: fb_id,
        default_name: fb_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    convertible_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        appendStatus(enter_JSON.toDisplay);
    });
    convertible_port.onDisconnect.addListener(function (event) {
    });
}

function ht_submit_effectiveness_user(resp, displayAtZero) {
    var effectiveness_port = chrome.runtime.connect({name: "post_effectiveness_request"});

    var ht_params = {
        'effectiveness': resp,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = JSON.parse(resp).user.sn_user_id;
    var fb_name = JSON.parse(resp).user.firstname + ' ' + JSON.parse(resp).user.lastname;
    var ht_request = 'http://www.horntracker.com/backend/submit/effectiveness.php';

    effectiveness_port.postMessage({
        request: "postrequest",
        url: ht_request,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: fb_id,
        default_name: fb_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    effectiveness_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        if (displayAtZero || enter_JSON.count != 0)
            appendStatus(enter_JSON.toDisplay);
        else
            console.log(enter_JSON.toDisplay);
    });
    effectiveness_port.onDisconnect.addListener(function (event) {
    });
}

function ht_submit_effectiveness(aEvent) {
    ht_submit_effectiveness_user(aEvent.newValue, true);
}

function ht_submit_group_effectiveness(aEvent) {
    ht_submit_group_effectiveness_withobj(aEvent.newValue);
}

function ht_submit_group_effectiveness_withobj(aEvent_obj) {
    var effectiveness_group_port = chrome.runtime.connect({name: "post_effectiveness_group_request"});

    var ht_params = {
        'effectiveness_group': aEvent_obj,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = JSON.parse(aEvent_obj).user.sn_user_id;
    var fb_name = JSON.parse(aEvent_obj).user.firstname + ' ' + JSON.parse(aEvent_obj).user.lastname;
    var ht_request = 'http://www.horntracker.com/backend/submit/effectiveness.php?mode=group';

    effectiveness_group_port.postMessage({
        request: "postrequest",
        url: ht_request,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: fb_id,
        default_name: fb_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    effectiveness_group_port.onMessage.addListener(function (msg) {
        var enterJSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        appendStatus(enter_JSON.toDisplay);
    });
    effectiveness_group_port.onDisconnect.addListener(function (event) {
    });
}

function ht_submit_crafting(aEvent) {
    ht_submit_crafting_withobj(aEvent.newValue);
}

function ht_submit_crafting_withobj(aEvent_obj) {
    if (!_cur_user_options.ht_is_opted_in_crafting)
        return;

    var crafting_port = chrome.runtime.connect({name: "post_crafting_request"});

    var ht_params = {
        'crafting': aEvent_obj,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = JSON.parse(aEvent_obj).user.sn_user_id;
    var fb_name = JSON.parse(aEvent_obj).user.firstname + ' ' + JSON.parse(aEvent_obj).user.lastname;
    var ht_request = 'http://www.horntracker.com/backend/submit/crafting.php';

    crafting_port.postMessage({
        request: "postrequest",
        url: ht_request,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: fb_id,
        default_name: fb_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    crafting_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        appendStatus(enter_JSON.toDisplay);
    });
    crafting_port.onDisconnect.addListener(function (event) {
    });
}

function setupEffectivenessClick() {
    //Non beta UI method.
    if ($('a.campPage-trap-trapEffectiveness').length == 0) {
        var handler_up = false;
        sbTimeout = setTimeout(function () {
            //No link, we should just avoid this loop entirely.
            if ($('a#effectiveness').length == 0)
                return;

            var evts = jQuery._data($('a#effectiveness').get(0), "events");

            //Check if this event is on this link already?
            if (is_set(evts)) {
                if (is_set(evts.click)) {
                    for (var idx in evts.click) if (evts.click.hasOwnProperty(idx) && idx != "length" && idx != "delegateCount") {
                        if (evts.click[idx].handler.name == "TEMEntered")
                            handler_up = true;
                    }
                }
            }

            if (!handler_up)
                $('a#effectiveness').on('click', TEMEntered);

            setupEffectivenessClick();
        }, 1200);
    }
    //Beta UI method.
    else
        $('a.campPage-trap-trapEffectiveness').on('click', TEMEntered);
}

function TEMEntered(event) {
    if (is_set(_cur_user_options)) {
        if (_cur_user_options.ht_is_opted_in_modify_TEM) {
            //Non beta UI method.
            if ($('a.campPage-trap-trapEffectiveness').length == 0) {
                //If we're showing effectivness AND we already have mice images here we modify_TEM_date.
                //If we don't have mice images xhrintercept will grab it from the effectiveness message.
                if ($('div#trapSelector.showEffectiveness').length > 0 &&
                    $('a.mouse').length > 0) {
                    var mice_array = {};
                    $('a.mouse').each(function (idx, elem) {
                        mice_array[idx] = $('img', elem).attr('title');
                    });
                    modify_TEM_date(mice_array);
                }
            }
            //Beta UI method.
            else {
                if ($('.campPage-trap-blueprintContainer').width() != 0) {
                    setTimeout(function () {
                        //If we don't have any links yet, wait for loading to disappear.
                        if ($('a.campPage-trap-trapEffectiveness-mouse').length == 0)
                            TEMEntered(event);
                        else
                            getMiceArrayFromBETAUI(modify_TEM_date);
                    }, 200);
                }
            }
        }
    }
}

function getMiceArrayFromBETAUI(func) {
    var mice_array = {};
    $('a.campPage-trap-trapEffectiveness-mouse').each(function (idx, elem) {
        mice_array[idx] = $(elem).attr('title');
    });
    func(mice_array);
}

function ht_mod_TEM(aEvent) {
    if (is_set(_cur_user_options)) {
        if (_cur_user_options.ht_is_opted_in_modify_TEM) {
            var eff_obj = JSON.parse(aEvent.newValue);
            var mice_array = {};
            var cnt = 0;
            for (var eff_key in eff_obj.effectiveness) {
                for (var idx in eff_obj.effectiveness[eff_key].mice) {
                    mice_array[cnt] = eff_obj.effectiveness[eff_key].mice[idx].name;
                    cnt++;
                }
            }

            modify_TEM_date(mice_array);
        }
    }
}

function modify_TEM_date(mice_array) {
    var get_mice_info_port = chrome.runtime.connect({name: "get_mice_request"});

    var ht_params = {
        'mice': mice_array,
        'user': JSON.stringify(uObj),
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = uObj.sn_user_id;
    var fb_name = uObj.firstname + ' ' + uObj.lastname;

    var ht_request = 'http://www.horntracker.com/backend/new/geteffectiveness.php';

    //Get info first, then send if successfully retrieved.
    get_mice_info_port.postMessage({
        request: "postrequest",
        url: ht_request,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: fb_id,
        default_name: fb_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    get_mice_info_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else
            return;

        //Perhaps we do the calculations here, and then send over the raw data per mouse to xhr
        var TEM_array = getMiceWithMessage(enter_JSON.attract);

        var evt = document.createEvent("MutationEvents");
        evt.initMutationEvent("xhri_modify_TEM", true, true, null, JSON.stringify(msg.original_msg.ht_post_params.mice), JSON.stringify(TEM_array), 'ht_stuff', 1);
        document.dispatchEvent(evt);

    });
    get_mice_info_port.onDisconnect.addListener(function (event) {
    });
}

function getMiceWithMessage(att) {
    var m_array = {};
    for (var idx in att.mice) {
        var mouse = att.mice[idx];

        //TODO: Make these default strings show up in a different const.js file.
        //If _cur_user_options.ht_TEM_display_style doesn't exist, use the default.
        var msg = 'AR: %ar%</br>CR: %cr%</br>ECR: %ecr%';
        if (is_set(_cur_user_options)) {
            if (is_set(_cur_user_options.ht_TEM_display_style))
                msg = _cur_user_options.ht_TEM_display_style;
        }

        //First check to see if there is any math to do.
        while (msg.match(/{.*?}/g) !== null) {
            var msg_math = msg.match(/{.*?}/)[0].replace(/[{}]/g, '');
            //Check for commas which determine the number of decimal places to convert to.
            var to_fixed = -1;
            var fixed_split = msg_math.split(',');
            if (fixed_split.length > 1) {
                msg_math = fixed_split[0];
                to_fixed = fixed_split[1];
            }

            while (msg_math.match(/%th|%ta|%ar|%cr|%ecr|%ms|%mc|%err/g) !== null) {
                var parm = msg_math.match(/%th|%ta|%ar|%cr|%ecr|%ms|%mc|%err/)[0];
                var val = get_val_from_TEM_parm(att, mouse, parm);

                msg_math = msg_math.replace(/%th|%ta|%ar|%cr|%ecr|%ms|%mc|%err/, val);
            }
            var ans = "UNK";

            ans = math.eval(msg_math);
            if (to_fixed != -1)
                ans = ans.toFixed(to_fixed);

            msg = msg.replace(/{.*?}/, ans);
        }

        //Second replace any existing keywords.
        while (msg.match(/%th|%ta|%ar|%cr|%ecr|%ms|%mc|%err/g) !== null) {
            var parm = msg.match(/%th|%ta|%ar|%cr|%ecr|%ms|%mc|%err/)[0];
            var val = get_val_from_TEM_parm(att, mouse, parm);

            msg = msg.replace(/%th|%ta|%ar|%cr|%ecr|%ms|%mc|%err/, val);
        }

        m_array[mouse.fullname] = msg;
    }

    return m_array;
}

function get_val_from_TEM_parm(overall, mouse, str) {
    switch (str) {
        case "%th":
            return parseInt(overall.totalHunts);
        case "%ta":
            return parseInt(overall.totalAttracted);
        case "%ar":
            return parseFloat(mouse.attractRate);
        case "%cr":
            return parseFloat(mouse.catchPerAttract);
        case "%ecr":
            return parseFloat(mouse.estimatedCatchRate);
        case "%ms":
            return parseInt(mouse.seen);
        case "%mc":
            return parseInt(mouse.caught);
        case "%err":
            return parseFloat(mouse.sError);
        default:
            return -1;
    }
}

function ht_submit_inventory(aEvent) {
    if (JSON.parse(aEvent.newValue).ht_submit_params.action == 'get_breakable_items')
        return false;

    var inventory_port = chrome.runtime.connect({name: "post_inventory_request"});

    var ht_params = {
        'inventory': aEvent.newValue,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = JSON.parse(aEvent.newValue).user.sn_user_id;
    var fb_name = JSON.parse(aEvent.newValue).user.firstname + ' ' + JSON.parse(aEvent.newValue).user.lastname;

    //We now need to determine what inventory we have based on the classification in each item.
    var items = JSON.parse(aEvent.newValue).items;

    var special_inv = {};
    var potion_inv = {};
    var cheese_inv = {};
    var charm_inv = {};

    var special_thumb = {};
    var potion_thumb = {};
    var cheese_thumb = {};
    var charm_thumb = {};
    for (var item in items) if (items.hasOwnProperty(item) && item != "length") {
        var classification = items[item].classification;
        var item_name = items[item].name;
        var item_quant = items[item].quantity;
        var thumbnail = items[item].thumbnail;
        switch (classification) {
            case "stat":
            case "quest":
            case "convertible":
                //Specials
                special_inv[item_name] = item_quant;
                special_thumb[item_name] = thumbnail;
                break;
            case "potion":
                //Potions
                potion_inv[item_name] = item_quant;
                potion_thumb[item_name] = thumbnail;
                break;
            case "bait":
                //Cheese
                cheese_inv[item_name] = item_quant;
                cheese_thumb[item_name] = thumbnail;
                break;
            case "trinket":
                //Charms
                charm_inv[item_name] = item_quant;
                charm_thumb[item_name] = thumbnail;
                break;
        }
    }

    if (_cur_user_options.ht_is_opted_in_cheese && !is_empty(cheese_inv))
        ht_submit_from_storage(JSON.stringify(cheese_inv), JSON.stringify(cheese_thumb), fb_id, fb_name, 'ht_is_opted_in_cheese', 'http://www.horntracker.com/backend/submit/cheesesubmit.php');
    if (_cur_user_options.ht_is_opted_in_potions && !is_empty(potion_inv))
        ht_submit_from_storage(JSON.stringify(potion_inv), JSON.stringify(potion_thumb), fb_id, fb_name, 'ht_is_opted_in_potions', 'http://www.horntracker.com/backend/submit/potionsubmit.php');
    if (_cur_user_options.ht_is_opted_in_charms && !is_empty(charm_inv))
        ht_submit_from_storage(JSON.stringify(charm_inv), JSON.stringify(charm_thumb), fb_id, fb_name, 'ht_is_opted_in_charms', 'http://www.horntracker.com/backend/submit/charmsubmit.php');
    if (_cur_user_options.ht_is_opted_in_special && !is_empty(special_inv))
        ht_submit_from_storage(JSON.stringify(special_inv), JSON.stringify(special_thumb), fb_id, fb_name, 'ht_is_opted_in_special', 'http://www.horntracker.com/backend/submit/specialsubmit.php');

    /*
     var ht_request = 'http://www.horntracker.com/backend/submit/crafting.php';

     inventory_port.postMessage({request: "postrequest", url: ht_request, ht_post_params: ht_params, lastHT:_last_ht_seen, fbid:fb_id, default_name:fb_name, hunters_online: _hunters_online, hunters_online_date: _hunters_online_date});
     inventory_port.onMessage.addListener(function(msg) {
     var enter_JSON = JSON.parse(msg.html);
     appendStatus(enter_JSON.toDisplay);
     });
     inventory_port.onDisconnect.addListener(function (event) {});
     */
}

function ht_relic_hunter(aEvent) {
    var treasure_map = JSON.parse(aEvent.newValue).treasure_map;
    if (!is_set(treasure_map))
        return;

    //Don't submit treasure chest info if we have more than the uncaught group.
    // if (treasure_map.groups.length <= 1)
    //        ht_submit_treasure(aEvent);

    //Get all mice here.
    for (var key in treasure_map.groups) {
        if (treasure_map.groups[key].type == 'uncaughtmice') {
            var mice_array = treasure_map.groups[key].mice;
            var fb_id = JSON.parse(aEvent.newValue).user.sn_user_id;
            var fb_name = JSON.parse(aEvent.newValue).user.firstname + ' ' + JSON.parse(aEvent.newValue).user.lastname;

            for (var m_key in mice_array) {
                var attraction_port = chrome.runtime.connect({name: "post_attraction_request_" + mice_array[m_key].name});

                var ht_params = {
                    'mouse': mice_array[m_key].name
                };

                var ht_request = 'http://www.horntracker.com/backend/submit/mouseattraction.php';

                attraction_port.postMessage({
                    request: "postrequest",
                    url: ht_request,
                    ht_post_params: ht_params,
                    lastHT: _last_ht_seen,
                    fbid: fb_id,
                    default_name: fb_name,
                    hunters_online: _hunters_online,
                    hunters_online_date: _hunters_online_date
                });

                attraction_port.onMessage.addListener(function (msg) {
                    var enter_JSON = {};
                    if (is_JSON_string(msg.html)) {
                        enter_JSON = JSON.parse(msg.html);
                        var res = JSON.parse(enter_JSON.toDisplay);
                        var m_label = res.mouse;

                        //TODO: Append to the map the attraction info.
                        var m_div = $('.treasureMapPopup-mice-group-mouse-name span:contains("' + m_label + '")').parent();
                        var ul = $('<ul>');
                        m_div.after(ul);
                        var best_div = {
                            best_percent: -1,
                            li_div: null
                        };

                        $.each(res.best, function (key, val) {
                            //var new_div = $('<li>').addClass('ht_bestloc');
                            //ul.append(new_div);
                            //new_div.append(val.ar + '%: ' + val.loc + " - " + val.chs + " - " + val.chm)
                            if (Number(val.ar) > best_div.best_percent) {
                                best_div.best_percent = Number(val.ar);
                                best_div.li_div = $('<li>').addClass('ht_bestloc').append(val.ar + '%: ' + val.loc + " - " + val.chs + " - " + val.chm);
                            }
                        });

                        ul.append(best_div.li_div);
                        //console.log(best_div);

                    } else {
                        appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                        return;
                    }

                    //appendStatus(enter_JSON.toDisplay);
                });

                attraction_port.onDisconnect.addListener(function (event) {
                });
            }

            break;
        }
    }
}

function ht_submit_treasure(aEvent) {
    var treasure_port = chrome.runtime.connect({name: "post_treasure_request"});

    var ht_params = {
        'treasure': aEvent.newValue,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var fb_id = JSON.parse(aEvent.newValue).user.sn_user_id;
    var fb_name = JSON.parse(aEvent.newValue).user.firstname + ' ' + JSON.parse(aEvent.newValue).user.lastname;

    var ht_request = 'http://www.horntracker.com/backend/submit/treasuresubmit.php';

    treasure_port.postMessage({
        request: "postrequest",
        url: ht_request,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: fb_id,
        default_name: fb_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    treasure_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        appendStatus(enter_JSON.toDisplay);
    });
    treasure_port.onDisconnect.addListener(function (event) {
    });
}

function ht_get_user_no_callback(aEvent) {
    getUserObject(addUserVarToHTMarker);
}

function ht_submit_marketplace(aEvent) {
    var mpObj = JSON.parse(aEvent.newValue);

    var ht_params = {
        'marketplace': JSON.stringify(mpObj.details),
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var ht_request = 'http://www.horntracker.com/backend/submit/marketplace.php';

    var marketplace_port = chrome.runtime.connect({name: "post_marketplace_request"});

    marketplace_port.postMessage({request: "postrequest", url: ht_request, ht_post_params: ht_params});
    marketplace_port.onMessage.addListener(function (msg) {
    }); //TODO: Do we want to do anything here? I'm not sure we do.
    marketplace_port.onDisconnect.addListener(function (event) {
    });
}

function ht_get_fb_user(aEvent) {
    uObj = JSON.parse(aEvent.newValue);
    var cb = JSON.parse(aEvent.prevValue);

    initWithUser(uObj);
}

function ht_submit_TEM_request() {
    var POST_params = {};

    var ar_port = chrome.runtime.connect({name: "get_TEM_request"});
    var ht_request = 'http://www.mousehuntgame.com/managers/ajax/users/getmiceeffectiveness.php';

    ar_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
    ar_port.onMessage.addListener(function (msg) {
        ht_submit_effectiveness_user(msg.html, false);
    });
}

function getUserObject(callback) {
    var POST_params = {};

    var user_port = chrome.runtime.connect({name: "get_user_obj"});
    var ht_request = 'https://www.mousehuntgame.com/managers/ajax/users/data.php';

    user_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
    user_port.onMessage.addListener(function (msg) {
        if (is_JSON_string(msg.html)) {
            var retval = JSON.parse(msg.html);
            if (is_set(retval.user))
                callback(retval.user);
            else {
                _request_type = 'postrequest_fb';

                var evt = document.createEvent("MutationEvents");
                evt.initMutationEvent("xhri_get_fb_user", true, true, null, callback, callback, 'ht_stuff', 1);
                document.dispatchEvent(evt);
            }
        }
    });
}

function ht_submit_TEM_request_individual() {
    var effectiveness = $('.mousedifficulty div')[0].innerText.replace('Difficulty: ', '').replace(' (your current trap setup)', '');
    var mouse_name = $('.mousename')[0].innerText;
    var trap_power = Number(uObj.trap_power);
    var trap_powertype = uObj.trap_power_type_name;

    var trap = uObj.weapon_name;
    var base = uObj.base_name;
    var location = uObj.location;
    var charm = uObj.trinket_name;

    var effectiveness_solo_port = chrome.runtime.connect({name: "post_effectiveness_solo_request"});

    var ht_params = {
        'eff': effectiveness,
        'mouse': mouse_name,
        'power': trap_power,
        'powertype': trap_powertype,
        'weapon_name': trap,
        'base_name': base,
        'location': location,
        'trinket_name': charm,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };
    var ht_request = 'http://www.horntracker.com/backend/submit/effectiveness.php?mode=solo';

    effectiveness_solo_port.postMessage({
        request: "postrequest",
        url: ht_request,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: _cur_user_id,
        default_name: _cur_user_full_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    effectiveness_solo_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }
        appendStatus(enter_JSON.toDisplay);
    });
    effectiveness_solo_port.onDisconnect.addListener(function (event) {
    });
}

function ht_submit_from_storage(collection_array, thumb_array, fb_id, fb_name, optin_option_name, requestURL) {
    //If we're opted in to optin_option_name, send crowns in storage.
    if (is_set(_cur_user_options)) {
        //optin_option_name enabled.
        if (_cur_user_options[optin_option_name]) {
            var collection_port = chrome.runtime.connect({name: "post_collection_request"});

            var ht_params = {
                'submission_array': collection_array,
                'thumb_array': thumb_array,
                'version': '{"id":"' + _version + '"}',
                'curpage': '{"id":"' + document.URL + '"}'
            };

            collection_port.postMessage({
                request: "postrequest",
                url: requestURL,
                ht_post_params: ht_params,
                lastHT: _last_ht_seen,
                fbid: fb_id,
                default_name: fb_name,
                hunters_online: _hunters_online,
                hunters_online_date: _hunters_online_date
            });
            collection_port.onMessage.addListener(function (msg) {
                var enter_JSON = {};
                if (is_JSON_string(msg.html))
                    enter_JSON = JSON.parse(msg.html);
                else {
                    appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                    return;
                }

                appendStatus(enter_JSON.toDisplay);
            });
        }
    }
}

function ht_submit_mostmice(mice_caught, profile_user_id, requestURL) {
    var collection_port = chrome.runtime.connect({name: "post_collection_request"});

    var ht_params = {
        'submission_array': mice_caught,
        'profile_id': profile_user_id,
        'version': '{"id":"' + _version + '"}',
        'curpage': '{"id":"' + document.URL + '"}'
    };


    collection_port.postMessage({
        request: "postrequest",
        url: requestURL,
        ht_post_params: ht_params,
        lastHT: _last_ht_seen,
        fbid: _cur_user_id,
        default_name: _cur_user_full_name,
        hunters_online: _hunters_online,
        hunters_online_date: _hunters_online_date
    });
    collection_port.onMessage.addListener(function (msg) {
        //TODO: Confirm success or not here.
        appendStatus('<span style="color:#1E5934">Profile information for ' + JSON.parse(msg.original_msg.ht_post_params.submission_array).un + ' added successfully for MostMice reporting.</span></br>');
    });
}

function ht_submit_ar_request(loc_name, chs_name, chm_name, bse_name, trp_name) {
    var ar_port = chrome.runtime.connect({name: "get_ar_request"});
    var ht_request = 'http://horntracker.com/backend/getattractrate.php?functionCall=getAttractionData&loc=' + loc_name + '&chs=' + chs_name + '&chm=' + chm_name + '&bse=' + bse_name + '&trp=' + trp_name;

    ar_port.postMessage({request: "getrequest", url: ht_request});
    ar_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            //NOTE: Do not display anything, silently fail.
            //appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

		//Checks for beta UI to display in correct attraction area
		//Beta UI check
		if($('.campPage-trap-trapStat.attraction .value').length == 0){
			var percent_parsed = enter_JSON.ar + "<span>%</span>";
			var percent_error = '<div class="description">Your <b>Attraction Rate</b> is based on real data submitted by hunters like you, and calculated based on your setup with a trap attraction bonus of <b>' + (Number(uObj.trap_attraction_bonus) * 100).toFixed(0) + '%</b>.</div><div class="math clear-block"><div class="part sign "><b>+/-</b></div><div class="part "><b>' + enter_JSON.arError + '<span>%</span></b><i>Margin of Error</i></div>';
			$($('.attraction_bonus .value')[0]).html(percent_parsed);
			$($('.attraction_bonus .help')[0]).html(percent_error);
			
			$('div#trapSelectorStats').addClass('ht_ar');
		}else {
			var percent_parsed = '<span>'+ enter_JSON.ar +'%</span>';
			var percent_error = '<div class="campPage-trap-trapStat-mathRow label"><div class="campPage-trap-trapStat-mathRow-value">'+ $('.campPage-trap-trapStat.attraction .value')[0].innerText +'</div><div class="campPage-trap-trapStat-mathRow-name">Total Attraction Rate</div></div><div class="campPage-trap-trapStat-mathRow ">Your <b>Attraction Rate</b> is based on real data submitted by hunters like you, and calculated based on your setup with a trap attraction bonus of <b>' + (Number(uObj.trap_attraction_bonus) * 100).toFixed(0) + '%</b>.<b> +/- ' + enter_JSON.arError + '%</b><i>Margin of Error</i></div>';
			$($('.campPage-trap-trapStat.attraction .value')[0]).html(percent_parsed);
			$($('.campPage-trap-trapStat.attraction .math')[0]).append(percent_error);
		}
    });
}

function ht_submit_stale_request(loc_name, chs_name, chm_name, bse_name, trp_name) {
    var stale_port = chrome.runtime.connect({name: "get_stale_request"});
    var ht_request = 'http://horntracker.com/backend/getattractrate.php?functionCall=getStaleData&loc=' + loc_name + '&chs=' + chs_name + '&chm=' + chm_name + '&bse=' + bse_name + '&trp=' + trp_name;

    stale_port.postMessage({request: "getrequest", url: ht_request});
    stale_port.onMessage.addListener(function (msg) {
        var enter_JSON = {};
        if (is_JSON_string(msg.html))
            enter_JSON = JSON.parse(msg.html);
        else {
            //NOTE: Do not display anything, silently fail.
            //appendStatus('Unexpected error returned from the server: </br>' + msg.html);
            return;
        }

        //TODO: YOU ARE HERE
        //line-height: 34px;
        //font-size: 20px;

        var percent_parsed = enter_JSON.stale + "<span>%</span>";
        var percent_error = '<div class="description">Your <b>Stale Rate</b> is defined as your probability of staling on a failure to attract, and is based on real data submitted by hunters like you. It is calculated based on your setup with a trap freshness rating of <b>' + uObj.trap_cheese_effect + '</b>.</div><div class="math clear-block"><div class="part sign "><b>+/-</b></div><div class="part "><b>' + enter_JSON.staleError + '<span>%</span></b><i>Margin of Error</i></div>';

        $($('.cheese_effect .value')[0]).html(percent_parsed);
        $($('.cheese_effect .help')[0]).html(percent_error);
		
		//Checks for beta UI to display in correct cheese effect area
		//Beta UI check
		if($('.campPage-trap-trapStat.cheeseEffect .value').length == 0){
			var percent_parsed = enter_JSON.stale + "<span>%</span>";
			var percent_error = '<div class="description">Your <b>Stale Rate</b> is defined as your probability of staling on a failure to attract, and is based on real data submitted by hunters like you. It is calculated based on your setup with a trap freshness rating of <b>' + uObj.trap_cheese_effect + '</b>.</div><div class="math clear-block"><div class="part sign "><b>+/-</b></div><div class="part "><b>' + enter_JSON.staleError + '<span>%</span></b><i>Margin of Error</i></div>';
			$($('.cheese_effect .value')[0]).html(percent_parsed);
			$($('.cheese_effect .help')[0]).html(percent_error);
		}else {
			var percent_parsed = '<span>'+ enter_JSON.stale +'%</span>';
			var percent_error = '<div class="campPage-trap-trapStat-mathRow ">Your <b>Stale Rate</b> is defined as your probability of staling on a failure to attract, and is based on real data submitted by hunters like you. It is calculated based on your setup with a trap freshness rating of <b>' + uObj.trap_cheese_effect + '</b>.<b> +/- ' + enter_JSON.staleError + '%</b><i>Margin of Error</i></div>';
			$($('.campPage-trap-trapStat.cheeseEffect .value')[0]).html(percent_parsed);
			$($('.campPage-trap-trapStat.cheeseEffect .math')[0]).append(percent_error);
		}
    });
}

function ht_submit_journal_summary(journal_vars, fb_id, fb_name, requestURL) {


    //If we're opted in to optin_option_name, send journal_vars.
    if (is_set(_cur_user_options)) {
        var requestURL = 'http://www.horntracker.com/backend/submit/journalsummarysubmit.php';

        //TODO: Do we want an option for this?
        if (true) {
            var journal_summary_port = chrome.runtime.connect({name: "post_journal_summary_request"});

            var ht_params = {
                'submission_array': journal_vars,
                'version': '{"id":"' + _version + '"}',
                'curpage': '{"id":"' + document.URL + '"}'
            };


            journal_summary_port.postMessage({
                request: "postrequest",
                url: requestURL,
                ht_post_params: ht_params,
                lastHT: _last_ht_seen,
                fbid: fb_id,
                default_name: fb_name,
                hunters_online: _hunters_online,
                hunters_online_date: _hunters_online_date
            });

            journal_summary_port.onMessage.addListener(function (msg) {

                var enter_JSON = {};
                if (is_JSON_string(msg.html))
                    enter_JSON = JSON.parse(msg.html);
                else {
                    appendStatus('Unexpected error returned from the server: </br>' + msg.html);
                    return;
                }

                if (document.URL.match(/journal.php/g) !== null)
                    appendStatus(enter_JSON.toDisplay);
            });
        }
    }
}

function createStatusBox() {
    var status_box = document.createElement('div');
    status_box.classList.add('statusBox');
    status_box.setAttribute('version', _version);

    var alert_span = document.createElement('span');
    status_box.appendChild(alert_span);

    var ok_button = document.createElement('input');
    ok_button.type = 'button';
    ok_button.id = 'submit';
    ok_button.value = ' X ';
    ok_button.addEventListener('click', function () {
        //Code to remove box.
        $('.statusBox').animate({opacity: 0}, "slow", function () {
            $('.statusBox').remove();
        });
    });
    status_box.appendChild(ok_button);

    document.body.appendChild(status_box);
    $(status_box).animate({opacity: 1}, "slow");

    var box_position = Array();
    box_position[0] = "50px";
    box_position[1] = "300px";

    if (is_set(_cur_user_options)) {
        //FB position by default.
        var position_var = 'ht_box_position_fb';
        if (document.location.href.match(/mousehuntgame/g) !== null)
            position_var = 'ht_box_position_mhg';
        if (document.location.href.match(/canvas/g) !== null)
            position_var = 'ht_box_position_fb';

        if (is_set(_cur_user_options[position_var]))
            box_position = _cur_user_options[position_var].split('_');
    }

    $('.statusBox').css("top", box_position[0]);
    $('.statusBox').css("left", box_position[1]);
    $('.statusBox').css("position", "absolute");

    //Make box draggable
    $(".statusBox").draggable({
        stop: function () {
            var posn_top = $(".statusBox").css("top");
            var posn_left = $(".statusBox").css("left");

            //Set options
            updateNotificationLocation(posn_top + "_" + posn_left);

            //Request an updated options object.
            //optionsRequest(fb_id, false);
        }
    });
}

function updateNotificationLocation(ht_box_position) {
    //FB position by default.
    var position_var = 'ht_box_position_fb';
    if (document.location.href.match(/mousehuntgame/g) !== null)
        position_var = 'ht_box_position_mhg';
    if (document.location.href.match(/canvas/g) !== null)
        position_var = 'ht_box_position_fb';

    var update_notification_position_port = chrome.runtime.connect({name: "update_notification_position_request"});
    update_notification_position_port.postMessage({
        request: "update_notification_position",
        fbid: _cur_user_id,
        position: ht_box_position,
        pos_var: position_var
    });
}

function updateBadge(text, color) {
    var badge_port = chrome.runtime.connect({name: "set_badge_request"});
    badge_port.postMessage({request: "badge_update", text: text, bg_color: color});
}

//Much of this function was taken from Rohan Mehta.
function createStatus(statToDisplay) {
    createStatusBox();

    //$('#statusMessage')[0].innerHTML = "<br>" + statToDisplay;
    $('.statusBox > span').html(statToDisplay);

    //Kill the box after x seconds, where x is defaulted to 10.
    //Default is 10
    var secondsTillDeath = 10;
    if (is_set(_cur_user_options)) {
        if (isNumber(_cur_user_options.ht_notification_display_time))
            secondsTillDeath = _cur_user_options.ht_notification_display_time;
    }

    sbTimeout = setTimeout(function () {
        killStatusBox()
    }, secondsTillDeath * 1000);
}

function appendStatus(status) {
    //Default is ON, so if not set, should display.
    var display_on = true;

    //Check to see what display value it is.
    if (is_set(_cur_user_options)) {
        if (!_cur_user_options.ht_notification)
            display_on = false;
    }

    if (display_on) {
        if (document.getElementsByClassName('statusBox').length <= 0)
            createStatus(status);
        else
            $('.statusBox > span').append('<hr>' + status);
    }

    console.log(status);
}

function killStatusBox() {
    if (document.getElementsByClassName('statusBox').length > 0) {
        $('.statusBox').animate({opacity: 0}, "slow", function () {
            $('.statusBox').remove();
        });
    }
}

//TODO: These are util functions and should be in a seperate file.
function is_set(variable) {
    if (typeof variable === "undefined")
        return false;
    else
        return true;
}

function is_JSON_string(str) {
    try {
        JSON.parse(str);
    }
    catch (e) {
        return false;
    }

    return true;
}

function badge_truncate(number) {
    if (number.toString().length <= 4)
        return number;

    return Math.floor(number / 1000) + "K";
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function is_empty(obj) {

    // null and undefined are empty
    if (obj == null) return true;
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length && obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    for (var key in obj) {
        if (hasOwnProperty.call(obj, key))    return false;
    }

    return true;
}

function formatTimeStamp(unix_timestamp) {
    var date_now = new Date();
    var n = date_now.getTime() - (unix_timestamp * 1000);

    var date = new Date(n);

    return timeSinceDetail(date);
}

function timeSinceDetail(date) {
    var toReturn = '';

    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 86400);

    //Days
    if (interval >= 1) {
        if (interval == 1)
            toReturn = interval + " day, ";
        else
            toReturn = interval + " days, ";

        //return toReturn;

        seconds = seconds - (interval * 86400);
    }

    //Hours
    interval = Math.floor(seconds / 3600);
    if (interval < 10)
        interval = '0' + interval;

    toReturn += interval + ":"
    seconds = seconds - (interval * 3600);

    //Minutes
    interval = Math.floor(seconds / 60);
    if (interval < 10)
        interval = '0' + interval;

    toReturn += interval + ":"

    seconds = seconds - (interval * 60);

    //Seconds
    if (seconds < 10)
        seconds = '0' + seconds;

    toReturn += seconds + " ago.";

    return toReturn;
}

function resetTableJournals(data) {
    $('#tabbarContent_page_4').empty();

    var h = [["Timeframe", "sortable-text"],
        ["Catches", "sortable-numeric"],
        ["FTAs", "sortable-numeric"],
        ["FTCs", "sortable-numeric"],
        ["Stales", "sortable-numeric"],
        ["Gold Gained", "sortable-numeric"],
        ["Gold Lost", "sortable-numeric"],
        ["Total Gold", "sortable-numeric"],
        ["Points Gained", "sortable-numeric"],
        ["Points Lost", "sortable-numeric"],
        ["Total Points", "sortable-numeric"],
        ["Submit Time", "sortable-sortTimeMixedFormat"]];
    var table = document.getElementById("ht_journals");

    if (table)
        table.parentNode.removeChild(table);

    table = document.createElement("table");
    var tr = document.createElement("tr");
    var thead = document.createElement("thead");
    var tbody = document.createElement('tbody');
    var th = document.createElement("th");
    var trc = tr.cloneNode(true);
    var thc;

    table.id = "ht_journals";
    table.className = "no-arrow rowstyle-alt";
    table.cellspacing = table.cellpadding = 0

    $('#tabbarContent_page_4').append(table);

    thead.appendChild(trc);
    table.appendChild(thead);
    table.appendChild(tbody);
    table = null;

    for (var j = 0; j < h.length; j++) {
        thc = th.cloneNode(false);
        trc.appendChild(thc);
        thc.className = h[j][1];
        thc.appendChild(document.createTextNode(h[j][0]));
        thc = null;
    }
    ;

    trc = thead = null;

    //Roll through data
    var count = 1;
    for (var i in data) {
        //Use addCells
        trc = tr.cloneNode(false);
        tbody.appendChild(trc);

        addCellsJournal(trc, count, data[i].time, parseInt(data[i].catches), parseInt(data[i].fta), parseInt(data[i].ftc), parseInt(data[i].stale),
            parseInt(data[i].gg), parseInt(data[i].gl), parseInt(data[i].pg), parseInt(data[i].pl), data[i].func, formatTimeStamp(data[i].since));

        count++;
    }

    tbody = null;

    fdTableSort.init();

    return false;
}

function addCellsJournal(tr, order, jtext, catches, fta, ftc, stale, gg, gl, pg, pl, detail, last_seen) {
    if (order % 2 == 0) tr.className = "alt";

    var td1 = document.createElement("td");
    tr.appendChild(td1);
    var td1A = document.createElement("a");
    td1A.setAttribute('href', '#');
    $(td1A).click(function () {
        var evt = document.createEvent("MutationEvents");
        evt.initMutationEvent("xhri_journal_summary", true, true, null, detail, detail, 'ht_stuff', 1);
        document.dispatchEvent(evt);
    });
    td1.appendChild(td1A);
    td1A.appendChild(document.createTextNode(jtext));

    var td2 = document.createElement("td");
    tr.appendChild(td2);
    td2.appendChild(document.createTextNode(catches));

    var td3 = document.createElement("td");
    tr.appendChild(td3);
    td3.appendChild(document.createTextNode(fta));

    var td4 = document.createElement("td");
    tr.appendChild(td4);
    td4.appendChild(document.createTextNode(ftc));

    var td5 = document.createElement("td");
    tr.appendChild(td5);
    td5.appendChild(document.createTextNode(stale));

    var td6 = document.createElement("td");
    tr.appendChild(td6);
    td6.appendChild(document.createTextNode(gg.formatNicely(0, ',', '.')));

    var td7 = document.createElement("td");
    tr.appendChild(td7);
    td7.appendChild(document.createTextNode(gl.formatNicely(0, ',', '.')));

    var td8 = document.createElement("td");
    tr.appendChild(td8);
    td8.appendChild(document.createTextNode((gg - gl).formatNicely(0, ',', '.')));

    var td9 = document.createElement("td");
    tr.appendChild(td9);
    td9.appendChild(document.createTextNode(pg.formatNicely(0, ',', '.')));

    var td10 = document.createElement("td");
    tr.appendChild(td10);
    td10.appendChild(document.createTextNode(pl.formatNicely(0, ',', '.')));

    var td11 = document.createElement("td");
    tr.appendChild(td11);
    td11.appendChild(document.createTextNode((pg - pl).formatNicely(0, ',', '.')));

    var td12 = document.createElement("td");
    tr.appendChild(td12);
    td12.appendChild(document.createTextNode(last_seen));

    td1 = td2 = td3 = td4 = td5 = td6 = td7 = td8 = td9 = td10 = td11 = td12 = null;
}

Number.prototype.formatNicely = function (decPlaces, thouSeparator, decSeparator) {
    var n = this,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator == undefined ? "." : decSeparator,
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};