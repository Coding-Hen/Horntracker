var uObj;
var _cur_user_options;
var _request_type = 'postrequest_hg';

document.addEventListener('DOMContentLoaded', function () 
{
	document.addEventListener("ht_fb_user", ht_get_fb_user, false);
	
	//Grab the user object.
	getUserObject(initWithUser);
});

function ht_get_fb_user(aEvent)
{
	uObj = JSON.parse(aEvent.newValue);

	initWithUser(uObj);
}

function getUserObject(callback)
{
	
	var POST_params = { };

	var user_port = chrome.runtime.connect({name: "get_user_obj"});
	var ht_request = 'https://www.mousehuntgame.com/managers/ajax/users/data.php';
	user_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
	
	user_port.onMessage.addListener(function(msg) {
		if (is_JSON_string(msg.html))
		{
			var retval = JSON.parse(msg.html);
			if (is_set(retval.user))
				callback(retval.user);
			else
			{
				//TODO: This likely doesn't work, since it needs to communicate with the bg.js page.
				_request_type = 'postrequest_fb';

				var evt = document.createEvent("MutationEvents");
				evt.initMutationEvent("xhri_get_fb_user", true, true, this, '', '', 'ht_stuff', 1);
				document.dispatchEvent(evt);
			}
		}
	});
	
}

function initWithUser(resp)
{
	
	uObj = resp;
	

	var query = { active: true, currentWindow: true };
	chrome.tabs.query(query, getVisibleTab);

	//Get info from the backend about links to display.
	var links_port = chrome.runtime.connect({name: "links_request"});
	var ht_request = 'http://horntracker.com/backend/getlinks.php';
	links_port.postMessage({request: "getrequest", url: ht_request});
	links_port.onMessage.addListener(function(msg) {
		
		//Remove the loading page.
		$('div#wrap').remove();
		
		var enter_JSON = {};
		if (is_JSON_string(msg.html))
			enter_JSON = JSON.parse(msg.html);
		else
		{
			//TODO: Some sort of error or graceful way to show links? Maybe default links?
			//appendStatus('Unexpected error returned from the server: </br>' + msg.html);
			return;
		}

		//Feed the background page with the link data.
		chrome.extension.getBackgroundPage()._link_JSON = enter_JSON;

		for (var key in enter_JSON.data)
		{
			var link = enter_JSON.data[key];
			//If not is_important don't display here.
			if (link.is_important != '1')
				continue;

			$('span#ht_links').append($('<br>'));

			var a_link = $('<a>')
							.attr('title', link.title_text)
							.attr('target', '_blank')
							.attr('href', link.URL);
			a_link.append(link.link_text);

			//If is_new add the floaty fun is_new thing.
			if (link.is_new == '1')
			{
				a_link.append($('<div>')
								.addClass('isnew')
								.append('NEW!'));
			}

			$('span#ht_links').append(a_link);
		}
	});
}

function getVisibleTab(tabs) 
{
	var currentTab = tabs[0];

	if(currentTab.url.match(/horntracker\.com/) !== null)
	{
		$('body').append('<span><b>Data Viewer Interactions</b></span><hr/>');
		var a_setup = $('<a>')
						 .attr('title', 'Input my location and trap setup into the Data Viewer.')
						 .attr('target', '_blank');
		$('body').append(a_setup);
		$(a_setup).append('Input My Setup');
		$(a_setup).click(currentTab, function (e){
			
			var input_port = chrome.runtime.connect({name: "input_my_setup"});
			input_port.postMessage({request: "input_my_setup", user:uObj, tabId: e.data.id});
		});
		$('body').append($('<br>'));
		$('body').append($('<br>'));
	}
}