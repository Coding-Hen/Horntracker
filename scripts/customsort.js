var sortRateWithInputs = fdTableSort.sortNumeric;

function sortRateWithInputsPrepareData(tdNode, innerText) 
{
        // Get the innerText of the TR nodes
        var aa = innerText;
        
        // Replace anything in parens.
        aa = aa.replace(/\((.+?)\)/g, " ");
        
        // Replace anything in brackets.
        aa = aa.replace(/\[(.+?)\]/g, " ");

        // Remove spaces
        aa = aa.replace("%","").trim();

        return aa;
}

var sortTimeMixedFormat = fdTableSort.sortNumeric;

function sortTimeMixedFormatPrepareData(tdNode, innerText)
{
	// Get the innerText of the TR nodes
    var aa = innerText.replace("ago.", "").trim();

    if (aa == 'Never')
    	return 1000000000;

	if (aa.indexOf("day") !== -1)
	{
		var idays = aa.match(/[0-9]+/g);
		return Number(idays[0]) * 24 * 60 * 60;
	}

	//Should be hh:mm:ss format now.
	var aaa = aa.split(":");

	return Number((aaa[0] * 60 * 60) + (aaa[1] * 60) + Number(aaa[2]));
}
