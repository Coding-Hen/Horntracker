function formatTimeStamp(unix_timestamp)
{
	var date_now = new Date();
	var n = date_now.getTime() - (unix_timestamp * 1000);
	
	var date = new Date(n);
	
	return timeSince(date);
}

function timeSince(date) 
{
	var toReturn = '';
	
	var seconds = Math.floor((new Date() - date) / 1000);
	var interval = Math.floor(seconds / 86400);
	
	//Days
	if (interval >= 1) 
	{
		if (interval == 1)
			toReturn = interval + " day ago.";
		else
			toReturn = interval + " days ago.";
			
		return toReturn;
		
		//seconds = seconds - (interval * 86400);
	}
	
	//Hours
	interval = Math.floor(seconds / 3600);
	if (interval < 10)
		interval = '0' + interval;
		
	toReturn += interval + ":"
	seconds = seconds - (interval * 3600);
	
	//Minutes
	interval = Math.floor(seconds / 60);
	if (interval < 10)
		interval = '0' + interval;
		
	toReturn += interval + ":"
		
	seconds = seconds - (interval * 60);
	
	//Seconds
	if (seconds < 10)
		seconds = '0' + seconds;
		
	toReturn += seconds + " ago.";
	
	return toReturn;
}

function is_set(variable)
{
	if (typeof variable === "undefined")
		return false;
	else
		return true;
}

function is_JSON_string(str) 
{
	try 
	{
		JSON.parse(str);
	} 
	catch (e) 
	{
		return false;
	}

	return true;
}

function isNumber(n) 
{
	return !isNaN(parseFloat(n)) && isFinite(n);
}