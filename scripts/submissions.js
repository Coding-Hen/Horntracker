var uObj;
var _cur_user_options;
var _request_type = 'postrequest_hg';
var _cur_tab_inc = 1;

document.addEventListener('DOMContentLoaded', function () 
{
	document.addEventListener("ht_fb_user", ht_get_fb_user, false);

	//Display a retrieving options spinner unless it already exists.
	if ($('#spinner').length == 0)
	{
		var div_loading = document.createElement('div');
		div_loading.id = 'spinner';
		$('#ht_message').append(div_loading);

		var div_loading_text = document.createElement('div');
		div_loading_text.id = 'spinner_text';
		div_loading_text.appendChild(document.createTextNode('Retrieving options...'));
		$('#ht_message').append(div_loading_text);
	}

	//Grab the user object.
	getUserObject(initWithUser);

	//Remove all localStorage mostmice entries.
	remove_local_mostmice();
});

function ht_get_fb_user(aEvent)
{
	uObj = JSON.parse(aEvent.newValue);

	initWithUser(uObj);
}

function getUserObject(callback)
{
	var POST_params = { };

	var user_port = chrome.runtime.connect({name: "get_user_obj"});
	var ht_request = 'https://www.mousehuntgame.com/managers/ajax/users/data.php';

	user_port.postMessage({request: _request_type, url: ht_request, params: POST_params});
	user_port.onMessage.addListener(function(msg) {
		if (is_JSON_string(msg.html))
		{
			var retval = JSON.parse(msg.html);
			if (is_set(retval.user))
				callback(retval.user);
			else
			{
				_request_type = 'postrequest_fb';

				var evt = document.createEvent("MutationEvents");
				evt.initMutationEvent("xhri_get_fb_user", true, true, this, '', '', 'ht_stuff', 1);
				document.dispatchEvent(evt);
			}
		}
	});
}

function initWithUser(resp)
{
	uObj = resp;

	optionsRequest(uObj.sn_user_id, true);
}

function optionsRequest(fbid)
{
	var options_port = chrome.runtime.connect({name: "options_request"});
	options_port.postMessage({request: "options", user: fbid});
	options_port.onMessage.addListener(function(msg) {
		//Update _cur_user_options.
		if (is_set(msg.options))
			_cur_user_options = msg.options;

		//Remove spinners
		$('#spinner').remove();
		$('#spinner_text').remove();

		//Init tabs based on options.
		//TODO: Utilize option variables to display these tabs.
		if (true)
			display_convertibles();

		if (true)
			display_points_over_time();

		//Click tab1 to init the table after the tabs are formed.
		$('.tab1').click();
	});
}

function display_convertibles()
{
	var li_tab = document.createElement('li');
	li_tab.id = "li_tab_convertible";
	li_tab.classList.add('tab' + _cur_tab_inc);
	_cur_tab_inc++;

	var li_tab_a = document.createElement('a');
	li_tab_a.href = '#';
	li_tab_a.appendChild(document.createTextNode('Convertibles'));

	li_tab.appendChild(li_tab_a);
	li_tab.addEventListener('click', handle_tab_convertibles);

	$('#tabnav').append(li_tab);
	$('#tabnav').append(document.createTextNode(' '));
}

function display_points_over_time()
{
	var li_tab = document.createElement('li');
	li_tab.id = "li_tab_points_over_time";
	li_tab.classList.add('tab' + _cur_tab_inc);
	_cur_tab_inc++;

	var li_tab_a = document.createElement('a');
	li_tab_a.href = '#';
	li_tab_a.appendChild(document.createTextNode('Points Timeline'));

	li_tab.appendChild(li_tab_a);
	//li_tab.addEventListener('click', handle_tab_points_over_time_storage);
	li_tab.addEventListener('click', handle_tab_points_over_time);

	$('#tabnav').append(li_tab);
	$('#tabnav').append(document.createTextNode(' '));
}

function remove_local_mostmice()
{
	var i = 0;
    var oJson = [];
    var sKey;
	
	while ((sKey = localStorage.key(i)))
	{
		var key = sKey.match(/^mostmice_(\d+)$/);
		if (key !== null)
	    	oJson.push(key[0]);
	    i++;
	}

	//Prowl through oJson to remove from.
	for (var rec in oJson)
		localStorage.removeItem(oJson[rec]);
}

function handle_tab_hunts(e)
{
	document.body.id = e.currentTarget.classList[0];
	
	clean_ui();

	//TODO: Implement.
	var div_unimp = document.createElement('div');
	div_unimp.id = 'unimp';
	div_unimp.appendChild(document.createTextNode('Coming soon...'));

	document.body.appendChild(div_unimp);
}

function handle_tab_convertibles(e)
{
	document.body.id = e.currentTarget.classList[0];

	clean_ui();

	$.ajax({
		url: 'http://horntracker.com/backend/userdata.php?functionCall=userconvert&fbid=' + uObj.sn_user_id,
		success: function(data) {
			if (!is_JSON_string(data))
			{
				//TODO: Some error?
				return;
			}

			resetConvertTable(JSON.parse(data));
		},
		error: function(data) {
			//TODO: Some error?
		}
	});
}

function handle_tab_points_over_time(e)
{
	document.body.id = e.currentTarget.classList[0];

	clean_ui();

	$.ajax({
		url: 'http://horntracker.com/backend/userdata.php?functionCall=userpointstime&fbid=' + uObj.sn_user_id,
		success: function(data) {
			if (!is_JSON_string(data))
			{
				//TODO: Some error?
				return;
			}

			createPointsChartWithData(JSON.parse(data));
		},
		error: function(data) {
			//TODO: Some error?
		}
	});
}

function handle_tab_points_over_time_storage(e)
{
	document.body.id = e.currentTarget.classList[0];

	clean_ui();

	//TODO: Get latest datetime from IndexedDB?
	var request = window.indexedDB.open("HT_DB", 3);
	request.onupgradeneeded = function(event) {
		upgradeHTDB();
	};

	request.onsuccess = function(event){
		db = request.result;
		
		//Load data into storedPoints
		var objectStore = db.transaction('storedPoints', 'readwrite').objectStore('storedPoints');

		//TODO: Can we in the transaction look at the last record?
		//Get latest record possible?
		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;

			if (cursor)
			{
				this.latest = cursor.value.postMessageValue.st;

				//Move to next record.
				cursor.continue();
			}
			else
			{
				if (this.latest)
					updatePointsData(this.latest);
				else
					updatePointsData('2014-02-13 15:39:18');
			}
		};
	};
}

function updatePointsData(timestart)
{
	$.ajax({
		url: 'http://horntracker.com/backend/userdata.php?functionCall=userpointstime&fbid=' + uObj.sn_user_id + '&mintime=' + timestart,
		success: function(data) {
			if (!is_JSON_string(data))
			{
				//TODO: Some error?
				return;
			}

			//Update IndexedDB with values that came back.
			var request = window.indexedDB.open("HT_DB", 3);
			request.ht_data = JSON.parse(data);
			request.onupgradeneeded = function(event) {
				upgradeHTDB();
			};

			request.onsuccess = function(event){
				db = request.result;
				
				//Load data into storedPoints
				var transaction = db.transaction("storedPoints", "readwrite");

				transaction.oncomplete = function(event) {
					//TODO: Necessary?
				};

				transaction.onerror = function(event) {
					//TODO: Some error notification?
				};

				var objectStore = transaction.objectStore("storedPoints");

				for (var i in this.ht_data)
				{
					var newItem = [{ postMessageValue: this.ht_data[i] }, this.ht_data[i].et];
					var requestAdd = objectStore.add(newItem[0]);

					requestAdd.onsuccess = function(event) {
						//TODO: Some notification for success
					};

					requestAdd.onerror = function(event) {
						//TODO: Some error notification?
					};
				}

				//TODO: Draw graph based on data from in the DB.
				if (this.ht_data.length > 0)
					createPointsGraph();
			};

			//TODO: Draw graph based on data from in the DB.
			if (request.ht_data.length == 0)
			{
				createPointsGraph();
			}
		},
		error: function(data) {
			//TODO: Some error?
		}
	});
}

function createPointsGraph()
{
	var request = window.indexedDB.open("HT_DB", 3);
	request.onupgradeneeded = function(event) {
		upgradeHTDB();
	};

	request.onsuccess = function(event){
		db = request.result;
		
		//Load data into storedPoints
		var objectStore = db.transaction('storedPoints', 'readwrite').objectStore('storedPoints');

		objectStore.openCursor().onsuccess = function(event) {
			var cursor = event.target.result;

			if (!this.source.records)
				this.source.records = [];

			if (cursor)
			{
				//Push hunt with this msg value to be submitted later.
				this.source.records.push(cursor.value.postMessageValue);

				//Move to next record.
				cursor.continue();
			}
			else
			{
				//TODO: End of the line, use this.source.records to populate the graph.
				//console.log(this.source.records.length);

				createPointsChartWithData(this.source.records);
			}
		};
	};
}

function createPointsChartWithData(data)
{
	var options = {
        legend: { show: false },
        series: {
            lines: { show: true },
            points: { show: true }
        },
        grid: { hoverable: true },
        xaxis: { mode: "time" },
        yaxis: { ticks: 10 },
        selection: { mode: "xy" }
    };

    //<div id="usersOnline" style="width:1200px;height:400px"></div>
	var div_pts = document.createElement('div');
	div_pts.id = 'pointsData';
	$(div_pts).attr('style', 'width:800px;height:400px');

	document.body.appendChild(div_pts);

	$(div_pts).ready(function(){
		var startData = [];
		var tp = 0;
		for (var i in data)
		{
			tp = tp + Number(data[i].tp);
			var date = new Date(data[i].et.split(' ').join('T'));
			startData.push([date.getTime(), tp, data[i]]);
		}

	    var plot = $.plot($("#pointsData"), [{ label: "Points Per Hunt By Location", data: startData }], options);

	    $("#pointsData").bind("plotselected", function (event, ranges) {
	        // clamp the zooming to prevent eternal zoom
	        if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
	            ranges.xaxis.to = ranges.xaxis.from + 0.00001;
	        if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
	            ranges.yaxis.to = ranges.yaxis.from + 0.00001;

	        // do the zooming
	        plot = $.plot($("#pointsData"), [{ label: "Points Per Hunt By Location", data: startData }],
	                      $.extend(true, {}, options, {
	                          xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to, mode: "time" },
	                          yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
	                      }));
	    });

	    var previousPoint = null;
	    $("#pointsData").bind("plothover", function (event, pos, item) {
			if (item) 
			{
				if (previousPoint != item.dataIndex) 
				{
					previousPoint = item.dataIndex;

					$("#g_tooltip").remove();
					var x = item.datapoint[0].toFixed(),
						y = item.datapoint[1].toFixed();

					var trueIndex = item.dataIndex + 1;
					if (trueIndex >= item.series.data.length)
						trueIndex = 0;

					var toDisplay = item.series.data[trueIndex][2].loc + ' - ' + 
									item.series.data[trueIndex][2].pph + ' points per hunt for ' + 
									item.series.data[trueIndex][2].h + ' hunts.';

					showGraphTooltip(0, 0, toDisplay);
				}
			}
			else 
			{
				$("#g_tooltip").remove();
				previousPoint = null;
			}
	    });
	});
}

function showGraphTooltip(x, y, contents) 
{
	$('<div id="g_tooltip">' + contents + '</div>').css( {
		position: 'absolute',
		display: 'none',
		top: y + 5,
		left: x + 5,
		border: '1px solid #fdd',
		padding: '2px',
		'background-color': '#fee',
		opacity: 0.80
	}).appendTo("body").fadeIn(200);
}

function upgradeHTDB()
{
	var db = event.target.result;

	db.onerror = function(event) {
		//TODO: Some error notification?
	};

	db.deleteObjectStore('storedHunts');
	db.deleteObjectStore('storedPoints');
	db.deleteObjectStore('storedGold');
	db.deleteObjectStore('storedRank');

	var objectStore = db.createObjectStore('storedHunts',  {keyPath: 'sHunt',  autoIncrement: true});
	objectStore.createIndex('postMessageValue', 'postMessageValue', {unique: true});

	var objectStore = db.createObjectStore('storedPoints', {keyPath: 'sPoint', autoIncrement: true});
	objectStore.createIndex('postMessageValue', 'postMessageValue', {unique: true});

	var objectStore = db.createObjectStore('storedGold',   {keyPath: 'sGold',  autoIncrement: true});
	objectStore.createIndex('postMessageValue', 'postMessageValue', {unique: true});

	var objectStore = db.createObjectStore('storedRank',   {keyPath: 'sRank',  autoIncrement: true});
	objectStore.createIndex('postMessageValue', 'postMessageValue', {unique: true});
}

function resetConvertTable(data)
{
	var h = [["Rank","sortable-numeric"], ["Name","sortable-text"], ["Submission Count","sortable-numeric"], ["Last Updated (hh:mm:ss)","sortable-sortTimeMixedFormat"]];
	var table = document.getElementById("board");

	if(table)
	{
		table.parentNode.removeChild(table);
		$('.fdtablePaginaterWrap').remove();
	}

	table     = document.createElement("table");
	var tr    = document.createElement("tr");
	var thead = document.createElement("thead");
	var tbody = document.createElement('tbody');
	var th    = document.createElement("th");
	var trc   = tr.cloneNode(true);
	var thc;

	table.id = "board";
	table.className = "no-arrow rowstyle-alt paginate-10 max-pages-10 sortable-onload-0";
	table.cellspacing = table.cellpadding = 0

	document.getElementsByTagName('body')[0].appendChild(table);
	thead.appendChild(trc);
	table.appendChild(thead);
	table.appendChild(tbody);
	table = null;
	
	for(var j = 0; j < h.length; j++)
	{
		thc = th.cloneNode(false);
		trc.appendChild(thc);
		thc.className = h[j][1];
		thc.appendChild(document.createTextNode(h[j][0]));
		thc = null;
	};

	trc = thead = null;

	//Roll through data
	var count = 1;
	for (var i in data)
	{
		//Use addConvertCells
		trc = tr.cloneNode(false);
		tbody.appendChild(trc);
		if (data[i].last_seen == 0)
			addConvertCells(trc, count, data[i].name, data[i].id, data[i].count, 'Never');
		else
			addConvertCells(trc, count, data[i].name, data[i].id, data[i].count, formatTimeStamp(data[i].last_seen));
		count++;
	}

	tbody = null;

	fdTableSort.init();

	tablePaginater.init(table);
	
	return false;
}

function addConvertCells(tr, rank, loot_name, loot_id, total, last_seen)
{
	if(rank % 2 == 0) tr.className = "alt";

	var td1 = document.createElement("td");
	tr.appendChild(td1);
	td1.appendChild(document.createTextNode(rank));

	var td2 = document.createElement("td");
	tr.appendChild(td2);
	var td2A = document.createElement("a");
	td2A.setAttribute('href', '#');
	td2A.setAttribute('lootid', loot_id);
	td2.appendChild(td2A);
	td2A.appendChild(document.createTextNode(loot_name));

	$(td2A).click(function(e){
		call_specific(loot_id);
	});

    var td3 = document.createElement("td");
    tr.appendChild(td3);
    td3.appendChild(document.createTextNode(total));

	var td4 = document.createElement("td");
	tr.appendChild(td4);
	td4.appendChild(document.createTextNode(last_seen));

	td1 = td2 = td3 = td4 = null;
}

function do_pie_hover(event, pos, obj) 
{
	if (!obj)
		return;

	$("#tooltip").remove();
	showTooltip(pos.pageX, pos.pageY, obj);
}

function showTooltip(x, y, obj) 
{
	$('<div id="tooltip">' + parseFloat(obj.series.percent).toFixed(2) + '%</div>').css( 
	{
		position: 'absolute',
		display: 'none',
		top: y - 5,
		left: x + 5,
		border: '1px solid ' + obj.series.color,
		padding: '2px',
		'background-color': '#000',
		opacity: 0.50
	}).appendTo("body").fadeIn(100);
}

function call_specific(lootid)
{
	console.log(lootid);
	
	//Start with a fresh UI, and then rebuild.
	clean_ui();

	//Create a variation of the loot.php page.
	$.ajax({
		url: 'http://horntracker.com/backend/userdata.php?functionCall=userconvertspec' + '&fbid=' + uObj.sn_user_id + '&lootid=' + lootid,
		success: function(data) {
			if (!is_JSON_string(data))
			{
				return;
			}

			resetLoot(JSON.parse(data));
		},
		error: function(data) {
			
		}
	});
}

function resetLoot(data)
{
	var loot_array = data.singles.loot_array;
	var totals_loot_array = data.totals.loot_array;

	var h = [["Name","sortable-text"], 
			 ["Probability","sortable-numeric"], 
			 ["Amount Per Open","sortable-numeric"], 
			 ["Range","sortable-text"]];

	var table = document.getElementById("board");

	if(table)
	{
		table.parentNode.removeChild(table);
		$('.fdtablePaginaterWrap').remove();
	}

	table     = document.createElement("table");
	var tr    = document.createElement("tr");
	var thead = document.createElement("thead");
	var tbody = document.createElement('tbody');
	var th    = document.createElement("th");
	var trc   = tr.cloneNode(true);
	var thc;

	table.id = "board";
	table.className = "no-arrow rowstyle-alt paginate-3 max-pages-10 sortable-onload-0";
	table.cellspacing = table.cellpadding = 0

	document.getElementsByTagName('body')[0].appendChild(table);
	thead.appendChild(trc);
	table.appendChild(thead);
	table.appendChild(tbody);
	table = null;
	
	for(var j = 0; j < h.length; j++)
	{
		thc = th.cloneNode(false);
		trc.appendChild(thc);
		thc.className = h[j][1];
		thc.appendChild(document.createTextNode(h[j][0]));
		thc = null;
	};

	trc = thead = null;

	var count = 1;
	//Roll through totals_loot_array. If it doesn't exist in loot_array, we have mysteries.
	for (var i in totals_loot_array)
	{
		//Use addCells
		trc = tr.cloneNode(false);
		tbody.appendChild(trc);
		
		if (is_set(loot_array[i]))
			addCellsLoot(trc, count, i, loot_array[i].percentage + '%', '+/- ' + loot_array[i].error + '%', totals_loot_array[i].quantity_rate, loot_array[i].quantity_array);
		else
			addCellsLoot(trc, count, i, 'Unknown', '?', totals_loot_array[i].quantity_rate, {Unknown:1});

		count++;
	}

	tbody = null;

	fdTableSort.init();

	tablePaginater.init(table);
	
	return false;
}

function addCellsLoot(tr, rank, name, percentage, error, amt_per_open, range_pie)
{
	if(rank % 2 == 0) tr.className = "alt";

	var td1 = document.createElement("td");
	tr.appendChild(td1);
	td1.appendChild(document.createTextNode(name));

	var td2 = document.createElement("td");
	tr.appendChild(td2);
	td2.appendChild(document.createTextNode(percentage + ' ' + error));

	var td3 = document.createElement("td");
	tr.appendChild(td3);
	td3.appendChild(document.createTextNode(amt_per_open));

	//TODO: Do graph better.
	var td4 = document.createElement("td");
	tr.appendChild(td4);
	var td4_div = document.createElement("div");
	td4_div.setAttribute('id', 'pie_' + rank);
	td4_div.setAttribute('class', 'graph');
	td4.appendChild(td4_div);

	$(td4_div).on('mouseout', function(event){
		$("#tooltip").remove();
	});

	var range_data = [];
	var counter = 0;
	for (var i in range_pie)
	{
		range_data[counter] = { label: i.replace('Q',''), data: range_pie[i] }
		counter++;
	}

	$.plot(td4_div, range_data,
	{
		series: 
		{
			pie:
			{
				show: true,
				radius: 3/4,
				label:
				{
					show: true,
					radius: 3/4,
					formatter: function(label, series)
					{
						return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '</div>';
					},
					background: 
					{
						opacity: 0.5,
						color: '#000'
					}
				}
			}
		},
		legend: 
		{
			show: false
		},
		grid:
		{
			hoverable: true
		}
	});
	$(td4_div).bind("plothover", do_pie_hover);
	//td4_div.bind("plotclick", do_pie_click);

	td1 = td2 = td3 = td4 = null;
}

function clean_ui()
{
	//Any table.
	$('#board').remove();

	//Any paginater wrap.
	$('.fdtablePaginaterWrap').remove();

	//Unimplemented div
	$('#unimp').remove();

	//Any graph
	$("#pointsData").remove();
}


//TODO: PUT THE BELOW FUNCTIONS IN A UTIL.JS FILE
function formatTimeStamp(unix_timestamp)
{
	var date_now = new Date();
	var n = date_now.getTime() - (unix_timestamp * 1000);
	
	var date = new Date(n);
	
	return timeSince(date);
}

function timeSince(date) 
{
	var toReturn = '';
	
	var seconds = Math.floor((new Date() - date) / 1000);
	var interval = Math.floor(seconds / 86400);
	
	//Days
	if (interval >= 1) 
	{
		if (interval == 1)
			toReturn = interval + " day ago.";
		else
			toReturn = interval + " days ago.";
			
		return toReturn;
		
		//seconds = seconds - (interval * 86400);
	}
	
	//Hours
	interval = Math.floor(seconds / 3600);
	if (interval < 10)
		interval = '0' + interval;
		
	toReturn += interval + ":"
	seconds = seconds - (interval * 3600);
	
	//Minutes
	interval = Math.floor(seconds / 60);
	if (interval < 10)
		interval = '0' + interval;
		
	toReturn += interval + ":"
		
	seconds = seconds - (interval * 60);
	
	//Seconds
	if (seconds < 10)
		seconds = '0' + seconds;
		
	toReturn += seconds + " ago.";
	
	return toReturn;
}

function is_set(variable)
{
	if (typeof variable === "undefined")
		return false;
	else
		return true;
}

function is_JSON_string(str) 
{
	try 
	{
		JSON.parse(str);
	} 
	catch (e) 
	{
		return false;
	}

	return true;
}

function isNumber(n) 
{
	return !isNaN(parseFloat(n)) && isFinite(n);
}