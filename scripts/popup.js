document.addEventListener('DOMContentLoaded', function () { 
	var report_records = report();
	fdTableSort.addEvent(window, "load", resetTable(report_records));
});

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function is_set(variable)
{
	if (typeof variable === "undefined")
		return false;
	else
		return true;
}

function report()
{
	var i = 0;
    var oJson = {};
    var sKey;
	
	while ((sKey = localStorage.key(i)))
	{
		var key = sKey.match(/^mostmice_(\d+)$/);
		if (key !== null)
	    	oJson[key[1]] = localStorage.getItem(sKey);
	    i++;
	}

	//Prowl through oJson to update most mice based on these numbers.
	var records = Array();
    var oCount = 0;
	for (var rec in oJson)
	{
        oCount++;
		var person = JSON.parse(oJson[rec]);

		for (var p in person.mice)
		{
			if (is_set(records[p]))
			{
				if (records[p].score < person.mice[p])
					records[p] = {user:person.un, score: person.mice[p], date: person.ts, pid: rec};
			}
			else
				records[p] = {user: person.un, score: person.mice[p], date: person.ts, pid: rec};
		}
	}

	//"oCount" will be a link that brings up all known profiles.
	if (oCount > 1)
		$('h2').html('Most mice caught from <a href="#">' + oCount + '</a> visited profiles!');
	else if (oCount == 1)
		$('h2').html('Most mice caught from <a href="#">' + oCount + '</a> visited profile!');
	else
		$('h2').html('No profiles visited yet!');
	
	$('h2 a').bind('click', {users: oJson, report: records}, recordCountClicked );

	return records;
}

function recordCountClicked(event)
{
	var users = event.data.users;
	var report = event.data.report;

	var records = Array();
	var count = 0;
	for (var rec in users)
	{
		var person = JSON.parse(users[rec]);

		if (Object.size(person.mice) > count)
			count = Object.size(person.mice);

		records[person.un] = {pid: rec, date: person.ts};
	}

	//Users from visited profiles containing [count] mice.
	if (count > 1)
		$('h2').html('Users from visited profiles containing <a href="#">' + count + '</a> mice!');
	else if (count == 1)
		$('h2').html('Users from visited profiles containing <a href="#">' + count + '</a> mouse!');
	else
		$('h2').html('No profiles visited yet!');
	
	$('h2 a').bind('click', mouseCountClicked);

	resetUserTable(records);
}

function mouseCountClicked(event) 
{
	resetTable(report());
}

function mouseClicked(event)
{
	var i = 0;
    var oJson = {};
    var sKey;
	
	while ((sKey = localStorage.key(i)))
	{
		var key = sKey.match(/^mostmice_(\d+)$/);
		if (key !== null)
	    	oJson[key[1]] = localStorage.getItem(sKey);
	    i++;
	}

	//Prowl through oJson to update most mice based on these numbers.
	var records = Array();
    var oCount = 0;
	for (var rec in oJson)
	{
        oCount++;
		var person = JSON.parse(oJson[rec]);

		if (is_set(person.mice[event.data.mname]))
			records[rec] = {user: person.un, score: person.mice[event.data.mname], date: person.ts};
		else
			records[rec] = {user: person.un, score: 0, date: person.ts};
	}

	//"oCount" will be a link that brings up all known profiles? Maybe changes the table to a list of users instead?
	if (oCount > 1)
		$('h2').html('Most ' + event.data.mname.replace("Mouse", "Mice") + ' caught from <a href="#">' + oCount + '</a> visited profiles!');
	else if (oCount == 1)
		$('h2').html('Most ' + event.data.mname.replace("Mouse", "Mice") + ' caught from <a href="#">' + oCount + '</a> visited profile!');
	else
		$('h2').html('No profiles visited yet!');
	
	$('h2 a').bind('click', mouseCountClicked);

	resetMouseTable(records);
}

function timeSince(date) 
{
	var toReturn = '';
	
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 86400);
    
    //Days
    if (interval >= 1) 
    {
    	if (interval == 1)
    		toReturn = interval + " day ago.";
    	else
    		toReturn = interval + " days ago.";
    		
    	return toReturn;
    	
        //seconds = seconds - (interval * 86400);
    }
    
    //Hours
    interval = Math.floor(seconds / 3600);
	if (interval < 10)
		interval = '0' + interval;
		
	toReturn += interval + ":"
    seconds = seconds - (interval * 3600);
    
    //Minutes
    interval = Math.floor(seconds / 60);
	if (interval < 10)
		interval = '0' + interval;
		
	toReturn += interval + ":"
		
    seconds = seconds - (interval * 60);
    
    //Seconds
	if (seconds < 10)
		seconds = '0' + seconds;
		
	toReturn += seconds + " ago.";
	
	return toReturn;
}

function createResetDataButton()
{
	//Remove reset data button.
	var clear_reset = document.getElementById("reset_data");
	if (clear_reset)
		clear_reset.parentNode.removeChild(clear_reset);

	var d = document.createElement("div");
	d.id = 'cancel_div';
	d.className = 'centered';

	var reset_data = document.createElement("button");
	reset_data.id = 'reset_data';
	reset_data.style = 'width: 100px';

	reset_data.addEventListener('click', function(){ 
		var i = 0;
        var sKey;
        var oJson = Array();
        
        while ((sKey = localStorage.key(i)))
        {
            var key = sKey.match(/^mostmice_(\d+)$/);
            if (key !== null)
                oJson.push(key[0]);
            i++;
        }

        for (var rec in oJson)
            localStorage.removeItem(oJson[rec]);

		resetTable({});
        
        $('h2').html('No profiles visited yet!');
	});

	reset_data.appendChild(document.createTextNode('Reset Data'));

	d.appendChild(reset_data);

	document.body.appendChild(d);
	document.body.appendChild(document.createElement('p'));
}

function resetTable(data)
{
	var h = [["Mouse","sortable-text"], ["Hunter","sortable-text"], ["Amount Caught","sortable-numeric"], ["Last Checked","sortable-text"]];
	var table = document.getElementById("board");

    if(table)
    {
    	table.parentNode.removeChild(table);
    	$('.fdtablePaginaterWrap').remove();
    }

    table     = document.createElement("table");
    var tr    = document.createElement("tr");
    var thead = document.createElement("thead");
    var tbody = document.createElement('tbody');
    var th    = document.createElement("th");
    var trc   = tr.cloneNode(true);
    var thc;

    table.id = "board";
    table.className = "no-arrow rowstyle-alt paginate-12 max-pages-10 sortable-onload-0";
    table.cellspacing = table.cellpadding = 0

    document.getElementsByTagName('body')[0].appendChild(table);
    thead.appendChild(trc);
    table.appendChild(thead);
    table.appendChild(tbody);
    table = null;
    
    for(var j = 0; j < h.length; j++)
    {
        thc = th.cloneNode(false);
        trc.appendChild(thc);
        thc.className = h[j][1];
        thc.appendChild(document.createTextNode(h[j][0]));
        thc = null;
    };

    trc = thead = null;

    var ht_user_id = localStorage.getItem('ht_mostmiceme');

    //Roll through data
    var count = 1;
    for (var i in data)
    {
		//Use addCells
		trc = tr.cloneNode(false);
        if (ht_user_id == data[i].pid)
            trc.classList.add('highlighted');
        tbody.appendChild(trc);

        addCells(trc, count, i, data[i].user, data[i].pid, data[i].score, data[i].date);
        count++;
	}

    tbody = null;

    //createSearchBar();

	createResetDataButton();

    fdTableSort.init();

    tablePaginater.init(table);
	
    return false;
}

function addCells(tr, rank, mouse_name, hunter_name, profile_link, score, last_checked)
{
    if(rank % 2 == 0) tr.classList.add('alt');

    var td1 = document.createElement("td");
    tr.appendChild(td1);
    var td1A = document.createElement("a");
    td1A.setAttribute('href', '#');
    $(td1A).bind('click', {mname: mouse_name}, mouseClicked );
    td1.appendChild(td1A);
   	td1A.appendChild(document.createTextNode(mouse_name));

    var td2 = document.createElement("td");
    tr.appendChild(td2);
    var td2A = document.createElement("a");
    td2A.setAttribute('href', 'http://www.mousehuntgame.com/profile.php?snuid=' + profile_link);
    td2.appendChild(td2A);
   	td2A.appendChild(document.createTextNode(hunter_name));

    var td3 = document.createElement("td");
    tr.appendChild(td3);
    td3.appendChild(document.createTextNode(score));

    var td4 = document.createElement("td");
    tr.appendChild(td4);
    td4.className = "lft";
    var lc = new Date(last_checked);

    td4.appendChild(document.createTextNode(timeSince(lc)));

    td1 = td2 = td3 = td4 = null;
}

function resetUserTable(data)
{
	//TODO: First name, last name?
	var h = [["Hunter","sortable-text"], ["Last Checked","sortable-text"]];
	var table = document.getElementById("board");

    if(table)
    {
    	table.parentNode.removeChild(table);
    	$('.fdtablePaginaterWrap').remove();
    }

    table     = document.createElement("table");
    var tr    = document.createElement("tr");
    var thead = document.createElement("thead");
    var tbody = document.createElement('tbody');
    var th    = document.createElement("th");
    var trc   = tr.cloneNode(true);
    var thc;

    table.id = "board";
    table.className = "no-arrow rowstyle-alt paginate-12 max-pages-10 sortable-onload-0";
    table.cellspacing = table.cellpadding = 0

    document.getElementsByTagName('body')[0].appendChild(table);
    thead.appendChild(trc);
    table.appendChild(thead);
    table.appendChild(tbody);
    table = null;
    
    for(var j = 0; j < h.length; j++)
    {
        thc = th.cloneNode(false);
        trc.appendChild(thc);
        thc.className = h[j][1];
        thc.appendChild(document.createTextNode(h[j][0]));
        thc = null;
    };

    trc = thead = null;

    var ht_user_id = localStorage.getItem('ht_mostmiceme');

    //Roll through data
    var count = 1;
    for (var i in data)
    {
		//Use addCells
		trc = tr.cloneNode(false);
        if (ht_user_id == data[i].pid)
            trc.classList.add('highlighted');
        tbody.appendChild(trc);

        addUserCells(trc, count, i, data[i].pid, data[i].date);
        count++;
	}

    tbody = null;

    //createSearchBar();

	createResetDataButton();

    fdTableSort.init();

    tablePaginater.init(table);
	
    return false;
}

function addUserCells(tr, rank, hunter_name, profile_link, last_checked)
{
    if(rank % 2 == 0) tr.classList.add('alt');

    var td2 = document.createElement("td");
    tr.appendChild(td2);
    var td2A = document.createElement("a");
    td2A.setAttribute('href', 'http://www.mousehuntgame.com/profile.php?snuid=' + profile_link);
    td2.appendChild(td2A);
   	td2A.appendChild(document.createTextNode(hunter_name));

    var td4 = document.createElement("td");
    tr.appendChild(td4);
    td4.className = "lft";
    var lc = new Date(last_checked);

    td4.appendChild(document.createTextNode(timeSince(lc)));

    td2 = td4 = null;
}

function resetMouseTable(data)
{
	//TODO: First name, last name?
	var h = [["Hunter","sortable-text"], ["Amount Caught","sortable-numeric"], ["Last Checked","sortable-text"]];
	var table = document.getElementById("board");

    if(table)
    {
    	table.parentNode.removeChild(table);
    	$('.fdtablePaginaterWrap').remove();
    }

    table     = document.createElement("table");
    var tr    = document.createElement("tr");
    var thead = document.createElement("thead");
    var tbody = document.createElement('tbody');
    var th    = document.createElement("th");
    var trc   = tr.cloneNode(true);
    var thc;

    table.id = "board";
    table.className = "no-arrow rowstyle-alt paginate-12 max-pages-10 sortable-onload-1r";
    table.cellspacing = table.cellpadding = 0

    document.getElementsByTagName('body')[0].appendChild(table);
    thead.appendChild(trc);
    table.appendChild(thead);
    table.appendChild(tbody);
    table = null;
    
    for(var j = 0; j < h.length; j++)
    {
        thc = th.cloneNode(false);
        trc.appendChild(thc);
        thc.className = h[j][1];
        thc.appendChild(document.createTextNode(h[j][0]));
        thc = null;
    };

    trc = thead = null;

    var ht_user_id = localStorage.getItem('ht_mostmiceme');

    //Roll through data
    //records[rec] = {user: person.un, score: person.mice[p], date: person.ts};
    var count = 1;
    for (var i in data)
    {
		//Use addCells
		trc = tr.cloneNode(false);
        if (ht_user_id == i)
            trc.classList.add('highlighted');
        tbody.appendChild(trc);

        addMouseCells(trc, count, i, data[i].user, data[i].score, data[i].date);
        count++;
	}

    tbody = null;

    //createSearchBar();

	createResetDataButton();

    fdTableSort.init();

    tablePaginater.init(table);
	
    return false;
}

function addMouseCells(tr, rank, profile_link, hunter_name, score, last_checked)
{
    if(rank % 2 == 0) tr.classList.add('alt');

    var td2 = document.createElement("td");
    tr.appendChild(td2);
    var td2A = document.createElement("a");
    td2A.setAttribute('href', 'http://www.mousehuntgame.com/profile.php?snuid=' + profile_link);
    td2.appendChild(td2A);
   	td2A.appendChild(document.createTextNode(hunter_name));

   	var td3 = document.createElement("td");
    tr.appendChild(td3);
    td3.appendChild(document.createTextNode(score));

    var td4 = document.createElement("td");
    tr.appendChild(td4);
    td4.className = "lft";
    var lc = new Date(last_checked);

    td4.appendChild(document.createTextNode(timeSince(lc)));

    td2 = td3 = td4 = null;
}