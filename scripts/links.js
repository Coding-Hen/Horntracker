document.addEventListener('DOMContentLoaded', function () 
{
	var links = chrome.extension.getBackgroundPage()._link_JSON.data;
	for (var key in links)
	{
		var link = links[key];
		
		$('span#ht_links').append($('<br>'));
		var a_link = $('<a>')
						.attr('title', link.title_text)
						.attr('target', '_blank')
						.attr('href', link.URL);
		a_link.append(link.link_text);
		//If is_new display the isnew div.
		if (link.is_new == '1')
		{
			a_link.append($('<div>')
							.addClass('isnew')
							.append('NEW!'));
		}
		
		$('span#ht_links').append(a_link).append(' - ' + link.title_text);
	}
});