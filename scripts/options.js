if (typeof KeyEvent == "undefined") {
    var KeyEvent = {
        DOM_VK_CANCEL: 3,
        DOM_VK_HELP: 6,
        DOM_VK_BACK_SPACE: 8,
        DOM_VK_TAB: 9,
        DOM_VK_CLEAR: 12,
        DOM_VK_RETURN: 13,
        DOM_VK_ENTER: 14,
        DOM_VK_SHIFT: 16,
        DOM_VK_CONTROL: 17,
        DOM_VK_ALT: 18,
        DOM_VK_PAUSE: 19,
        DOM_VK_CAPS_LOCK: 20,
        DOM_VK_ESCAPE: 27,
        DOM_VK_SPACE: 32,
        DOM_VK_PAGE_UP: 33,
        DOM_VK_PAGE_DOWN: 34,
        DOM_VK_END: 35,
        DOM_VK_HOME: 36,
        DOM_VK_LEFT: 37,
        DOM_VK_UP: 38,
        DOM_VK_RIGHT: 39,
        DOM_VK_DOWN: 40,
        DOM_VK_PRINTSCREEN: 44,
        DOM_VK_INSERT: 45,
        DOM_VK_DELETE: 46,
        DOM_VK_0: 48,
        DOM_VK_1: 49,
        DOM_VK_2: 50,
        DOM_VK_3: 51,
        DOM_VK_4: 52,
        DOM_VK_5: 53,
        DOM_VK_6: 54,
        DOM_VK_7: 55,
        DOM_VK_8: 56,
        DOM_VK_9: 57,
        DOM_VK_SEMICOLON: 59,
        DOM_VK_EQUALS: 61,
        DOM_VK_A: 65,
        DOM_VK_B: 66,
        DOM_VK_C: 67,
        DOM_VK_D: 68,
        DOM_VK_E: 69,
        DOM_VK_F: 70,
        DOM_VK_G: 71,
        DOM_VK_H: 72,
        DOM_VK_I: 73,
        DOM_VK_J: 74,
        DOM_VK_K: 75,
        DOM_VK_L: 76,
        DOM_VK_M: 77,
        DOM_VK_N: 78,
        DOM_VK_O: 79,
        DOM_VK_P: 80,
        DOM_VK_Q: 81,
        DOM_VK_R: 82,
        DOM_VK_S: 83,
        DOM_VK_T: 84,
        DOM_VK_U: 85,
        DOM_VK_V: 86,
        DOM_VK_W: 87,
        DOM_VK_X: 88,
        DOM_VK_Y: 89,
        DOM_VK_Z: 90,
        DOM_VK_CONTEXT_MENU: 93,
        DOM_VK_NUMPAD0: 96,
        DOM_VK_NUMPAD1: 97,
        DOM_VK_NUMPAD2: 98,
        DOM_VK_NUMPAD3: 99,
        DOM_VK_NUMPAD4: 100,
        DOM_VK_NUMPAD5: 101,
        DOM_VK_NUMPAD6: 102,
        DOM_VK_NUMPAD7: 103,
        DOM_VK_NUMPAD8: 104,
        DOM_VK_NUMPAD9: 105,
        DOM_VK_MULTIPLY: 106,
        DOM_VK_ADD: 107,
        DOM_VK_SEPARATOR: 108,
        DOM_VK_SUBTRACT: 109,
        DOM_VK_DECIMAL: 110,
        DOM_VK_DIVIDE: 111,
        DOM_VK_F1: 112,
        DOM_VK_F2: 113,
        DOM_VK_F3: 114,
        DOM_VK_F4: 115,
        DOM_VK_F5: 116,
        DOM_VK_F6: 117,
        DOM_VK_F7: 118,
        DOM_VK_F8: 119,
        DOM_VK_F9: 120,
        DOM_VK_F10: 121,
        DOM_VK_F11: 122,
        DOM_VK_F12: 123,
        DOM_VK_F13: 124,
        DOM_VK_F14: 125,
        DOM_VK_F15: 126,
        DOM_VK_F16: 127,
        DOM_VK_F17: 128,
        DOM_VK_F18: 129,
        DOM_VK_F19: 130,
        DOM_VK_F20: 131,
        DOM_VK_F21: 132,
        DOM_VK_F22: 133,
        DOM_VK_F23: 134,
        DOM_VK_F24: 135,
        DOM_VK_NUM_LOCK: 144,
        DOM_VK_SCROLL_LOCK: 145,
        DOM_VK_COMMA: 188,
        DOM_VK_PERIOD: 190,
        DOM_VK_SLASH: 191,
        DOM_VK_BACK_QUOTE: 192,
        DOM_VK_OPEN_BRACKET: 219,
        DOM_VK_BACK_SLASH: 220,
        DOM_VK_CLOSE_BRACKET: 221,
        DOM_VK_QUOTE: 222,
        DOM_VK_META: 224
    };
}

var goal_setting_values = [ 0,   1,   2,    3,    4,    5,    6];
var goal_true_values =    [10, 100, 500, 1000, 2000, 5000, 10000];

	document.addEventListener('DOMContentLoaded', function () 
	{
		//Fix for people who had the dreaded default option.
        localStorage.removeItem('ht_00000000');

        //Remove all localStorage mostmice entries.
		remove_local_mostmice();

		$('#table_tab2').hide();
		$('#table_tab3').hide();
		$('#table_tab4').hide();
		$('#table_tab5').hide();

		HelpToolTip.enableTooltips();
		populate_fb_link();
			
		$('#notification_time_slider').slider({ 
			min: 1, 
			max: 120,
			orientation: "horizontal",
			range: "min",
			slide: function(event, ui) {
				if (ui.value == 1)
					$('#notification_time_display').html(ui.value + '</br></br>second');
				else
					$('#notification_time_display').html(ui.value + '</br></br>seconds');
			}
		});

		var goal_slider = $('#goal_count_slider').slider({ 
			min: 0, 
			max: 6,
			orientation: "horizontal",
			range: "min",
			slide: function(event, ui) {
				var includeLeft = event.keyCode != $.ui.keyCode.RIGHT;
	            var includeRight = event.keyCode != $.ui.keyCode.LEFT;
	            var value = findNearest(includeLeft, includeRight, ui.value);

	            goal_slider.slider("value", value); 
				
				$('#goal_count_display').html(getRealValue(value) + '</br></br>catches');
			}
		});

		$('#badge_text').combobox();
		$('#fb_link').combobox();
		$('#fb_link').on('comboboxselect', fb_link_change);

		$('#cheese_ordering').combobox();
		$('#potion_ordering').combobox();
		$('#charm_ordering').combobox();
		$('#special_ordering').combobox();

		$('button').button();
		$('#display_text_format').css('height', '100px');
		$('#display_text_format').css('width', '100%');

		$('#TEM_text_format').css('width', '100%');

		if (document.getElementById("fb_link").children.length == 0)
		{
			//Message indicating what needs to be done to populate messages.
			display_error('No users have been seen using HornTracker since this version has been installed. </br>Please submit a hunt, or convertible to associate a user with options.');
			
			var controls = $('.ht_control');
			for (var i = 0; i < controls.length; i++)
			{
				controls[i].disabled = true;
				controls[i].classList.add('ui-state-disabled');
			}
		}
		else
		{
			//Request existing settings from server?
			populate_existing_settings();

			restore_options(JSON.parse(localStorage.getItem('ht_' + document.getElementById('fb_link').value)));
			
			//TODO: Populate admin note with notes from the server for the user.
			document.getElementById('btn_apply').addEventListener('click', handle_apply);
			document.getElementById('btn_reset_usr').addEventListener('click', handle_user_reset);
			document.getElementById('btn_reset_all').addEventListener('click', handle_all_reset);
			document.getElementById('btn_notification_reset').addEventListener('click', handle_notification_reset);

			document.getElementById('li_tab1').addEventListener('click', handle_tab1);
			document.getElementById('li_tab2').addEventListener('click', handle_tab2);
			document.getElementById('li_tab3').addEventListener('click', handle_tab3);
			document.getElementById('li_tab4').addEventListener('click', handle_tab4);
			document.getElementById('li_tab5').addEventListener('click', handle_tab5);

			//document.getElementById('fb_link').addEventListener('change', fb_link_change);

			document.getElementById('myonoffswitch_profile_sharing').addEventListener('click', handle_profile_sharing);

			$('#suggested_scoreboard_name').data('old', $('#suggested_scoreboard_name').val());
			$('#suggested_scoreboard_name').on('keypress', altname_keycheck);
			$('#suggested_scoreboard_name').on('input propertychange', altname_lengthcheck);
		}

		//More changes for profile enabled/disabled.
	});

	function findNearest(includeLeft, includeRight, value) 
	{
        var nearest = null;
        var diff = null;
        for (var i = 0; i < goal_setting_values.length; i++) {
            if ((includeLeft && goal_setting_values[i] <= value) || (includeRight && goal_setting_values[i] >= value)) {
                var newDiff = Math.abs(value - goal_setting_values[i]);
                if (diff == null || newDiff < diff) {
                    nearest = goal_setting_values[i];
                    diff = newDiff;
                }
            }
        }
        return nearest;
    }

    function getRealValue(sliderValue) 
    {
        for (var i = 0; i < goal_setting_values.length; i++) {
            if (goal_setting_values[i] >= sliderValue) {
                return goal_true_values[i];
            }
        }
        return 0;
    }

    function getSettingValue(sliderValue) 
    {
        for (var i = 0; i < goal_true_values.length; i++) {
            if (goal_true_values[i] == sliderValue) {
                return goal_setting_values[i];
            }
        }
        return 0;
    }

	function populate_fb_link()
	{
		var link = document.getElementById("fb_link");
		for (var i = 0; i < localStorage.length; i++)
		{
			if (localStorage.key(i).match(/^ht_[0-9]*$/) != null)
			{
				var e_opt = document.createElement("option");
				var settingsJSON = JSON.parse(localStorage.getItem(localStorage.key(i)));
				e_opt.setAttribute('value', localStorage.key(i).match(/[0-9]*$/)[0]);
				link.appendChild(e_opt);
				e_opt.appendChild(document.createTextNode(settingsJSON.ht_default_name));
			}
		}
	}

	function populate_existing_settings()
	{
		var fb_id = document.getElementById('fb_link').value;
		var get_settings_port = chrome.runtime.connect({name: "get_settings_request"});

		get_settings_port.postMessage({request: "getrequest", url: 'http://www.horntracker.com/backend/getoptions.php?fbid=' + fb_id});
		get_settings_port.onMessage.addListener(function(msg) {
			if (is_JSON_string(msg.html))
				restore_server_options(JSON.parse(msg.html));
			else
			{
				display_error('Could not retrieve existing settings, options you see below</br> are the last saved settings on this machine and may not represent your settings saved on the server.</br></br>');
				restore_options(JSON.parse(localStorage.getItem('ht_' + document.getElementById('fb_link').value)));
			}
		});
	}

	//Should save to localStorage and then submit changes to the server.
	function handle_apply(e) 
	{
		//Remove any previously existing error messages.
		$('#admin_note').empty();

		//Remove any previously existing error classes from ht_controls.
		$('.ht_control').removeClass('ui-state-error');

		var fb_id = document.getElementById('fb_link').value;

		//localStorage save.
		var retMes = save_options(fb_id);
		if (retMes != "")
		{
			display_error(retMes);
			return; 
		}

		submit_saved_options(fb_id);
	}

	function submit_saved_options(fb_id)
	{
		var status = document.getElementById("status");
		status.innerHTML = "Waiting for response from server...";

		var ht_params = localStorage.getItem('ht_' + fb_id);
		var fb_name = JSON.parse(ht_params).ht_default_name;

		var submit_options_port = chrome.runtime.connect({name: "post_options_request"});

		//Submission of changes to server.
		submit_options_port.postMessage({request: "postrequest", url: 'http://www.horntracker.com/backend/submit/optionsubmit.php', ht_post_params: JSON.parse(ht_params), fbid:fb_id, default_name:fb_name});
		submit_options_port.onMessage.addListener(function(msg) {
			var enter_JSON = JSON.parse(msg.html);
			//Update status to let user know options were saved.
			var status = document.getElementById("status");
			status.innerHTML = enter_JSON.toDisplay;
			setTimeout(function() {	status.innerHTML = ""; }, 2750);
		});

		//Submission of changes to extension.
		var options_listener_port = chrome.runtime.connect({name: "options_listener"});

		options_listener_port.postMessage({settings: ht_params});
	}

	function handle_user_reset(e) 
	{
		reset_options('ht_' + document.getElementById('fb_link').value);

		//Submit reset options to the server.
		submit_saved_options(document.getElementById('fb_link').value);

		restore_options(JSON.parse(localStorage.getItem('ht_' + document.getElementById('fb_link').value)));
	}

	function handle_all_reset(e) 
	{
		//Loop through all children of fb_link and reset them to default as well.
		var link = document.getElementById('fb_link');

		for (var i = 0; i < link.children.length; i++)
		{
			reset_options('ht_' + link.children[i].value);

			//Submit reset options to the server.
			submit_saved_options(link.children[i].value);
		}

		restore_options(JSON.parse(localStorage.getItem('ht_' + document.getElementById('fb_link').value)));
	}

	function handle_notification_reset(e)
	{
		var user_str = 'ht_' + document.getElementById('fb_link').value;
		var storageJSON = JSON.parse(localStorage.getItem(user_str));

		storageJSON['ht_box_position_fb'] = '50px_300px';
		storageJSON['ht_box_position_mhg'] = '50px_300px';

		localStorage.setItem(user_str, JSON.stringify(storageJSON));
	} 

	function altname_keycheck(e)
	{
		var key = String.fromCharCode(e.keyCode);
		if (e.keyCode != KeyEvent.DOM_VK_BACK_SPACE && 
			e.keyCode != KeyEvent.DOM_VK_TAB && 
			e.keyCode != KeyEvent.DOM_VK_SHIFT &&
			e.keyCode != KeyEvent.DOM_VK_CONTROL &&
			e.keyCode != KeyEvent.DOM_VK_ALT &&
			e.keyCode != KeyEvent.DOM_VK_CAPS_LOCK &&
			e.keyCode != KeyEvent.DOM_VK_SPACE &&
			e.keyCode != KeyEvent.DOM_VK_END &&
			e.keyCode != KeyEvent.DOM_VK_HOME &&
			e.keyCode != KeyEvent.DOM_VK_LEFT &&
			e.keyCode != KeyEvent.DOM_VK_RIGHT &&
			e.keyCode != KeyEvent.DOM_VK_INSERT &&
			e.keyCode != KeyEvent.DOM_VK_DELETE)
		{
			var regex = /[a-zA-Z0-9\[\]\.@$]/;
			if(!regex.test(key))
			{
				e.returnValue = false;
				if(e.preventDefault)
					e.preventDefault();
			}
		}
	}

	function altname_lengthcheck(e)
	{
		//NOTE: Should actively never let you be bigger than 64 chars.
		if (e.currentTarget.value.length > 64)
		{
			var input_elem = $('#suggested_scoreboard_name');
			input_elem.val(input_elem.data('old'));
			//TODO: Notification for bad data.
		}
		else
		{
			$('#suggested_scoreboard_name').data('old', e.currentTarget.value);
		}
	}

	function handle_tab1(e)
	{
		if (document.body.id == 'tab2')
		{
			$('#table_tab2').fadeOut(300, function(){
				$('#table_tab1').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab3')
		{
			$('#table_tab3').fadeOut(300, function(){
				$('#table_tab1').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab4')
		{
			$('#table_tab4').fadeOut(300, function(){
				$('#table_tab1').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab5')
		{
			$('#table_tab5').fadeOut(300, function(){
				$('#table_tab1').fadeIn(300, function(){});
			});
		}
		else
			return;

		document.body.id='tab1';
	}

	function handle_tab2(e)
	{
		if (document.body.id == 'tab1')
		{
			$('#table_tab1').fadeOut(300, function(){
				$('#table_tab2').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab3')
		{
			$('#table_tab3').fadeOut(300, function(){
				$('#table_tab2').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab4')
		{
			$('#table_tab4').fadeOut(300, function(){
				$('#table_tab2').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab5')
		{
			$('#table_tab5').fadeOut(300, function(){
				$('#table_tab2').fadeIn(300, function(){});
			});
		}
		else
			return;

		document.body.id='tab2';
	}

	function handle_tab3(e)
	{
		if (document.body.id == 'tab1')
		{
			$('#table_tab1').fadeOut(300, function(){
				$('#table_tab3').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab2')
		{
			$('#table_tab2').fadeOut(300, function(){
				$('#table_tab3').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab4')
		{
			$('#table_tab4').fadeOut(300, function(){
				$('#table_tab3').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab5')
		{
			$('#table_tab5').fadeOut(300, function(){
				$('#table_tab3').fadeIn(300, function(){});
			});
		}
		else
			return;

		document.body.id='tab3';
	}

	function handle_tab4(e)
	{
		if (document.body.id == 'tab1')
		{
			$('#table_tab1').fadeOut(300, function(){
				$('#table_tab4').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab2')
		{
			$('#table_tab2').fadeOut(300, function(){
				$('#table_tab4').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab3')
		{
			$('#table_tab3').fadeOut(300, function(){
				$('#table_tab4').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab5')
		{
			$('#table_tab5').fadeOut(300, function(){
				$('#table_tab4').fadeIn(300, function(){});
			});
		}
		else
			return;

		document.body.id='tab4';
	}
	function handle_tab5(e)
	{
		if (document.body.id == 'tab1')
		{
			$('#table_tab1').fadeOut(300, function(){
				$('#table_tab5').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab2')
		{
			$('#table_tab2').fadeOut(300, function(){
				$('#table_tab5').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab3')
		{
			$('#table_tab3').fadeOut(300, function(){
				$('#table_tab5').fadeIn(300, function(){});
			});
		}
		else if (document.body.id == 'tab4')
		{
			$('#table_tab4').fadeOut(300, function(){
				$('#table_tab5').fadeIn(300, function(){});
			});
		}
		else
			return;

		document.body.id='tab5';
	}

	function handle_profile_sharing(e)
	{
		if (e.currentTarget.checked == false)
		{
			$('#cheese_option_sharing input')[0].disabled = true;
			$('#cheese_option_sharing')[0].classList.add('ui-state-disabled');
			setup_checked('cheese_option_sharing', false);

			$('#potion_option_sharing input')[0].disabled = true;
			$('#potion_option_sharing')[0].classList.add('ui-state-disabled');
			setup_checked('potion_option_sharing', false);
			
			$('#charm_option_sharing input')[0].disabled = true;
			$('#charm_option_sharing')[0].classList.add('ui-state-disabled');
			setup_checked('charm_option_sharing', false);
			
			$('#special_option_sharing input')[0].disabled = true;
			$('#special_option_sharing')[0].classList.add('ui-state-disabled');
			setup_checked('special_option_sharing', false);

			$('#journal_summary_option_sharing input')[0].disabled = true;
			$('#journal_summary_option_sharing')[0].classList.add('ui-state-disabled');
			setup_checked('journal_summary_option_sharing', false);

			$('#tacky_glue_option_sharing input')[0].disabled = true;
			$('#tacky_glue_option_sharing')[0].classList.add('ui-state-disabled');
			setup_checked('tacky_glue_option_sharing', false);
		}
		else
		{
			$('#cheese_option_sharing input')[0].disabled = false;
			$('#cheese_option_sharing')[0].classList.remove('ui-state-disabled');

			$('#potion_option_sharing input')[0].disabled = false;
			$('#potion_option_sharing')[0].classList.remove('ui-state-disabled');
			
			$('#charm_option_sharing input')[0].disabled = false;
			$('#charm_option_sharing')[0].classList.remove('ui-state-disabled');
			
			$('#special_option_sharing input')[0].disabled = false;
			$('#special_option_sharing')[0].classList.remove('ui-state-disabled');

			$('#journal_summary_option_sharing input')[0].disabled = false;
			$('#journal_summary_option_sharing')[0].classList.remove('ui-state-disabled');

			$('#tacky_glue_option_sharing input')[0].disabled = false;
			$('#tacky_glue_option_sharing')[0].classList.remove('ui-state-disabled');
		}
	}

	function fb_link_change(e, ui)
	{
		//Retrieve existing server settings for new user.
		populate_existing_settings();

		restore_options(JSON.parse(localStorage.getItem('ht_' + e.currentTarget.value)));
	}

	function restore_options(settingsJSON)
	{
		//------------------|
		//    Scoreboard    |
		//------------------|

		//Scoreboard name and profile shown on scoreboard pages.
		if (is_set(settingsJSON.ht_is_opted_in_hunt))
			setup_checked('scoreboard_option', settingsJSON.ht_is_opted_in_hunt);
		else
			setup_checked('scoreboard_option', false);

		//Scoreboard name and profile shown on convertible pages.
		if (is_set(settingsJSON.ht_is_opted_in_convertible))
			setup_checked('convertible_option', settingsJSON.ht_is_opted_in_convertible);
		else
			setup_checked('convertible_option', false);

		//Crowntracker enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_crown))
			setup_checked('crowntracker_option', settingsJSON.ht_is_opted_in_crown);
		else
			setup_checked('crowntracker_option', false);

		//Mouse scoreboards enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_mice))
			setup_checked('mouseboard_option', settingsJSON.ht_is_opted_in_mice);
		else
			setup_checked('mouseboard_option', false);

		//Alternate name
		if (is_set(settingsJSON.ht_alt_name))
			document.getElementById("suggested_scoreboard_name").value = settingsJSON.ht_alt_name;
		else
			document.getElementById("suggested_scoreboard_name").value = '';

		//------------------|
		//   User Profile   |
		//------------------|

		//User profile enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_profile))
			setup_checked('profile_option', settingsJSON.ht_is_opted_in_profile);
		else
			setup_checked('profile_option', false);

		//Cheese submission enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_cheese))
			setup_checked('cheese_option', settingsJSON.ht_is_opted_in_cheese);
		else
			setup_checked('cheese_option', false);

		//Potion submission enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_potions))
			setup_checked('potion_option', settingsJSON.ht_is_opted_in_potions);
		else
			setup_checked('potion_option', false);

		//Charm submission enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_charms))
			setup_checked('charm_option', settingsJSON.ht_is_opted_in_charms);
		else
			setup_checked('charm_option', false);
		
		//Special tab submission enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_special))
			setup_checked('special_option', settingsJSON.ht_is_opted_in_special);
		else
			setup_checked('special_option', false);

		//Journal summary enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_journal))
			setup_checked('journal_summary_option', settingsJSON.ht_is_opted_in_journal);
		else
			setup_checked('journal_summary_option', false);

		//Tacky glue setups enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_tacky))
			setup_checked('tacky_glue_option', settingsJSON.ht_is_opted_in_tacky);
		else
			setup_checked('tacky_glue_option', false);

		//Public hunts enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_public_hunts))
			setup_checked('public_hunts_option', settingsJSON.ht_is_opted_in_public_hunts);
		else
			setup_checked('public_hunts_option', false);

		//User profile sharing enabled/disabled.
		//TODO: if disabled, should disable all other profile sharing options.
		var profile_share = false; //Default
		if (is_set(settingsJSON.ht_is_sharing_profile))
		{
			if (settingsJSON.ht_is_sharing_profile)
				profile_share = true;

			setup_checked('profile_option_sharing', settingsJSON.ht_is_sharing_profile);
		}
		else
			setup_checked('profile_option_sharing', false);

		if (profile_share)
		{
			//Cheese sharing enabled/disabled.
			if (is_set(settingsJSON.ht_is_sharing_cheese))
				setup_checked('cheese_option_sharing', settingsJSON.ht_is_sharing_cheese);
			else
				setup_checked('cheese_option_sharing', false);

			//Potion sharing enabled/disabled.
			if (is_set(settingsJSON.ht_is_sharing_potions))
				setup_checked('potion_option_sharing', settingsJSON.ht_is_sharing_potions);
			else
				setup_checked('potion_option_sharing', false);

			//Charm sharing enabled/disabled.
			if (is_set(settingsJSON.ht_is_sharing_charms))
				setup_checked('charm_option_sharing', settingsJSON.ht_is_sharing_charms);
			else
				setup_checked('charm_option_sharing', false);
			
			//Special tab sharing enabled/disabled.
			if (is_set(settingsJSON.ht_is_sharing_special))
				setup_checked('special_option_sharing', settingsJSON.ht_is_sharing_special);
			else
				setup_checked('special_option_sharing', false);

			//Journal summary sharing enabled/disabled.
			if (is_set(settingsJSON.ht_is_sharing_journal))
				setup_checked('journal_summary_option_sharing', settingsJSON.ht_is_sharing_journal);
			else
				setup_checked('journal_summary_option_sharing', false);

			//Tacky glue setup sharing enabled/disabled.
			if (is_set(settingsJSON.ht_is_sharing_tacky))
				setup_checked('tacky_glue_option_sharing', settingsJSON.ht_is_sharing_tacky);
			else
				setup_checked('tacky_glue_option_sharing', false);
		}
		else
		{
			setup_checked('cheese_option_sharing', false);
			setup_checked('potion_option_sharing', false);
			setup_checked('charm_option_sharing', false);
			setup_checked('special_option_sharing', false);
			setup_checked('journal_summary_option_sharing', false);
			setup_checked('tacky_glue_option_sharing', false);

			//Disable the sharing switches.
			$('#cheese_option_sharing input')[0].disabled = true;
			$('#potion_option_sharing input')[0].disabled = true;
			$('#charm_option_sharing input')[0].disabled = true;
			$('#special_option_sharing input')[0].disabled = true;
			$('#journal_summary_option_sharing input')[0].disabled = true;
			$('#tacky_glue_option_sharing input')[0].disabled = true;

			$('#cheese_option_sharing')[0].classList.add('ui-state-disabled');
			$('#potion_option_sharing')[0].classList.add('ui-state-disabled');
			$('#charm_option_sharing')[0].classList.add('ui-state-disabled');
			$('#special_option_sharing')[0].classList.add('ui-state-disabled');
			$('#journal_summary_option_sharing')[0].classList.add('ui-state-disabled');
			$('#tacky_glue_option_sharing')[0].classList.add('ui-state-disabled');
		}

		//Cheese profile ordering.
		if (is_set(settingsJSON.ht_cheese_order))
			$('#cheese_ordering').combobox('value', settingsJSON.ht_cheese_order.toString());
		else
			$('#cheese_ordering').combobox('value', 'ht_cheese_az');

		//Potion profile ordering.
		if (is_set(settingsJSON.ht_potion_order))
			$('#potion_ordering').combobox('value', settingsJSON.ht_potion_order.toString());
		else
			$('#potion_ordering').combobox('value', 'ht_potion_az');

		//Charm profile ordering.
		if (is_set(settingsJSON.ht_charm_order))
			$('#charm_ordering').combobox('value', settingsJSON.ht_charm_order.toString());
		else
			$('#charm_ordering').combobox('value', 'ht_charm_az');

		//Special profile ordering.
		if (is_set(settingsJSON.ht_special_order))
			$('#special_ordering').combobox('value', settingsJSON.ht_special_order.toString());
		else
			$('#special_ordering').combobox('value', 'ht_special_az');

		//------------------|
		// UI Modifications |
		//------------------|

		//Display actual attraction rates instead of attraction bonus in the trap selector screen.
		if (is_set(settingsJSON.ht_real_ar))
			setup_checked('ab_display_change', settingsJSON.ht_real_ar);
		else
			setup_checked('ab_display_change', false);

		//Change stale in user selection from words to actual percentages in HT.
		if (is_set(settingsJSON.ht_real_stale))
			setup_checked("stale_display_change", settingsJSON.ht_real_stale);
		else
			setup_checked("stale_display_change", false);

		//Show/hide hidden items on the special tab.
		if (is_set(settingsJSON.ht_is_reveal_hidden_special))
			setup_checked("hidden_specials", settingsJSON.ht_is_reveal_hidden_special);
		else
			setup_checked("hidden_specials", false);

		//Display platinum crowns.
		if (is_set(settingsJSON.ht_is_platinum_crown))
			setup_checked('show_plat', settingsJSON.ht_is_platinum_crown);
		else
			setup_checked('show_plat', false);

		//Display top-100 badges.
		if (is_set(settingsJSON.ht_is_mouse_badge))
			setup_checked("show_mbadges", settingsJSON.ht_is_mouse_badge);
		else
			setup_checked("show_mbadges", false);

		//Display mouse catch goals.
		if (is_set(settingsJSON.ht_is_opted_in_goals))
			setup_checked('show_goal', settingsJSON.ht_is_opted_in_goals);
		else
			setup_checked('show_goal', false);

		//TODO: Should this use setup_option?
		//Mouse catch goal count.
		if (is_set(settingsJSON.ht_is_opted_in_goals_amount))
		{
			$('#goal_count_slider').slider("value", getSettingValue(settingsJSON.ht_is_opted_in_goals_amount));
			if (settingsJSON.ht_is_opted_in_goals_amount == 1)
				$('#goal_count_display').html(settingsJSON.ht_is_opted_in_goals_amount + '</br></br>catch');
			else
				$('#goal_count_display').html(settingsJSON.ht_is_opted_in_goals_amount + '</br></br>catches');
		}
		else
		{
			$('#goal_count_slider').slider("value", '1');
			$('#goal_count_display').html('100</br></br>catches');
		}

		//Modify TEM display.
		if (is_set(settingsJSON.ht_is_opted_in_modify_TEM))
			setup_checked('modify_TEM', settingsJSON.ht_is_opted_in_modify_TEM);
		else
			setup_checked('modify_TEM', false);

		//TEM display message
		if (is_set(settingsJSON.ht_TEM_display_style))
			document.getElementById("TEM_text_format").value = settingsJSON.ht_TEM_display_style;
		else
			document.getElementById("TEM_text_format").value = 'AR: %ar%</br>CR: %cr%</br>ECR: %ecr%';

		//TODO: Eliminate this one? Or convert to something else maybe?
		//Display charm instead of trap power in the HUD.
		/*
		if (is_set(settingsJSON.ht_charm_not_trap))
			setup_checked('charm_over_power', settingsJSON.ht_charm_not_trap);
		else
			setup_checked('charm_over_power', false);
		*/

		//------------------|
		//   Notifications  |
		//------------------|

		//Notification display enabled/disabled. (By default this is on)
		if (is_set(settingsJSON.ht_notification))
			setup_checked('notification_display', settingsJSON.ht_notification);
		else
			setup_checked('notification_display', true);

		//TODO: Should this use setup_option?
		//Notification display time.
		if (is_set(settingsJSON.ht_notification_display_time))
		{
			$('#notification_time_slider').slider("value", settingsJSON.ht_notification_display_time);
			if (settingsJSON.ht_notification_display_time == 1)
				$('#notification_time_display').html(settingsJSON.ht_notification_display_time + '</br></br>second');
			else
				$('#notification_time_display').html(settingsJSON.ht_notification_display_time + '</br></br>seconds');
		}
		else
		{
			$('#notification_time_slider').slider("value", '10');
			$('#notification_time_display').html('10</br></br>seconds');
		}

		//Notification message
		if (is_set(settingsJSON.ht_message_style))
			document.getElementById("display_text_format").value = settingsJSON.ht_message_style;
		else
			document.getElementById("display_text_format").value = 'You successfully submitted %s hunt(s)!';

		//------------------|
		//       Misc       |
		//------------------|

		//Badge text option.
		if (is_set(settingsJSON.ht_badge_text))
			$('#badge_text').combobox('value', settingsJSON.ht_badge_text.toString());
		else
			$('#badge_text').combobox('value', 'ht_badge_none');

		//MostMice profiling enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_mostmice))
			setup_checked("local_profiles", settingsJSON.ht_is_opted_in_mostmice);
		else
			setup_checked("local_profiles", false);

		//Crafting submissions enabled/disabled.
		if (is_set(settingsJSON.ht_is_opted_in_crafting))
			setup_checked("crafting_submissions", settingsJSON.ht_is_opted_in_crafting);
		else
			setup_checked("crafting_submissions", false);
	}

	//This omits any modification of non-server stored options.
	function restore_server_options(settingsJSON)
	{
		setup_checked('scoreboard_option', settingsJSON.ht_is_opted_in_hunt);	
		setup_checked('convertible_option', settingsJSON.ht_is_opted_in_convertible);
		setup_checked('crowntracker_option', settingsJSON.ht_is_opted_in_crown);
		setup_checked('mouseboard_option', settingsJSON.ht_is_opted_in_mice);
		setup_checked('profile_option', settingsJSON.ht_is_opted_in_profile);
		setup_checked('cheese_option', settingsJSON.ht_is_opted_in_cheese);
		setup_checked('potion_option', settingsJSON.ht_is_opted_in_potions);
		setup_checked('charm_option', settingsJSON.ht_is_opted_in_charms);
		setup_checked('special_option', settingsJSON.ht_is_opted_in_special);
		setup_checked('journal_summary_option', settingsJSON.ht_is_opted_in_journal);
		setup_checked('tacky_glue_option', settingsJSON.ht_is_opted_in_tacky);
		setup_checked('public_hunts_option', settingsJSON.ht_is_opted_in_public_hunts);
		
		setup_checked('stale_display_change', settingsJSON.ht_real_stale);

		setup_checked('profile_option_sharing', settingsJSON.ht_is_sharing_profile);
		setup_checked('cheese_option_sharing', settingsJSON.ht_is_sharing_cheese);
		setup_checked('potion_option_sharing', settingsJSON.ht_is_sharing_potions);
		setup_checked('charm_option_sharing', settingsJSON.ht_is_sharing_charms);
		setup_checked('special_option_sharing', settingsJSON.ht_is_sharing_special);
		setup_checked('journal_summary_option_sharing', settingsJSON.ht_is_sharing_journal);
		setup_checked('tacky_glue_option_sharing', settingsJSON.ht_is_sharing_tacky);

		//Cheese profile ordering.
		if (settingsJSON.ht_cheese_order.toString() != '')
			$('#cheese_ordering').combobox('value', settingsJSON.ht_cheese_order.toString());
		else
			$('#cheese_ordering').combobox('value', 'ht_cheese_az');

		//Potion profile ordering.
		if (settingsJSON.ht_potion_order.toString() != '')
			$('#potion_ordering').combobox('value', settingsJSON.ht_potion_order.toString());
		else
			$('#potion_ordering').combobox('value', 'ht_potion_az');

		//Charm profile ordering.
		if (settingsJSON.ht_charm_order.toString() != '')
			$('#charm_ordering').combobox('value', settingsJSON.ht_charm_order.toString());
		else
			$('#charm_ordering').combobox('value', 'ht_charm_az');

		//Special profile ordering.
		if (settingsJSON.ht_special_order.toString() != '')
			$('#special_ordering').combobox('value', settingsJSON.ht_special_order.toString());
		else
			$('#special_ordering').combobox('value', 'ht_special_az');

		if (settingsJSON.ht_badge_text.toString() != '')
			$('#badge_text').combobox('value', settingsJSON.ht_badge_text.toString());
		else
			$('#badge_text').combobox('value', 'ht_badge_none');

		setup_checked('hidden_specials', settingsJSON.ht_is_reveal_hidden_special);

		if (settingsJSON.ht_notification_display_time != '')
		{
			$('#notification_time_slider').slider("value", settingsJSON.ht_notification_display_time);
			if (settingsJSON.ht_notification_display_time == 1)
				$('#notification_time_display').html(settingsJSON.ht_notification_display_time + '</br></br>second');
			else
				$('#notification_time_display').html(settingsJSON.ht_notification_display_time + '</br></br>seconds');
		}
		else
		{
			$('#notification_time_slider').slider("value", '10');
			$('#notification_time_display').html('10</br></br>seconds');
		}

		if (settingsJSON.ht_is_opted_in_goals_amount != '')
		{
			$('#goal_count_slider').slider("value", getSettingValue(settingsJSON.ht_is_opted_in_goals_amount));
			if (settingsJSON.ht_is_opted_in_goals_amount == 1)
				$('#goal_count_display').html(settingsJSON.ht_is_opted_in_goals_amount + '</br></br>catch');
			else
				$('#goal_count_display').html(settingsJSON.ht_is_opted_in_goals_amount + '</br></br>catches');
		}
		else
		{
			$('#goal_count_slider').slider("value", '1');
			$('#goal_count_display').html('100</br></br>catches');
		}

		setup_checked('notification_display', settingsJSON.ht_notification);

		setup_checked('ab_display_change', settingsJSON.ht_real_ar);
		setup_checked('show_plat', settingsJSON.ht_is_platinum_crown);
		setup_checked("show_mbadges", settingsJSON.ht_is_mouse_badge);
		setup_checked('show_goal', settingsJSON.ht_is_opted_in_goals);

		setup_checked("local_profiles", settingsJSON.ht_is_opted_in_mostmice);
		setup_checked("crafting_submissions", settingsJSON.ht_is_opted_in_crafting);

		//Alternate name
		document.getElementById("suggested_scoreboard_name").value = settingsJSON.ht_alt_name;

		//Notification message
		document.getElementById("display_text_format").value = settingsJSON.ht_message_style;

		//Modify TEM display.
		setup_checked('modify_TEM', settingsJSON.ht_is_opted_in_modify_TEM);
		//TEM display message
		document.getElementById("TEM_text_format").value = settingsJSON.ht_TEM_display_style;
	}

	function setup_option(option_id, option_value)
	{
		var option_element = document.getElementById(option_id);
		for (var i = 0; i < option_element.children.length; i++)
		{
			if (option_element.children[i].value == option_value)
			{
				option_element.children[i].selected = true;
				break;
			}
		}

		//TODO: Do something when an option isn't found?
	}

	function setup_checked(check_id, check_value)
	{
		$('input:checkbox', document.getElementById(check_id))[0].checked = check_value;

		//TODO: Do something when a checkbox isn't found?
	}

	// Saves options to localStorage.
	function save_options(user)
	{
		var storageJSON = JSON.parse(localStorage.getItem('ht_' + user));

		storageJSON['ht_is_opted_in_hunt'] = $('#scoreboard_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_convertible'] = $('#convertible_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_crown'] = $('#crowntracker_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_mice'] = $('#mouseboard_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_cheese'] = $('#cheese_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_potions'] = $('#potion_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_charms'] = $('#charm_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_special'] = $('#special_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_profile'] = $('#profile_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_journal'] = $('#journal_summary_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_tacky'] = $('#tacky_glue_option input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_public_hunts'] = $('#public_hunts_option input:checkbox')[0].checked;

        storageJSON['ht_is_sharing_cheese'] = $('#cheese_option_sharing input:checkbox')[0].checked;
        storageJSON['ht_is_sharing_potions'] = $('#potion_option_sharing input:checkbox')[0].checked;
        storageJSON['ht_is_sharing_charms'] = $('#charm_option_sharing input:checkbox')[0].checked;
        storageJSON['ht_is_sharing_special'] = $('#special_option_sharing input:checkbox')[0].checked;
        storageJSON['ht_is_sharing_profile'] = $('#profile_option_sharing input:checkbox')[0].checked;
        storageJSON['ht_is_sharing_journal'] = $('#journal_summary_option_sharing input:checkbox')[0].checked;
        storageJSON['ht_is_sharing_tacky'] = $('#tacky_glue_option_sharing input:checkbox')[0].checked;

        storageJSON['ht_cheese_order'] = $('#cheese_ordering').combobox('value');
        storageJSON['ht_potion_order'] = $('#potion_ordering').combobox('value');
        storageJSON['ht_charm_order'] = $('#charm_ordering').combobox('value');
        storageJSON['ht_special_order'] = $('#special_ordering').combobox('value');

        //Do not let save occur here if we fail to meet our requirements.
        if (document.getElementById("suggested_scoreboard_name").value.length < 3 && 
			document.getElementById("suggested_scoreboard_name").value.length != 0)
        {
        	$('#suggested_scoreboard_name').addClass('ui-state-error');
        	return "Error: Alternate name may not be less than 3 characters, either remove it, or make it bigger.";
        }
        storageJSON['ht_alt_name'] = document.getElementById("suggested_scoreboard_name").value;
        
        storageJSON['ht_notification'] = $('#notification_display input:checkbox')[0].checked;

        storageJSON['ht_notification_display_time'] = $('#notification_time_slider').slider("value");

        storageJSON['ht_real_ar'] = $('#ab_display_change input:checkbox')[0].checked;
        storageJSON['ht_real_stale'] = $('#stale_display_change input:checkbox')[0].checked;
        storageJSON['ht_badge_text'] = $('#badge_text').combobox('value');
        storageJSON['ht_is_opted_in_mostmice'] = $('#local_profiles input:checkbox')[0].checked;
        storageJSON['ht_is_opted_in_crafting'] = $('#crafting_submissions input:checkbox')[0].checked;
        storageJSON['ht_is_reveal_hidden_special'] = $('#hidden_specials input:checkbox')[0].checked;
        storageJSON['ht_is_platinum_crown'] = $('#show_plat input:checkbox')[0].checked;
        storageJSON['ht_is_mouse_badge'] = $('#show_mbadges input:checkbox')[0].checked;

        storageJSON['ht_is_opted_in_goals'] = $('#show_goal input:checkbox')[0].checked;

        storageJSON['ht_is_opted_in_goals_amount'] = getRealValue($('#goal_count_slider').slider("value"));

        storageJSON['ht_message_style'] = document.getElementById("display_text_format").value;

        //Modify TEM display.
        storageJSON['ht_is_opted_in_modify_TEM'] = $('#modify_TEM input:checkbox')[0].checked;
		//TEM display message
		storageJSON['ht_TEM_display_style'] = document.getElementById("TEM_text_format").value;

        localStorage.setItem('ht_' + user, JSON.stringify(storageJSON));

        return "";
	}

	function reset_options(ht_user)
	{
		var storageJSON = { };

		//By default all scoreboard opt-ins are FALSE.
        storageJSON['ht_is_opted_in_hunt'] = false;
        storageJSON['ht_is_opted_in_convertible'] = false;
        storageJSON['ht_is_opted_in_crown'] = false;
        storageJSON['ht_is_opted_in_mice'] = false;

        storageJSON['ht_is_opted_in_profile'] = false;
        storageJSON['ht_is_opted_in_cheese'] = false;
        storageJSON['ht_is_opted_in_potions'] = false;
        storageJSON['ht_is_opted_in_charms'] = false;
        storageJSON['ht_is_opted_in_special'] = false;
        storageJSON['ht_is_opted_in_journal'] = false;
        storageJSON['ht_is_opted_in_tacky'] = false;
        storageJSON['ht_is_opted_in_public_hunts'] = false;

        storageJSON['ht_is_sharing_profile'] = false;
        storageJSON['ht_is_sharing_cheese'] = false;
        storageJSON['ht_is_sharing_potions'] = false;
        storageJSON['ht_is_sharing_charms'] = false;
        storageJSON['ht_is_sharing_special'] = false;
        storageJSON['ht_is_sharing_journal'] = false;
        storageJSON['ht_is_sharing_tacky'] = false;

        storageJSON['ht_cheese_order'] = 'ht_cheese_az';
        storageJSON['ht_potion_order'] = 'ht_potion_az';
        storageJSON['ht_charm_order'] = 'ht_charm_az';
        storageJSON['ht_special_order'] = 'ht_special_az';

        storageJSON['ht_default_name'] = JSON.parse(localStorage.getItem(ht_user)).ht_default_name;
        storageJSON['ht_alt_name'] = '';

        //By default notifications are on, and set for 10 seconds.
        storageJSON['ht_notification'] = true;
        storageJSON['ht_notification_display_time'] = 10;

        //By default attraction rate instead of attraction bonus is off as is stale rate instead of freshness.
        storageJSON['ht_real_ar'] = false;
        storageJSON['ht_real_stale'] = false;

        //By default platinum crown display is false.
        storageJSON['ht_is_platinum_crown'] = false;

        //By default mouse badges is false.
        storageJSON['ht_is_mouse_badge'] = false;

        //By default mouse catch goals are off, and set for 100 catches when on.
        storageJSON['ht_is_opted_in_goals'] = false;
        storageJSON['ht_is_opted_in_goals_amount'] = 100;

        //Default for badge text is none.
        storageJSON['ht_badge_text'] = 'ht_badge_none';

        //By default MostMice report checking is off.
        storageJSON['ht_is_opted_in_mostmice'] = false;

        //By default crafting submissions are off.
        storageJSON['ht_is_opted_in_crafting'] = false;

        //By default revealing hidden specials is off.
        storageJSON['ht_is_reveal_hidden_special'] = false;

        //Default notification message.
        storageJSON['ht_message_style'] = 'You successfully submitted %s hunt(s)!';

        //Modify TEM display.
        storageJSON['ht_is_opted_in_modify_TEM'] = false;
		//TEM display message
		storageJSON['ht_TEM_display_style'] = 'AR: %ar%</br>CR: %cr%</br>ECR: %ecr%';

        localStorage.setItem(ht_user, JSON.stringify(storageJSON));
	}

	function display_error(message)
	{
		var admin_note = document.getElementById("admin_note");
		admin_note.innerHTML = '<span style="color:#5C1015">' + message + '</span></br></br>';
	}
	
	function is_set(variable)
	{
		if (typeof variable === "undefined")
			return false;
		else
			return true;
	}

	function is_JSON_string(str) 
	{
		try 
		{
			JSON.parse(str);
		} 
		catch (e) 
		{
			return false;
		}

		return true;
	}

	function isNumber(n) 
	{
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	function update_status(message)
	{
		//TODO: Implement.
	}

	function remove_local_mostmice()
	{
		var i = 0;
	    var oJson = [];
	    var sKey;
		
		while ((sKey = localStorage.key(i)))
		{
			var key = sKey.match(/^mostmice_(\d+)$/);
			if (key !== null)
		    	oJson.push(key[0]);
		    i++;
		}

		//Prowl through oJson to remove from.
		for (var rec in oJson)
			localStorage.removeItem(oJson[rec]);
	}