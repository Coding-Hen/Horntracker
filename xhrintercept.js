var arguments;

var _is_xhri_setup = false;
var _trap_update_user_array = new Array();
var _timeout_called = false;
var _timeout_hidden_called = false;
var _timeout_plat_called = false;
var _cur_user_from_XHR = '';
var _last_user_options = {};

var _specials = {};

setupCommunications();

function setupCommunications()
{
	document.addEventListener("xhri_hidden_item", xhri_hidden_item, false);
	document.addEventListener("xhri_hidden_item_legacy", xhri_hidden_item_legacy, false);
	document.addEventListener("xhri_platinum_crown", xhri_platinum_crown, false);
	document.addEventListener("xhri_get_fb_user", xhri_get_fb_user, false);
	document.addEventListener("xhri_options_change", xhri_options_change, false);
	document.addEventListener("xhri_modify_TEM", xhri_modify_TEM, false);
	document.addEventListener("xhri_populate_hidden_specials", xhri_populate_hidden_specials, false);

	document.addEventListener("xhri_journal_summary", xhri_journal_summary, false);
	
	_is_xhri_setup = true;
}

//NOTE: These are legacy functions, used ONLY for the old mousehunt UI.
function xhri_hidden_item_legacy(aEvent)
{
    if (typeof hg == 'undefined' || 
        $(".inventoryitemview[data-view-id="+$($("#tabbarContent_page_4 .inventoryitemview")[0]).data("viewId")+"]").length <= 0)
    {
        timeout_hidden = setTimeout(function(){xhri_hidden_item_legacy(aEvent)}, 2000);
        return;
    }

    if (_timeout_hidden_called)
        return;

    _timeout_hidden_called = true;
    timeout_hidden = setTimeout(function(){call_modify_hidden_items_legacy()}, 2000);
}

function call_modify_hidden_items_legacy()
{
    var inv = hg.utils.UserInventory.getAllItems();
    for (var category in inv) if (inv.hasOwnProperty(category) && category != "length")
    {
        for (var item in inv[category]) if (inv[category].hasOwnProperty(item) && item != "length")
        {
            hg.utils.UserInventory.getAllItems()[category][item].is_hidden = false;
        }
    }

    //Empty the existing items:
    $(".inventoryitemview[data-view-id="+$($("#tabbarContent_page_4 .inventoryitemview")[0]).data("viewId")+"]").empty();

    //Render all the items again:
    app.views.InventoryItemView[$($("#tabbarContent_page_4 .inventoryitemview")[0]).data("viewId")].render();
}
//END LEGACY

function xhri_hidden_item(aEvent)
{
    //Setup _specials with the appropriate data
    _specials = JSON.parse(aEvent.prevValue);

    //TODO: Call another page.php and use that value to populate the html.
    var evt = document.createEvent("MutationEvents");
    evt.initMutationEvent("ht_getpage_special", true, true, null, JSON.stringify(_specials), '', 'ht_stuff', 1);
    document.dispatchEvent(evt);
}

function xhri_populate_hidden_specials(aEvent)
{
    //Convert aEvent to a valid tab value, and specials.
    var tabToRender = JSON.parse(aEvent.prevValue);
    var specs = JSON.parse(aEvent.newValue);

    //NOTE: DO NOT USE _specials in here as it's possible it's changed 
    //      or not available yet.
    var tag_made = {};
    for (var x in specs)
    {
        var item = specs[x];
        var internal_item_name = item.type;
        
        var tag_type = "unknown";
        if (is_set(item.tags))
            tag_type = item.tags[0].type;

        have_match = false;

        for (var i in tabToRender.subtabs)
        {
            var st = tabToRender.subtabs[i];
            for (var j in st.tags)
            {
                var tag = st.tags[j];
                for (var k in tag.items)
                {
                    var tag_item = tag.items[k];
                    if (tag_item.type == internal_item_name)
                    {
                        //NOTE: We have a match, stop looking and move on.
                        have_match = true;
                        break;
                    }
                }

                if (have_match)
                    break;
            }

            if (!have_match)
            {
                //TODO: Build new item from old item and populate to this tab.
                var new_item = {};
                //Convertible
                /*
                action_verb: "Open"
                classification: "convertible"
                css_class: ""
                description: "This scroll case smells faintly of roses. Who knows what the scroll inside will lead to? True love, maybe?"
                display_order: 0
                has_action: true
                is_convertible: true
                is_full: true
                is_multiple_open: null
                item_id: 1658
                name: "Valentine's Scroll Case"
                name_formatted: "Valentine's Scroll Cas..."
                quantity: 2
                quantity_formatted: 2
                size: "full"
                thumbnail: "https://www.mousehuntgame.com/images/items/convertibles/e71bedc4928eb1bd37444de3ed6a14b7.gif?cv=216"
                type: "valentines_2015_scroll_case_convertible"
                */
                //Bare minimum?
                /*
                classification: "stat"
                css_class: ""
                description: "This specialized drill is used to quickly bore through ice, assisting hunters in reaching deeper depths of the Iceberg.<br />↵<br />↵It requires Drill Charges to operate, and <b>can only be used 7 times</b> during each Iceberg invasion before it overheats. It will be reusable after defeating an Iceberg invasion.<br />↵<br />↵Your first purchase of the Ice Drill Mk. III comes with <b>4 Drill Charges</b>, which can also be purchased from the General Store with Cold Fusion.<br>↵<br>↵<b>Caution: Drilling past Mouse Generals will skip them and their loot.</b>"
                display_order: 0
                is_small: true
                item_id: 891
                name: "Ice Drill Mk. III"
                name_formatted: "Ice Drill Mk. III"
                quantity: 1
                quantity_formatted: 1
                size: "small"
                thumbnail: "https://www.mousehuntgame.com/images/items/stats/01ef6c31b2cd160f1bf49868cae51d02.gif?cv=216"
                type: "iceberg_drill_level_three_stat_item"
                */
                new_item.classification = item.classification;
                new_item.css_class = "ht_hidden_item";
                new_item.description = item.description;
                new_item.display_order = item.display_order;
                new_item.is_small = true;
                new_item.item_id = item.item_id;
                new_item.name = item.name;
                new_item.name_formatted = item.name;
                new_item.quantity = item.quantity;
                if (item.limited_edition == true)
                    new_item.limited_edition = true;
                new_item.quantity_formatted = item.quantity;
                new_item.size = "small";
                new_item.thumbnail = item.thumbnail;
                new_item.type = item.type;

                //TODO: Add a new tag for HT added things if it doesn't exist.
                if ($.isEmptyObject(tag_made))
                {
                    tag_made = {
                        count: 0,
                        css_class: "ht_hidden_tab",
                        display_order: -1,
                        items: [],
                        name: "Hidden Items",
                        type: "unknown"
                    };

                    st.columns = st.columns + 1;
                    st.tags.push(tag_made);
                }

                tag_made.count = tag_made.count + 1;

                /*
                count: 23
                css_class: ""
                display_order: -1
                items: Array[23]
                name: "Adventuring Items"
                type: "adventuring"
                */

                //Add to the tab!
                tag_made.items.push(new_item);
            }
        }
    }

    //Remove the spinner and text if they exist.
    if ($('#spinner').length > 0)
    {
        $('#spinner').remove();
        $('#spinner_text').remove();
    }

    if ($('.mousehuntPage-loading:visible').length == 0)
    {
        if (document.URL.match(/inventory.php\?tab=special/g) !== null)
        {
            //Modify the tab value with our new hidden items.
            //hg.utils.TemplateUtil.render("PageInventory_tab_content", tabToRender);
            var returned = 
                hg.utils.TemplateUtil.render("PageInventory_tab_content", tabToRender);
            
            //Actually populate this value.
            $('.mousehuntHud-page-tabContent.active').html(returned);
        }
        else
        {
            //?
        }
    }
    else
    {
        timeout_hidden = setTimeout(xhri_populate_hidden_specials, 2000, aEvent);
    }
}

function xhri_platinum_crown(aEvent)
{
    if ($('.crownbreak').length <= 0)
    {
        timeout_plat = setTimeout(function(){xhri_platinum_crown(aEvent)}, 2000);
        return;
    }

    if (_timeout_plat_called)
        return;

    _timeout_plat_called = true;
    timeout_plat = setTimeout(function(){call_create_platinum_crowns()}, 2000);
}

function xhri_get_fb_user(aEvent)
{
    if (_cur_user_from_XHR == '')
    {
        _timeout_get_fb = setTimeout(function(){xhri_get_fb_user(aEvent)}, 2000);
        return;
    }
    
    var evt = document.createEvent("MutationEvents");
    evt.initMutationEvent("ht_fb_user", true, true, null, JSON.stringify(aEvent.newValue), JSON.stringify(_cur_user_from_XHR), 'ht_stuff', 1);
    document.dispatchEvent(evt);
}

function xhri_options_change(aEvent)
{
    _last_user_options = JSON.parse(aEvent.newValue);
}

function xhri_modify_TEM(aEvent)
{
    //beta
    if ($('a.campPage-trap-trapEffectiveness').length != 0)
    {
        if ($('.campPage-trap-trapEffectiveness-mouse').length == 0)
        {
            //Resubmit this event in 2 seconds or so as we don't have the divs in place yet.
            timeout_TEM = setTimeout(function(){
                xhri_modify_TEM(aEvent);
            }, 2000);
        }
    }

    //Actually modify the TEM...
    var TEM_obj = JSON.parse(aEvent.newValue);
    for (var mouse_name in TEM_obj)
    {
        var ht_TEM_div = $('<div>').addClass('ht_TEM').append(TEM_obj[mouse_name]);
        //Non beta UI method.
        if ($('a.campPage-trap-trapEffectiveness').length == 0)
            $('a.mouse img[title="' + mouse_name + '"]').parent().parent().append(ht_TEM_div);
        else
            $('.campPage-trap-trapEffectiveness-mouse[title="' + mouse_name + '"]').append(ht_TEM_div);
    }
}

function xhri_journal_summary(aEvent)
{
    //TODO: Fix this.
    //NOTE: This is likely insecure.
    eval(aEvent.prevValue);
    return;
}

function call_create_platinum_crowns()
{
    //If we already have a platinum div, leave!
    if ($('.crownheaderplatinum').length > 0)
        return;

    //Create platinum header after the first crownbreak.
    /*
    <div class="crownheader crownheaderplatinum">Platinum Crowns 
        <span class="crownquantity">( [NumCrowns] )</span> 
        <div class="crownnote">Earned at 1,000 catches</div>
    </div>
    */

    var plat = document.createElement('div');
    plat.classList.add('crownheader');
    plat.classList.add('crownheaderplatinum');
    plat.appendChild(document.createTextNode('Platinum Crowns '));

    var span_quant = document.createElement('span');
    span_quant.classList.add('crownquantity');
    span_quant.appendChild(document.createTextNode('( ? )'));
    plat.appendChild(span_quant);

    var divnote = document.createElement('div');
    divnote.classList.add('crownnote');
    var plat_note = 'Earned at ' + Number(1000).toLocaleString() + ' catches';
    divnote.appendChild(document.createTextNode(plat_note));
    plat.appendChild(divnote);

    if ($('.crownheadertop').length == 0)
        $('.crownheader').parent().prepend(plat); //No favorites set.
    else
        $($('.crownbreak')[0]).after(plat); //Favorites set.

    //Find all $('.crownheadergold ~ a .mousebox > .deets > .numcatches') >= 1000 and move them to the platinum crown div.
    var platCount = 0;
    var lastElem = $('.crownheaderplatinum');
    var css_use = '.crownheadergold ~ a .mousebox > .deets > .numcatches';
    var css_use_individual = '.crownheadergold ~ a';
    if ($(css_use).length == 0)
    {
        css_use = '.crownheadergold ~ .mousebox > .deets > .numcatches';
        css_use_individual = '.crownheadergold ~ .mousebox';
    }

    $(css_use).each(function(index, element){
        if (parseInt(element.innerHTML) >= 1000)
        {
            platCount++;
            var newPlat = $(css_use_individual)[0];
            $(element).removeClass('gold');
            $(element).addClass('platinum');
            lastElem.after(newPlat);
            lastElem = $(newPlat);
        }
    });

    //Find all favorite crowns >= 1000 and move them to the platinum crown div
    css_use = '.crownheadertop ~ a .mousebox.favorite > .deets > .numcatches';
    css_use_individual = '.crownheadertop ~ a';
    if ($(css_use).length == 0)
    {
        css_use = '.crownheadertop ~ .mousebox.favorite > .deets > .numcatches';
        css_use_individual = '.crownheadertop ~ .mousebox';
    }

    $(css_use).each(function(index, element){
        if (parseInt(element.innerHTML) >= 1000)
        {
            $(element).removeClass('gold');
            $(element).addClass('platinum');
        }
    });

    //Change the crownquantity for crownheadergold to be the existing number minus the number added. $('.crownheadergold > .crownquantity')
    var oldGold = parseInt($('.crownheadergold > .crownquantity').text().replace(')','').replace('(','').trim());

    $('.crownheadergold > .crownquantity').text('( ' + (oldGold - platCount) + ' )');

    //Change the crownquantity for crownheaderplatinum to be the new number minus the number added. $('.crownheaderplatinum > .crownquantity')
    $('.crownheaderplatinum > .crownquantity').text('( ' + platCount + ' )');

    //If platCount is 0, add a no crown div, otherwise add a crownbreak.
    if (platCount == 0)
    {
        //Create a nocrown div: <div class="nocrowns">Platinum Crowns are earned by catching a mouse 1,000 times.</div>
        var crownbreak = document.createElement('div');
        crownbreak.classList.add('nocrowns');
        var break_note = 'Platinum Crowns are earned by catching a mouse ' + Number(1000).toLocaleString() + ' times.';
        crownbreak.appendChild(document.createTextNode(break_note));
        $('.crownheadergold').before(crownbreak);
    }
    else
    {
        //Create a crownbreak div: <div class="crownbreak"></div>
        var crownbreak = document.createElement('div');
        crownbreak.classList.add('crownbreak');
        $('.crownheadergold').before(crownbreak);
    }
}

function getQueryParams(qs) 
{
    var urlParams = {},
        e,
        d = function (s) { 
            var comp = s;
            try {
                comp = decodeURIComponent(s).replace(/\+/g, " "); 
            }
            catch (e){}
            return comp;
        },
        r = /([^&=]+)=?([^&]*)/g;

    while (e = r.exec(d(qs))) {
        if (e[1].indexOf("[") == "-1")
            urlParams[d(e[1])] = d(e[2]);
        else {
            var b1 = e[1].indexOf("["),
                aN = e[1].slice(b1+1, e[1].indexOf("]", b1)),
                pN = d(e[1].slice(0, b1));
          
            if (typeof urlParams[pN] != "object")
                urlParams[d(pN)] = {},
                urlParams[d(pN)].length = 0;
            
            if (aN)
                urlParams[d(pN)][d(aN)] = d(e[2]);
            else
                Array.prototype.push.call(urlParams[d(pN)], d(e[2]));
        }
    }

    return urlParams;
}

(function(open) 
{
    XMLHttpRequest.prototype.open = function(method, url, async, user, pass) 
    {
		if (typeof url == 'string')
        {
			if (url.search('mousehuntgame') != -1 &&
                url.search('board.php') == -1 &&
                url.search('marketplace.php?') == -1)
            {
				if (!_is_xhri_setup)
                    setupCommunications();
				
				window.unsafeWindow || (
                    unsafeWindow = (function() 
                    { 
                        var el = document.createElement('p');
                        el.setAttribute('onclick', 'return window;');
                        return el.onclick();
                    })()
                );
				
				var mh_user = unsafeWindow.user;

                _cur_user_from_XHR = mh_user;
				
				if (mh_user != null)
                {
					this.mhUserJSON = mh_user;

                    //TODO: Make a call to grab the user variable from HG with the least amount of data. Use this as a direct before snapshot of event firing.
                    //      In conjunction with the earlier variable grab we can determine 3 points of user values for hunt tracking.

                    this.calledURL = url;
                    this.addEventListener("readystatechange", function() 
                    {
						if (this.readyState == 4)
                        {
							if (is_JSON_string(this.responseText))
                            {
								//TODO: Make this all generic, have one event thrown, and one submit. 
                                //      Specific instructions can be made in the response packet so message information can be gathered from there.

                                var respJSON = JSON.parse(this.responseText);
                                respJSON.ht_submit_URL = this.calledURL;
                                respJSON.ht_submit_params = this.submit_params;

								//Hunt submission
                                if (this.calledURL.search('activeturn.php') != -1)
                                {
                                    var evt = document.createEvent("MutationEvents");

                                    evt.initMutationEvent("ht_submit", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);
                                    document.dispatchEvent(evt);
                                }
								
								//Trap change
                                //Hunt submission
                                else if (this.calledURL.search('changetrap.php') != -1)
                                {
                                    var evt = document.createEvent("MutationEvents");

                                    evt.initMutationEvent("ht_trap_change", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);
                                    document.dispatchEvent(evt);
                                }
								
								//Rift misting change.
                                else if (this.calledURL.search('rift_burroughs.php') != -1)
                                {
                                    var evt = document.createEvent("MutationEvents");

                                    evt.initMutationEvent("ht_trap_change", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);
                                    document.dispatchEvent(evt);
                                }
								
								//Crafting attempts.
                                else if (this.calledURL.search('crafting.php') != -1)
                                {
                                    var t_array = Array();
                                    for (var i in respJSON.ht_submit_params.parts)
                                    {
                                        //Success?
                                        if (is_set(respJSON.inventory))
                                        {
                                            for (var j in respJSON.inventory)
                                            {
                                                if (j == i)
                                                {
                                                    var t_obj = new Object();
                                                    t_obj.name = respJSON.inventory[j].name;
                                                    t_obj.val = respJSON.ht_submit_params.parts[i];
                                                    t_array.push(t_obj);
                                                    break;
                                                }
                                            }
                                        }
                                        //Failed?
                                        else
                                        {
                                            var inventory = $('.itemBoxSmall.selected');
                                            for (var j = 0; j < inventory.length; j++)
                                            {
                                                if ($(inventory[j]).data('item-type') == i)
                                                {
                                                    var t_obj = new Object();
                                                    t_obj.name = $('div.tooltip b', inventory[j]).text();
                                                    t_obj.val = respJSON.ht_submit_params.parts[i];
                                                    t_array.push(t_obj);
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    respJSON.ht_submit_params.parts.translated = t_array;

                                    var evt = document.createEvent("MutationEvents");

                                    evt.initMutationEvent("ht_crafting", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);
                                    document.dispatchEvent(evt);
                                }
								
								//TEM effectiveness selection
                                else if (this.calledURL.search('getmiceeffectiveness.php') != -1)
                                {
                                    var evt = document.createEvent("MutationEvents");

                                    evt.initMutationEvent("ht_effectiveness", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);
                                    document.dispatchEvent(evt);
                                }
								
								//Adversaries page tab click
                                else if (this.calledURL.search('getmousegroup.php') != -1 ||
                                         this.calledURL.search('getregionmice.php') != -1)
                                {
                                    var evt = document.createEvent("MutationEvents");

                                    evt.initMutationEvent("ht_group_effectiveness", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);
                                    document.dispatchEvent(evt);
                                }
								
								//Convertible
                                else if (this.calledURL.search('useconvertible.php') != -1)
                                {
                                    var evt = document.createEvent("MutationEvents");
                                    evt.initMutationEvent("ht_convertible", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);

                                    document.dispatchEvent(evt);
                                }
								
								//Inventory
                                else if (this.calledURL.search('userInventory.php') != -1)
                                {
                                    //This is a bit of a hack, but it should work in almost all instances:
                                    //If we get a value back that's only 1 item and it's stat, quest, or convertible
                                    //We should ignore as we're likely just pulling up LLL.
                                    if (respJSON.items.length > 1)
                                    {
										
                                        //Look for a classification of weapon AND base in the same response. 
                                        //This is a TEM opening and NOT an inventory check.
                                        var hasWeapon = false;
                                        var hasBase = false;
                                        for (var i in respJSON.items)
                                        {
                                            var classification = respJSON.items[i].classification;
                                            if (classification == 'weapon')
                                                hasWeapon = true;
                                            if (classification == 'base')
                                                hasBase = true;
                                        }
										
                                        //Check to see if we don't have a weapon or don't have a base and we're likely good to go.
                                        if (!hasWeapon || !hasBase)
                                        {
                                            var evt = document.createEvent("MutationEvents");
                                            evt.initMutationEvent("ht_inventory", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);

                                            document.dispatchEvent(evt);
                                        }
                                    }
                                }
								
								//Treasure Map
                                else if (this.calledURL.search('relichunter.php') != -1)
                                {
                                    if (is_set(respJSON.treasure_map))
                                    {
                                        var evt = document.createEvent("MutationEvents");
                                        evt.initMutationEvent("ht_treasure", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);

                                        document.dispatchEvent(evt);
                                    }
                                }
								
								//Adding platinum crowns?
                                else if (this.calledURL.search('profiletabs.php') != -1)
                                {
                                    if (is_set(_last_user_options))
                                    {
                                        if (_last_user_options.ht_is_platinum_crown)
                                            timeout_plat = setTimeout(function(){call_create_platinum_crowns()}, 2000);
                                    }
                                    else
                                        Console.log('User options are not currently set.');
                                }
								
								//Modifying data from tacky glue
                                else if (this.calledURL.search('getstat.php') != -1)
                                {
                                    var evt = document.createEvent("MutationEvents");
                                    evt.initMutationEvent("ht_getstat", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);

                                    document.dispatchEvent(evt);
                                }
								
								//Marketplace data.
                                else if (this.calledURL.search('marketplace.php') != -1)
                                {
                                    if (respJSON.details !== undefined)
                                    {
                                        var evt = document.createEvent("MutationEvents");

                                        evt.initMutationEvent("ht_marketplace", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);
                                        document.dispatchEvent(evt);
										
									}
                                }
								
								
                                //2017 Snowman
                                else if (this.calledURL.search('winter_hunt_2017.php') != -1)
                                {
                                    for (var i in respJSON.journal_markup) {
                                        var entry = respJSON.journal_markup[i];
                                        var cssClasses = entry.render_data.css_class;

                                        if (cssClasses.indexOf('claimGolemReward') !== -1) {
                                            var evt = document.createEvent("MutationEvents");
                                            evt.initMutationEvent("ht_2017_winter_hunt", true, true, null, JSON.stringify(this.mhUserJSON), JSON.stringify(respJSON), 'ht_stuff', 1);

                                            document.dispatchEvent(evt);
                                        }
                                    }
                                }
                                
								//Page.php call. (could be a lot of things)
                                else if (this.calledURL.search('page.php') != -1)
                                {
                                    //TODO: This is where we need to check the responseJSON object
                                    //      for our different workflows.
                                    for (var i in respJSON.page.tabs)
                                    {
                                        var tab = respJSON.page.tabs[i];
                                        if (tab.initialized)
                                        {
                                            switch (tab.name.toLowerCase())
                                            {
                                                case "special":
                                                    //Have the _specials items call called.
                                                    if ($.isEmptyObject(_specials))
                                                    {
                                                        var evt = document.createEvent("MutationEvents");
                                                        evt.initMutationEvent("ht_getspecials", true, true, null, '', '', 'ht_stuff', 1);

                                                        document.dispatchEvent(evt);
                                                    }
                                                    else
                                                    {
                                                        //Call another page.php via HT.
                                                        var evt = document.createEvent("MutationEvents");
                                                        evt.initMutationEvent("ht_getpage_special", true, true, null, JSON.stringify(_specials), '', 'ht_stuff', 1);
                                                        document.dispatchEvent(evt);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
							}
						}
					}, 
                    false);
				}
			}
		}
        //Call regardless. Use async if not defined to avoid hanging browser due to extension needing waiting on response.
        if (is_set(async))
            open.call(this, method, url, async, user, pass);
        else
            open.call(this, method, url, true, user, pass);
    };
})(XMLHttpRequest.prototype.open);

(function(send) {
    XMLHttpRequest.prototype.send = function()
    {
        if (arguments.length != 0)
        {
            if (typeof arguments[0] == 'string'
                && arguments[0] != "")
            {
                this.submit_params = getQueryParams(arguments[0]);
            }
        }

        send.apply(this, arguments);
    }
})(XMLHttpRequest.prototype.send);

function is_set(variable)
{
    if (typeof variable === "undefined")
        return false;
    else
        return true;
}

function is_JSON_string(str) 
{
    try 
    {
        JSON.parse(str);
    } 
    catch (e) 
    {
        return false;
    }

    return true;
}

 if (window == top) 
{
    if (is_set(chrome.runtime))
    {
        chrome.runtime.onMessage.addListener(function(req, sender, sendResponse) {
        	window.postMessage({ type: "FROM_PAGE", user:req }, "*");
        });
    }
} 